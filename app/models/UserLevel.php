<?php

class UserLevel extends SparkModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'UserLevels';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = array('id');
    
    
    public function user(){
        return $this->belongsTo("User");
    }
}
