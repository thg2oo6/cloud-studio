<?php

abstract class SparkModel extends Eloquent {

    public function createdBy() {
        return User::find($this->created_by)->username;
    }

    public function updatedBy() {
        return User::find($this->updated_by)->username;
    }

    public static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = Auth::user()->id;
            $model->updated_by = Auth::user()->id;
        });

        static::updating(function($model) {
            $model->updated_by = Auth::user()->id;
        });
    }

}
