<?php

class SSHKey extends SparkModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'SSHKeys';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = array('id');
    
    
    public function user(){
        return $this->belongsTo("User");
    }
}