<?php

class ProjectType extends SparkModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ProjectTypes';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = array('id');
    
    
    public function projects(){
        return $this->hasMany("Project");
    }
}
