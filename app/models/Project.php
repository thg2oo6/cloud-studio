<?php

class Project extends SparkModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Projects';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = array('id');
    
    
    public function user(){
        return $this->belongsTo("User");
    }
    
    public function projecttype(){
        return $this->belongsTo("ProjectType");
    }

    public function clones(){
        return $this->belongsTo("Project", "clones", "id");
    }

    public function activities(){
        return $this->hasMany("Activity", "project_id", "id");
    }
}
