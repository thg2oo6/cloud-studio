<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');
    protected $guarded = array('id', 'password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            \Spark\Project\User::CreateWorkspace($user);
        });
    }

    public function projects()
    {
        return $this->hasMany("Project");
    }

    public function getLevelAttribute()
    {
        try {
            $today = date("Y-m-d H:i:s", time());
            $level = UserLevel::where("user_id", "=", $this->id)->where("expiration", ">=", $today)->orderBy("expiration", "desc")->firstOrFail();
            return $level->level;
        } catch (\Exception $ex) {
            return 0;
        }
    }

    public function activities()
    {
        return $this->hasMany("Activity");
    }

    public function sshkeys()
    {
        return $this->hasMany("SSHKey");
    }

    public function updateApi()
    {
        $randomid = time();

        $str = sprintf("%s|%d|%s|%d|%s|%s|%s|%d",
            $this->username,
            $randomid,
            $this->email,
            $randomid,
            $this->firstname,
            $this->username,
            $this->lastname,
            time()
        );

        $this->apikey = sha1($str);
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

}
