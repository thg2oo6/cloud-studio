<?php

class LevelAuthorization extends SparkModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'LevelAuthorizations';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = array('id');
}
