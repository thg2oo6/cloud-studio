<?php

class Activity extends SparkModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Activities';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $guarded = array('id');
    
    
    public function user(){
        return $this->belongsTo("User");
    }
    
    public function project(){
        return $this->belongsTo("Project");
    }
}