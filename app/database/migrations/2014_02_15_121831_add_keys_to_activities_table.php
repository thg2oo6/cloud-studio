<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToActivitiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('Activities', function(Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('Users');
            $table->foreign('updated_by')->references('id')->on('Users');
            
            $table->foreign('user_id')->references('id')->on('Users');
            $table->foreign('project_id')->references('id')->on('Projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('Activities', function(Blueprint $table) {
            $table->dropForeign('activities_created_by_foreign');
            $table->dropForeign('activities_updated_by_foreign');
            
            $table->dropForeign('activities_user_id_foreign');
            $table->dropForeign('activities_project_id_foreign');
        });
    }

}
