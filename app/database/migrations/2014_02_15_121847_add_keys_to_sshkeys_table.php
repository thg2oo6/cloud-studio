<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToSshkeysTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('SSHKeys', function(Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('Users');
            $table->foreign('updated_by')->references('id')->on('Users');
            
            $table->foreign('user_id')->references('id')->on('Users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('SSHKeys', function(Blueprint $table) {
            $table->dropForeign('sshkeys_user_id_foreign');
            
            $table->dropForeign('sshkeys_created_by_foreign');
            $table->dropForeign('sshkeys_updated_by_foreign');
        });
    }

}
