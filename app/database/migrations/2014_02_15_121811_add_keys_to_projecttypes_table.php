<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToProjecttypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('ProjectTypes', function(Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('Users');
            $table->foreign('updated_by')->references('id')->on('Users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('ProjectTypes', function(Blueprint $table) {
            $table->dropForeign('projecttypes_created_by_foreign');
            $table->dropForeign('projecttypes_updated_by_foreign');
        });
    }

}
