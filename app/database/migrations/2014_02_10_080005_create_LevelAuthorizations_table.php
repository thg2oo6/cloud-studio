<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelAuthorizationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('LevelAuthorizations', function(Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('level')->unsigned();
            $table->string('name', 255);
            $table->string('slug', 75);
            $table->timestamps();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('LevelAuthorizations');
    }

}
