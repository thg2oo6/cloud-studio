<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('Projects', function(Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 60);
            $table->string('slug', 60);
            $table->string('logo', 60)->default('cds');
            $table->bigInteger('user_id')->unsigned()->nullable(); //Owner ID
            $table->bigInteger('projecttype_id')->unsigned()->nullable();
            $table->text('description');
            $table->bigInteger('clones')->unsigned()->nullable();
            $table->boolean('visibility')->default(false);  //Public (true) or Private (false)
            $table->timestamps();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('Projects');
    }

}
