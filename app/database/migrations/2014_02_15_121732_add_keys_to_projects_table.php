<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToProjectsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('Projects', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('Users');
            $table->foreign('created_by')->references('id')->on('Users');
            $table->foreign('updated_by')->references('id')->on('Users');
            $table->foreign('projecttype_id')->references('id')->on('ProjectTypes');
            
            $table->foreign('clones')->references('id')->on('Projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('Projects', function(Blueprint $table) {
            $table->dropForeign('projects_clones_foreign');
            
            $table->dropForeign('projects_user_id_foreign');
            $table->dropForeign('projects_created_by_foreign');
            $table->dropForeign('projects_updated_by_foreign');
            $table->dropForeign('projects_projecttype_id_foreign');
        });
    }

}
