<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ProjectTypes', function(Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 75);
            $table->string('slug', 75);
            $table->string('logo');
            $table->text('description');
            $table->integer('level')->unsigned()->default(0);
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ProjectTypes');
    }

}
