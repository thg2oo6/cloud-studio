<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeysToLevelauthorizationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('LevelAuthorizations', function(Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('Users');
            $table->foreign('updated_by')->references('id')->on('Users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('LevelAuthorizations', function(Blueprint $table) {
            $table->dropForeign('levelauthorizations_created_by_foreign');
            $table->dropForeign('levelauthorizations_updated_by_foreign');
        });
    }

}
