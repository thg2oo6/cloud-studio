<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('Users', function(Blueprint $table) {
            $table->bigIncrements("id")->unsigned();
            $table->string("username", 60)->unique();
            $table->string("password", 255);
            $table->text("email");
            $table->string("firstname", 60);
            $table->string("lastname", 60);
            $table->integer("invitations")->unsigned()->default(0);
            $table->bigInteger("invitedby")->unsigned()->default(0);
            $table->boolean("activated")->default(false);
            $table->string("activation")->nullable()->default(null);
            $table->boolean("oauth")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('Users');
    }

}
