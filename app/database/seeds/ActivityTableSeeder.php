<?php

class ActivityTableSeeder extends Seeder {

    public function run() {
        DB::table("Activities")->truncate();
        Activity::create(
                array(
                    'activity' => "test 123",
                    'user_id' => 1,
                    'project_id' => 1,
                )
        );
    }

}
