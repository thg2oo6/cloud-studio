<?php

class LevelAuthorizationTableSeeder extends Seeder {

    public function run() {
        DB::table("LevelAuthorizations")->truncate();
        LevelAuthorization::create(
                array(
                    'level' => 2,
                    'name' => "Testing environment",
                    'slug' => "SPK_TESTING_ENV",
                )
        );
    }

}
