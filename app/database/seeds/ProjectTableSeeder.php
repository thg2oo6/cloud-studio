<?php

class ProjectTableSeeder extends Seeder {

    public function run() {
        DB::table("Projects")->truncate();
        Project::create(
                array(
                    'name' => "test 123",
                    'slug' => "test_123",
                    'logo' => "gigel",
                    'description' => "testing only",
                    'user_id' => 1,
                    'projecttype_id' => 1,
                )
        );
    }

}
