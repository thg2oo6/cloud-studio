<?php

class UserLevelTableSeeder extends Seeder {

    public function run() {
        DB::table("UserLevels")->truncate();
        UserLevel::create(
                array(
                    'user_id' => 1,
                    'level' => 2,
                    'expiration' => "2014-02-28 03:00:00",
                )
        );
    }

}
