<?php

class SSHKeyTableSeeder extends Seeder {

    public function run() {
        DB::table("SSHKeys")->truncate();
        SSHKey::create(
                array(
                    'user_id' => 1,
                    'sshkey' => "TestingEnvironment",
                )
        );
    }

}
