<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $this->call('UserTableSeeder');
//        $this->call('UserLevelTableSeeder');
//        $this->call('LevelAuthorizationTableSeeder');
//        $this->call('SSHKeyTableSeeder');
//        $this->call('ProjectTypeTableSeeder');
//        $this->call('ProjectTableSeeder');
//        $this->call('ActivityTableSeeder');
    }

}
