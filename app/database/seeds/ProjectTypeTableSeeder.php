<?php

class ProjectTypeTableSeeder extends Seeder {

    public function run() {
        DB::table("ProjectTypes")->truncate();
        ProjectType::create(
                array(
                    'name' => "test 123",
                    'slug' => "test_123",
                    'logo' => "gigel.png",
                    'description' => "testing only",
                )
        );
    }

}
