<?php

if (strtoupper(PHP_OS) === 'DARWIN') {
    define("CDS_BASE", "/Volumes/WorkStuff/cds");
} else {
	define("CDS_BASE", "/opt/cds");
}

return array(
	"environment"  => "dev",
    "path"         => array(
        "projects"     => CDS_BASE . "/projects",
        "testing"      => CDS_BASE . "/test",
        "deployment"   => CDS_BASE . "/deployment",
    ),
    "special"      => array(
        "tools"        => CDS_BASE . "/tools",
        "tmp"          => CDS_BASE . "/tmp"
    ),
    "disable_register" => false,
);