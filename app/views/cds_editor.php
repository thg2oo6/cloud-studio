<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= $project->name ?> | Cloud Developer Studio v<?= \Spark\Base\About::version(".") ?></title>

    <meta http-equiv="Expires" CONTENT="0">
    <meta http-equiv="Cache-Control" CONTENT="no-cache">
    <meta http-equiv="Pragma" CONTENT="no-cache">

    <script>
        window.CDSConfig = {
            ContactEmail: "support@duricu.ro",
            TicketSystem: "https://bitbucket.org/thg2oo6/cloud-studio/issues",
            Language:   'en',
            Project: '<?= $project->slug ?>',
            ProjectName: '<?= $project->name?>',

            Path: "<?= URL::asset(''); ?>",
            Test: "<?= substr(URL::asset(''), 0, strlen(URL::asset('')) - 1);?>:2081/<?= Auth::user()->username;?>/<?= $project->slug?>/",
            Live: "<?= substr(URL::asset(''), 0, strlen(URL::asset('')) - 1);?>:2080/<?= Auth::user()->username;?>/<?= $project->slug?>/",
            API: "master",

            Home: '<?= URL::route('dashboard'); ?>',
            Profile: '<?= URL::route('dashboard'); ?>',
            Logout: '<?= URL::route('logout'); ?>',

            Version: '<?= \Spark\Base\About::version(".") ?>',
            That: this,

            Theme: "eclipse",

            APIPath: function(what){
                return this.Path + "api/" + this.API + "/" + what;
            }
        }
    </script>

    <!-- IDE design -->
    <link href="<?= URL::asset("/editor/style.less");?>" type="text/css" rel="stylesheet/less" />
    <script src="<?= URL::asset("/front/scripts/less.js");?>"></script>
    <script src="<?= URL::asset("/front/scripts/mustache.js");?>"></script>

    <script src="<?= URL::asset("/front/scripts/jquery.js");?>"></script>
    <script src="<?= URL::asset("/editor/3rdparty/jqui.js");?>"></script>
    <link href="<?= URL::asset("/editor/3rdparty/jqui.css");?>" type="text/css"/>

    <script src="<?= URL::asset("/front/scripts/bootstrap.js");?>"></script>
    <link href="<?= URL::asset("/front/css/bootstrap.css");?>" type="text/css" rel="stylesheet" />
<!--    <link href="--><?//= URL::asset("/front/css/theme.css");?><!--" type="text/css" rel="stylesheet" />-->
    <link href="<?= URL::asset("/front/css/font-awesome.css");?>" type="text/css" rel="stylesheet" />
    <script src="<?= URL::asset("/editor/3rdparty/bootstrap.contextmenu.js");?>"></script>
    <script src="<?= URL::asset("/editor/3rdparty/sprintf.js");?>"></script>
    <script src="<?= URL::asset("/front/scripts/underscore.js");?>"></script>
</head>
<body>
<section class="loader">
    <section class="img"></section>
</section>

<script data-main="<?= URL::asset("/editor/cds.js");?>" src="<?= URL::asset("/editor/3rdparty/require.js");?>"></script>
<script src="<?= URL::asset("/editor/deps.js");?>"></script>
</body>
</html>