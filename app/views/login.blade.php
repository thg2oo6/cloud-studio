<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login @ Cloud Developer Studio</title>

    <script src="<?= URL::asset("/front/scripts/jquery.js");?>"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css' />

    <script src="<?= URL::asset("/front/scripts/bootstrap.js");?>"></script>
    <link href="<?= URL::asset("/front/css/bootstrap.css");?>" type="text/css" rel="stylesheet" />
    <link href="<?= URL::asset("/front/css/theme.css");?>" type="text/css" rel="stylesheet" />

    {{ HTML::style('/front/css/login.css') }}
</head>
<body>
    <section class="wrapper">
        <header class="mainHeader">
            <a href="{{ URL::route('home') }}" class="cds" title="Cloud Developer Studio">Cloud Developer Studio</a>
        </header>
        
        <section class="inputs">
            <form action="" method="POST" id="LoginAction">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <ul>
                    @if (Session::has('cds_error'))
                    <li class="error"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;{{ Session::get('cds_error') }}</li>
                    @endif
                    @if (Session::has('cds_notice'))
                    <li class="info"><span class="glyphicon glyphicon-info-sign"></span> {{ Session::get('cds_notice') }}</li>
                    @endif
                    <li class="input">
                        <span class="glyphicon glyphicon-user"></span> <span class="divider"></span>
                        <input type="text" name="username" placeholder="username" />
                        <span class="clear"></span>
                    </li>
                    <li class="input">
                        <span class="glyphicon glyphicon-lock"></span> <span class="divider"></span>
                        <input type="password" name="password" placeholder="password" />
                        <span class="clear"></span>
                    </li>
                    <li class="buttons">
                        <section class="recover">
                            <a href="<?= URL::route('recover')?>">recover password</a><br/>
                            <a href="<?= URL::route('register')?>">register</a>
                        </section>
                        <section class="login">
                            <button type="submit"><span class="glyphicon glyphicon-off"></span> Login</button>
                        </section>
                        <span class="clear"></span>
                    </li>
<!--                     <li class="buttons" style="text-align: center;"><a href="<?= URL::asset('social/facebook');?>" type="submit" class="btn btn-default">Login with Facebook</a></li> -->
                </ul>
            </form>
        </section>
        
        <footer class="mainFooter">
            2014 &copy; Cloud Developer Studio.
        </footer>
    </section>
</body>
</html>