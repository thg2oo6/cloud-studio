<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Account activated @ Cloud Developer Studio</title>

    <script src="{{ URL::asset('/front/scripts/jquery.js') }}"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css' />

    <script src="{{ URL::asset('/front/scripts/bootstrap.js'); }}"></script>
    <link href="{{ URL::asset('/front/css/bootstrap.css'); }}" type="text/css" rel="stylesheet" />
    <link href="{{ URL::asset('/front/css/theme.css'); }}" type="text/css" rel="stylesheet" />

    {{ HTML::style('/front/css/login.css') }}
</head>
<body>
<section class="wrapper">
    <header class="mainHeader">
        <a href="{{ URL::route('home') }}" class="cds" title="Cloud Developer Studio">Cloud Developer Studio</a>
    </header>

    <section class="inputs">
        <ul>
            <li>Your account, {{ $user->username }}, has been activated.<br/>Happy coding!</li>
            <li><a href="{{ URL::route('login') }}" class="btn btn-default pull-right">Login</a></li>
        </ul>
        </form>
    </section>

</section>
</body>
</html>