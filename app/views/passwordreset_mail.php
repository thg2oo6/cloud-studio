<html>
    <head>
        <title>Password reset</title>
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300);

            *{
                font-family: "Open Sans";
                font-size: 10pt;
            }
            body{
                background: #f1f1f1;
            }
            .cds{
                background: #fcfcfc;
                padding: 10px;
                border: 1px solid #dcdcdc;
                width: 650px;
                margin: 40px auto;
            }
            .cds .logo{
                background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdwAAAA7CAYAAADRlTt7AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAFj5JREFUeNrsXd2O7EhS/mrV9+N9gmOe4PhIiNv2uQWN2s0FIAS0SytGKwHbVYwYBglUXYsEy/JT3asVuxKgql4h0F6sulqLuG2fS8TF+DzB+DwBngcYm4uOnMqKyrTTLrv+Oj7J6q4q/4QjI76ISKczB/j5/0IgaAAfwIT+3gNYiEoEAsFLRPlbv9po/zOU5b5ljgCcAwgAJADeA1hKUxoRkq5CACkFvHTHMjxQWyl5JOD2D0/zE4/a/HEPbS8QCDdugQH+83/2SSIPpCCOBYCh2NAa5gBiw/eXO0xQAgBfbNiQoG+dP1CPAsdQEh6BYH/cWP7Orx1NhTvRgm0O4I1WQcUAMgBTsaVvdBUzXfkAngDMdhhwPWmKvZCJCrYJkYgKwnPyk0TUJBBu3Cs39h5wA0bATZzeBzBiFW1G/49JWZEE3G+g62pJulJEG1JbpKKmk0OMVfc9ANwRqSQAbgHcALiQgHuyCIkrX2nffZAk63i58QyFU8D1sHqGFMLcvQW60SURQ1Zxvoh9fs8Cd86I5iUjYolNxnS1C6Py6DoXht90A8pJDvX3nRDDVuD61tv4kQKu+MnpJVkXBo40YUl2sBBu3Bs3Nq1wa4n2mrIIr0HlOiIjGBP51hFJZiCWkLaXTtiv2eevDPu86jHDvoL5+UhVYNaTqpyIYVqThAnqE9PMEHxDUdPJtPWMFTMJ47+ANl87JsJzt+oYL2+w6T65sW3ALavI9sEQaBM8jwBLNALwNHKOtEwtAPDWEHR98a9GQc9W5fRZ0c4ds+yxJpNHThCQ3B5tMW0LCbzOEB95ORhRsFWo85OICqFQs5UH6vF4SY/h9sGNWwbcorB1a8wNNzO2VJyqilmyQB3g+XnsmxoyEQJuHxiB567brhBgc1SsSrJy+o3bhW4TS002lX37ml1FZEcLab5GAVd85DQRs2DrMvJccS0fnXtDVd6tqNXKjSouJYdS4ZqC7QL27mEYKuCxdo6ASHfKFOFCJkIy1c/owh70pI+KzVlwDBucJ6fjltT+I1Y9Q4KuE2GIj5x2UjVn3NnEJ4ZaYqswwWrwkHDjuh70noQczyP+dxp4+aCp0BBsl2j+TuzCUNlMHZTUZyA5FeLlmVrWoZ5C1j7TDoKiCtrvmW1J0K3Ga/GRk8eEfb5rcY4xC7gqAL+EKrcJNwasJ8HDc2/dt3dc4RYwVB569tx2AoqlVtX4tLmQgwrSqfhjJa57CFjn7HOXTrtggVb9n0pbt66OxE+OG6GBM5siIxvQE+UL5ruBFtzvcfqDq0zceGEJzOEuq1y9wr3G5nOjIdy6kW2VDSeIrIERJuKPlZndiHR81+F53/cs94KCeqx99wDgV6RJWydH70QVR5sw+R2dK6+5zpNWDXonHnBt3PjBsv9OE1YVcD2sv0CsyDHp0CBcz3WlZWICewbn4XmQRN7heZe0RQ2TpCZQXWCedg39Gb/AjVQirAYrCo63h4K3a97SHmxBJMTLmiHOxo0L+i1gXJTvUjgVcCNDo3RFgBncu6UDrcRPxSetOroh/fQRpC41o8x6OH9OcuvPU2IJuI0Qa6QiOB2ELRIoD5vjYh61/1+9IP3VceMb8p1X2NPiH+q1INOMNtuS7bQFic46DvaniAkFrT4Xd+jbEBd0H3qVG0MGULkSrLKBO1HH0SKzVGdNA25OSbLeM5gIN1Zyz96gAm7Evt9Hd+4cq+XeEvHHSsK9PPIegJyIJda+O5eA69T26nncNuMrBIcRcHOs9yyGLRPPJeTRwlFw4xmKMrQ04K6DbYzV5BoCO05lCrd3LOBGOKwlGT2suurSAwhuKtgGRMiSnBw/eNKpuBDSvqfJjWcoisBQfWQ7liPGaumxpsQW4LlLPNCyxRSrhexdzudj9d7wvaOxe3RMQLLftZDdp6z2FdZfEdDl50hPxDmWWH9FSAU4l/uL8Pyeqq6zBNXPZTyspsNL8fzYIrfY0wybr2ws0G6QhafJG2g+9g7NJihQx962TEptOnvvSFK6vT9i85UxX7uG39IP+9BjiNVrInc4rN6zKczzlM/x3EU8xP7fs26jvwCrt15c+XRX3BhRvMhacvaaPQ7+6b+5PaYAHss//XWjLAP8w39NsD74IsHz/Md9NuAT++4GzZ/bRtic7NuE2wpyVfgC6wMPXBZVf2KG8LaBM6sAH9fsl2NzMNvbPZKGqe22kef/2P3VTWvn0ua2gMSnwTPJHWD9FQqTQ79pcH9qhi2von1t01xyv8xJP00z+DoZ1LmnqH7vmsuj2srD+kxiNnAf92k7Z70JeuKck76vttCj7qfqfJkhoIR0/ozsYldzfsfYnP8AjI/vtqjceLuhQTv5xI1eg9jgAfiStdWgwfX75MYI61PTtok7HnFQnXwZgGH56W8k6xXu14Vv2HEfmV4TzLC5DuJYkz3SnGhEn219+wGaL3EWoP0qLZzUU82hcs0JZzj94fwp02PViEo9YKZYzevtYX0+2pHWvcSdzcWuPC0ApfSdHgxih4zdw6r7l9unTige3VfuQKhpQ9L1iFxCRjB3GllFWs/OjDJ/116mK5LnSeuZmGoyBnTuSLt2RG0WVSRNuUaairzRkR7VPd8ym1KVSYDVAL4YbvMab4uFJostyQ21e3vEdt2meU0lGGp8HLXgoHgL3uqbG6+2bKsA6/PM5wDG5WcfLwBg8MNf+lpy6AN4Gvzwl7flZx+PVwG3OIiA2yZj1zPAS7bPkrYHzbkfKLvNDU7YpjuhbTeEblAZzKspLcjYnk486GaOetWDLddZTgT6kZbJj7C5JrPn4EyhlgAqUr5kpH9VQ8KmYHvJM1+ty0wF+mXHujUF2ykj3gVd9wtNnrnBn6quEdB5hoYE4ZJVmIGh+y3BcxdhxgKBx3pTutLjR9q+MZOdt90cu1nsXfn7vCL597REQAXfNlV4CvcezI9a3MtHLXWwC27chks9wzUvy88vvrGN8rOPMwDTwQ8eH8mnAGA0+MHjh/Lzi1sA+BaKEke0BSjKG/ZdUrH/WPvfR1HO9iz/DEXpaZ+nKMrcsm+KolwcWfs03TJD+/J9QhRl7KCze8NxTWS5oL85ivKWyZg1OO81u4+xZb97Zpt+h3odGeR8Z9k3J52qzxHTt20LaVuiKIeG3z0U5dyirxsU5bdRlG/p2kuy913pMSIdcdlzkilncuzCF1IU5RuSJ6nZ16M2+hJF+dTC1g9xO3RunDP5nmOPAeXnF1y+2eBvlr6qcI+pIpq0qKASLQOOsb/1WH1Dv39dNv6I+mdjp465o854mzZ94T/UKgDTuX3HNr5h1UTmKK/foV029ZMF0/PEsSs1h3lkeUTn8yp6E/apx1mF7Kp6jJld7AoL2nw8PxaLamxPdTnf4njf8Dh0bgzg9khqhaK4Z/c0ATDkqwUdMjzLTYeofgacMKe53pNhRoaunbxhl+tLQ8DIJoP7qMKvWl4z7biNXZF3aGemQHde0zWq+4lP56kjvYVB7pgF77HWBchlzPegx1gL6LmD33l7sv2MdDcmP7hC9fPREbXfWxzf+9mHzo1XFcmOucr9y99MBt//xZrdDb7/i/EZiiLvsJ97l42i33RVVvuVpZLZNc5bEOxLD7gXDfQRGAJImwo334O8Obp73euiItBMGwSqc4eAe18RbHMi/7RC38s96FElcK6zdC0PwA9SrAYKxhQAQosumjyDPxQcOjdGFYE4qahycxZPwzN8XaTshMGBNsorg8J9vVx3rFj2dX/elgHhJSCrSY6qyPSanSftScbEIWgrvHeUt0tS9y1+4qN6hHUbHkjZdWcVwZaTz+uK++5bjwtHUk9weN20C9oimLvtI8feiUPCoXOjzafiwV/9fFr+9W+bg/9zbNVt+fUZiuKrI6lwA8NNL6krJaaMNW1wrnTH8ocQ1OkkqzH0vOI8sfZ5fABOWSXvBOuvFkx79BNFzgFdd+kYbOoCLvcfnfxNlS0nn2CPenys+X2Kw5/PfUk6fTDocnJkAfeQuTGw2Id6NWlm7VHYHB8VnKEoD6UCbENoU9S/9uOSUZ0SqqqYQ89sPzi0OccI64OEFj2Qje+QffuO98tfaxuju+4xz2LbQ6xe/XHtcqzzkZxxhiLNW8dk1tujHvddQUVaZW6atcsVGSU3fKKJAO2X+ts3BxzatXyL3lXQjQZ/8R+j8m9/d7MNN8dHeWcoitRiEIeWIQUWp1fv+vn09xgHDXQJ14kUDrFNkwaOE2HzWdbtFtVtgtWIz2mF031o6bSKZPWKbFyRHPkd+YiqLodkGxH97XLear1bt88VjNro0dTO++SHEOuzHYVw7+K2JT5jbI7mD3Acj62CA7/Wa8v3t/RbDGA2+PN/z8u/+71FTYULNWgqZcK4DJg4FKQUZNUL619SEE4cs/NTxOsjaL/IkDXWVSixFhQ5iU4d2tyrqRZMgc4z2JsrZhZHX6D+9TS/Y30vtIQsJpkuK2Ro4iPq/pboZzDLNnrEFu3XB64N8mzLR0tDwK0blS7YHkONl+aDP/vZOYBx+fd/kFsCbq5eC1oyY47Q33Ow847KelPQVTO1PKF6wEO643vwWnZveDs2IFX9NKkYutLh0tJOAbMDH+uzFLkuJJ2yQM11q1YvUtfIDCSfVVwrqPhOHddkwYKgBz9RldRcS05vLVVpEx8JWhyTNvi+Sz3uO9kOe5DHVDR1Aa+D+zt2bqz0qfIfr4aDT+8zPL9mFgOIBp/eTy38maqJL+6x/qK5j/6eBQYdHRMaHPUNVs92Qjw/t8p7yHCDDvYPerjOtlXRNRnyq56N2MPmi+53lqozYJXs244ch/cCJCzxuDW0QVWvgUlnN2g3+MZvSSivLX4yZfeQYDW14Yi2tn4SsqTFlSA/VAQP3huxaz32bft9oI9EItjBMa8PnBt9S7GQaEF3OhjPH7FaZWyG1eQqOt6f4etCEVuCzQkiFj0YW9RhRekZbmpK5B3j+Rlf4FBJ9Z3FBS0dz295nXdbOm2EfkdpXrP7t1Urj8xewi3ay69pk4zkiEi+WwMh3Nf0DtQFO1dcdWiXnqWdhySbbTajx47bPGgQ0JcsIdu1HvsETyb8nq7zfksuaFtYeB1co09ubBqvnHyqnA1TAG8H3/u3kOwuYvvlABJ9ascpNoftj9B+BJ2NaG3Ky2rILMdqpRHdEZcWo76ljS+jd28gWpdArsviNTSA8wrjzBsShst12lbx77B6hSREP8+AlF3pbTWuIF6+MkibnpeJgexMjnSnBR91nUirgNOKe1I27BsCfdbQyUctiNknOZZYLTVXZ2cZVrMZlez7LtveN1y36vz3hoDbVo+HBs5hPrqZ1tOW0GQtiwVT8KwLhFct+D002Hqf3NgkaKt2Whjs0YjyR99JACSDP/6Xe7DFN8of/2H+LbYIwJJNzjyxTCjfZvNpwnAYJqUOayZJ90m2qWXC+apJvnX5F4YJ8zODPFX3PLFMvP+6QoaIJtzODJOoVy3UEBpkO6+Z0DytmPS7bluyycS7ngDcM0wCPjboBZaFBJT+vRYT+SfsXJ5hkn7dB2bsWlMHm5gaJp6ftZwk/ZZN6F83Mf8V/X00yBo5LEQANnG8q7yJg9/w8y8cztmVHg9tkv7HCvtpu0XsXvUFNzKDLl0Wp7i2cFIVv8cNFiXR5V7siBuzhgucXGn+sGTX9yunRd6UZQoA30JRQNuGKIpM++yhKOb0F1tsHorigf5mKIq37PdJxXFzFEWOopiiKFIUxY32e4yiCCuue63JnqMoxpb9luzzhWW/CYoioHsYst8iyzEz+ntnuP6kRl+5QVeRpT3Uvd5t0U4ZiuKW/g9I9+ho81AUT3Re9d2CtqrjpiiKRPvsa7ZUd80R6V+1vbIhvW08gw/k9P2NJmdiuUZMbZLRfpd0vN5eM0f9zGn/lNrxztFeApI1JRlumbyzGn3p500c2oRvSY3fXDEbc7HRrvSYdmjDXWwLg0x1PFa3XRt8purzjPwIFfYUU7tmjjyn4oTJpy8s7TTZMTdOmWx+xf2MqE1uNb7P2f0aY+3guz/16Hi17035z59kFHA3KopLljkEtASUv0VV80TnUedP2TJ7PorygWVpvnZdvQqasgz5wZKxjbRrmJbdQsVSULEhK5tp51NLaC2YvHND5hpry0ktLcd4LOtS9z2kY3llxnU1Idm6WLZqqlVW8ZZtr2erXxp6G4aOx1+yDDNEUX5RkalHJPeM2Zxqu9xgm9wHuF94hox+Ru2nH6NsLWW2aFtGTWXfX2i2onxwwaqT2FANKXvJmT71e/Ys1+dL6KWGe3e1GSXLzFBphhX8goreja70eGhV7tAg10PL3sQZ04eJA0y9O19Y9Dhi9jSu6WHSbSun/e9Yu8XMV5XdBSRXtkNuHDPdm5aQnJBeU822eQwJUZQPg09+stYtPfjkJypuKRkW5U+/+834gwG+82Nb//2T4aHvtOEz3QjrU74N2fO3meE5S0L7B5ZjbOfOtecW+vOHhM6ROTyzeGLnW2gPzn1tsMlSe6YxY/37Sg4lQ4rNyThs9+1r1+Gv5jywZz/qOvoxb9DNe5Ae1hfiBrX7e2y+7/e24nmcGrgWGp7ZthmQF2N9Oj9dd6bnKwlWq9Xw+5szfabaeSLLM9lUa8dQe0Z2abnGNbWzZ2g3MDtXz4rGzFY8bC4mb2p720IBE6y/gZBpNhKy9p2iehrFG6ZbfcT4CKv3ZdU1dD+06cnFFrfRI/e1GxzGtI1dcOzMMB6iigNG1I42Par24m0VM7/PSMeBNl4gJW5MteejfNpJZReh1k7DmnvqgxsDOsY3yFbF21XckTM7zAFMy3/9o7W2HCD+UdVAhznM892qSQYyy3EhOYl+cdtkFL62b8gGVdS91O7RjV9gfSm3lLZHNBuV7JFxXWB99Rh1roWFkPT79TUjqZq2rc192wKOiai7CLoTh4EnY+Zkr2CenELJ2cV6xIHWRr4hKLq2fYDVSHbeBvdYjdxX+wTMJu7hNjdxRIM2AsNglEQ7V1UwMsmR0vXvamTwmZ94Da/tEnB1Pwi1QS+J5jvboo0ePaymtaxK4PeBNhyrOG9isPtLB99SHHfO+KqurXytbQNNzrSmfWPNbnWbuK/wz11xo871uk/cO9iIiTv0WLEs53+yIecAV7UzsdluhGdHtpFtfQSDlw6PkW7eMylMYB+dXYdMCwqZNN3RwiXgHjJCuM1mti/ZJpYklXNsaPj9Foe/2MIpciPKxfca7T/A78+aZJcXjsSbaySbig2cDCI8v5caWpw/1QjiA6pfpRFIwBVsVk2hoSfCxK+J1osjxcyeUP5s1Gj/s8qhzetY0jbEqhsvAPCRts979LsWqWC/UDYg2bRA0D1S2m4rKtpE1HS8OLMNba5Bhu5fjhcIBAKBBFgJuAKBQCAQCLYKuKVoQSAQCAQCCbgCgUAgEEjAFQgEAoFA4BRwS3mGKxAIBAKBVLgCgUAgEJxGhSsBVyAQCASCHVS40qUsEAgEAoFUuAKBQCAQnEbAFSUIBAKBQNB/wJVBUwKBQCAQ7KLClYArEAgEAoEEXIFAcAh4JyoQCCTgCgSC/pEAWACI6XMuKhEIJOAKBIJ+MARwR/+nog6BoBn+fwCF+qYroCF8HwAAAABJRU5ErkJggg==') no-repeat center center;
                width: 100%;
                height: 70px;
            }

            .cds h1 {
                font-size: 12pt;
            }
            .cds .footer {
                text-align: center;
                margin-top: 45px;
                color: #ababab;
                font-size: 8pt;
            }
            .cds .header {
                margin-top: 45px;
            }
            a{
                color: #0078dc;
                text-decoration: none;
            }
            ul{
                list-style: none;
            }
        </style>
    </head>
    <body>
        <section class="cds">
            <section class="logo">&nbsp;</section>

            <section class="header">
                <h1>Account activation</h1>
            </section>

            <section class="content">
                <p>Hi, <?= $user->firstname . " " . $user->lastname . " (" . $user->username . ")"?>!</p>
                <p>You've received this message because you've requested a password reset. If you think this is a mistake, then please login with your old password.</p>
                <p>
                    Account information:
                    <ul>
                        <li><strong>Username:</strong> <?= $user->username?></li>
                    </ul>
                </p>
                <p>
                    To reset your password please click here <br/> <a href="<?= URL::asset('/recover/' . $url); ?>"> <?= URL::asset('/recover/' . $url);?> </a><br/>or copy the URL in your browser address bar.
                </p>
            </section>

            <section class="footer">
                2013 - 2014 &copy; Cloud Developer Studio
            </section>
        </section>
    </body>
</html>