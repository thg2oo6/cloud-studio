<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Welcome @ Cloud Developer Studio</title>

    <script src="<?= URL::asset("/front/scripts/jquery.js");?>"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css' />

    <script src="<?= URL::asset("/front/scripts/bootstrap.js");?>"></script>
    <script src="<?= URL::asset("/front/scripts/cycle.js");?>"></script>
    <link href="<?= URL::asset("/front/css/bootstrap.css");?>" type="text/css" rel="stylesheet" />
    <link href="<?= URL::asset("/front/css/theme.css");?>" type="text/css" rel="stylesheet" />
    
    <script type="text/javascript" src="<?= URL::asset("/front/scripts/jquery.mousewheel.js");?>"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?= URL::asset("/front/scripts/fancybox/jquery.fancybox.js");?>"></script>
    <link href="<?= URL::asset("/front/scripts/fancybox/jquery.fancybox.css");?>" type="text/css" rel="stylesheet" />

    <?= HTML::style('/front/css/landing.css') ?>
</head>
<body>
    <section class="top-header">
        <section class="container">
            <section class="row">
                <section class="col-md-2">
                    <a href="<?= URL::route('home') ?>" class="cds" title="Cloud Developer Studio">Cloud Developer Studio</a>
                </section>
                <section class="col-md-6 pull-right">
                    <ul class="menu">
                        <li class="active"><a href="#home">home</a></li>
                        <li><a href="#howto">how to</a></li>
                        <li><a href="#screen">screenshots</a></li>
                        <li><a href="http://blog.cloud-studio.ro">blog</a></li>
                        <?php if(Auth::check()):?>
                        <li><a href="<?= URL::route('dashboard') ?>">profile</a></li>
                        <?php else: ?>
                        <li><a href="<?= URL::route('login') ?>">login</a></li>
                        <?php endif; ?>
                    </ul>
                </section>
            </section>
        </section>
    </section>
    <section class="slider">
        <section class="container cycle-slideshow"
                 data-cycle-slides="> section"
                 data-cycle-timeout="4000"
                 data-cycle-pager=".mypager .pagination"
                 data-cycle-pager-template="<li><a>{{slideNum}}</a></li>"
                 data-cycle-pager-active-class="active"
                 >
            <section class="row slide" id="slide1">
                <section class="col-md-12">
                    <section class="pull-left cloud">
                        <span class="the-icon"></span>
                    </section>
                    <section class="pull-right text">
                        <h1>Cloud Solution</h1>
                        <p>Cloud Developer Studio offers you a simple solution to write and deploy WebApps in Cloud.</p>
                    </section>
                    <section class="clearfix"></section>
                </section>
            </section>
            <section class="row slide" id="slide2">
                <section class="col-md-12">
                    <section class="pull-right design">
                        <span class="the-icon"></span>
                    </section>
                    <section class="pull-left text">
                        <h1>Design, Code &amp; Deploy</h1>
                        <p>Design the user interface for the application, write code for it and finally deploy it. Your clients will be able to access the application from all over the world.</p>
                    </section>
                    <section class="clearfix"></section>
                </section>
            </section>
            <section class="row slide" id="slide3">
                <section class="col-md-12">
                    <section class="pull-left platform">
                        <span class="the-icon"></span>
                    </section>
                    <section class="pull-right text">
                        <h1>Mobile, Tablet and PC</h1>
                        <p>The user interface is designed to be responsive and work on any platform: Mobile, Tablet and PC.</p>
                    </section>
                    <section class="clearfix"></section>
                </section>
            </section>
        </section>
    </section>
    <section class="mypager">
        <ul class="pagination">
        </ul>
    </section>
    <section class="container" id="home">
        <section class="row">
            <section class="col-md-6">
                <h1>What is Cloud Developer Studio</h1>
                <p>Cloud Developer Studio is an <em>Integrated Development Environment (IDE)</em> that offers you a simple solution to build WebApps in Cloud with only a few mouse clicks. Besides having a <strong>Code Editor</strong><sup>1</sup>, CDS comes with a <strong>UI Builder</strong> that lets you create easily a user interface for your application. You can also deploy your application on our servers without any effort.</p>
                <p>CDS currently supports the following technologies: HTML, CSS, Javascript, PHP (to run on the servers). Alongside this we support any technology that provides a REST adapter, some of these being: OpenEdge (11.2+), C#, Ruby or Node. Databases adapters are not currently provided by CDS, but efforts are put in to make them available to our users.</p>
                <p><sup>1</sup>We are using <a href="http://ace.c9.io" target="_blank">ACE</a> as the code editor.</p>
            </section>
            <section class="col-md-offset-1 col-md-5">
                <h1>Features</h1>
                <ul>
                    <li>Code editor &amp; UI builder</li>
                    <li>Cloud based application</li>
                    <li>Cross-Platform solution</li>
                    <li>Application hosting</li>
                    <li>Support for REST adapters</li>
                    <li>Responsive UI – same UI can be used on Mobile, Tablet or PC</li>
                </ul>
                <?php //<h1>Contribute</h1> ?>
                <?php //<p>CDS is an open-source application, the code being available <a href="https://bitbucket.org/thg2oo6/cloud-developer-studio" target="_blank">on Bitbucket</a>. You can help with the development of CDS by contributing with fixes to existing issues or by reporting new ones.</p> ?>
            </section>
        </section>
    </section>
    
    <section class="container" id="howto">
        <section class="row">
            <section class="col-md-6">
                <h1>How to</h1>
            </section>
        </section>
        <section class="row">
            <section class="col-md-4">
                <h2>How can I use it</h2>
                <p>You first need to register. The option to do so is available in the upper right corner, in the login submenu or by clicking <a href="<?= URL::route('register')?>">here</a>. After you have registered and logged in, you may create your first application. These are all the required steps to begin your experience with CDS. A more detailed tutorial is available over in the <a href="<?= /*URL::route('help')*/ "#" ?>">Help</a> section.</p>
            </section>
            <section class="col-md-4">
                <h2>What can I do with it</h2>
                <p>You can build any type of application, such as a website or service. CDS gives you the freedom to decide what type of application you want to build with what kind of tools you want. Check the <a href="<?= /*URL::route('blog')*/ "#"?>" target="_blank">blog</a> for more information regarding new tools and features that will be introduced in CDS.</p>
            </section>
            <section class="col-md-4">
                <h2>How can others see my application</h2>
                <p>When you think that your application is ready to be presented to the public, you may press the deployment button from the IDE and the application shall become available at the following address: <a href="http://cloud-studio.ro:2080/" target="_blank">http://cloud-studio.ro:2080/</a>. We are working hard to support subdomains and domain redirection.</p>
            </section>
        </section>
    </section>
    
    <section class="container" id="screen">
        <section class="row">
            <section class="col-md-12">
                <h1>Screenshots</h1>
            </section>
        </section>
        <section class="row">
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_1.jpg" title="login form"><img src="front/screens/cds_1.jpg" width="100%" /></a>
                login form
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_2.jpg" title="registration form"><img src="front/screens/cds_2.jpg" width="100%" /></a>
                registration form
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_3.jpg" title="dashboard"><img src="front/screens/cds_3.jpg" width="100%" /></a>
                dashboard
            </section>
        </section>
        <section class="row">
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_5.jpg" title="project creation form"><img src="front/screens/cds_5.jpg" width="100%" /></a>
                project creation form
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_4.jpg" title="project information"><img src="front/screens/cds_4.jpg" width="100%" /></a>
                project information
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_6.jpg" title="ide dashboard"><img src="front/screens/cds_6.jpg" width="100%" /></a>
                ide dashboard
            </section>
        </section>
        <section class="row">
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_10.jpg" title="top menu"><img src="front/screens/cds_10.jpg" width="100%" /></a>
                top menu
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_9.jpg" title="code editor"><img src="front/screens/cds_9.jpg" width="100%" /></a>
                code editor
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_7.jpg" title="ui builder"><img src="front/screens/cds_7.jpg" width="100%" /></a>
                ui builder
            </section>
        </section>
        <section class="row">
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_8.jpg" title="object properties"><img src="front/screens/cds_8.jpg" width="100%" /></a>
                object properties
            </section>
            <section class="col-md-4">
                <a rel="screenshots" href="front/screens/cds_11.jpg" title="object actions"><img src="front/screens/cds_11.jpg" width="100%" /></a>
                object actions
            </section>
        </section>
    </section>
    
    <section class="the-footer">
            2014 © Cloud Developer Studio - powered by <a href="http://www.duricu.ro" target="_blank">Spark</a>
    </section>
    <script src="<?= URL::asset("/front/scripts/landing.js");?>"></script>
</body>
</html>