<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Register @ Cloud Developer Studio</title>

    <script src="<?= URL::asset("/front/scripts/jquery.js");?>"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css' />

    <script src="<?= URL::asset("/front/scripts/bootstrap.js");?>"></script>
    <link href="<?= URL::asset("/front/css/bootstrap.css");?>" type="text/css" rel="stylesheet" />
    <link href="<?= URL::asset("/front/css/theme.css");?>" type="text/css" rel="stylesheet" />

    {{ HTML::style('/front/css/login.css') }}
</head>
<body>
    <section class="wrapper register">
        <header class="mainHeader">
            <a href="{{ URL::route('home') }}" class="cds" title="Cloud Developer Studio">Cloud Developer Studio</a>
        </header>
        
        <section class="inputs">
            @if (!Config::get("spark.disable_register"))
            <form action="{{ URL::route('register') }}" method="POST" id="RegisterAction">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <ul>
                    <li>If all goes well, you will be redirected to login!<br/>Also, you will receive an activation email!</li>
                    <li class="msg error"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;<span id="errorMsg"></span></li>
                    <li class="input">
                        <input type="text" name="username" placeholder="username" />
                        <span class="clear"></span>
                    </li>
                    <li class="input">
                        <input type="email" name="email" placeholder="email" />
                        <span class="clear"></span>
                    </li>
                    <li class="input">
                        <input type="password" name="password" placeholder="password" />
                        <span class="clear"></span>
                    </li>
                    <li class="input">
                        <input type="password" name="cpassword" placeholder="confirm password" />
                        <span class="clear"></span>
                    </li>
                    <li class="input">
                        <input type="text" name="firstname" placeholder="first name" />
                        <span class="clear"></span>
                    </li>
                    <li class="input">
                        <input type="text" name="lastname" placeholder="last name" />
                        <span class="clear"></span>
                    </li>
<!--                    <li class="input">
                        Invitation
                        <input type="text" name="invitation" placeholder="Invitation" />
                        <span class="clear"></span>
                    </li>-->
                    <li class="buttons">
                        <section class="recover"><a href="{{ URL::route('login') }}" id="login">login</a></section>
                        <section class="login">
                            <button type="submit"><span class="glyphicon glyphicon-log-in"></span> Register</button>
                        </section>
                        <span class="clear"></span>
                    </li>
                </ul>
            </form>
            @else
                Registers are currently disabled.            
            @endif
        </section>
        
        <footer class="mainFooter">
            2014 &copy; Cloud Developer Studio.
        </footer>
    </section>
    
    @if (!Config::get("spark.disable_register"))
        <script src="<?= URL::asset("/front/scripts/register.js");?>"></script>
    @endif
</body>
</html>