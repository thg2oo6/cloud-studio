<?php

namespace Spark\Project;
use Spark\Base\Object;
use Spark\FileSystem\FolderOperation;

/**
 * Description of Project
 *
 * @author Valentin
 */
class Project extends Object{
    public static function Create(\Project $project, \User $user){
        FolderOperation::CreateFolder($project->slug, "/" . $user->username);
    }
    
    public static function Rename($newname, \Project $project, \User $user){
        $deploy = Deployment::undeploy($project, $user);
        FolderOperation::RenameFolder($project->slug, "/" . $user->username, $newname);
        if($deploy)
            Deployment::deploy_path($newname, $user);
    }
    
    public static function Delete(\Project $project, \User $user){
        $deploy = Deployment::undeploy($project, $user);
        FolderOperation::DeleteFolder($project->slug, "/" . $user->username);
    }
    
    public static function Contents(\Project $project, \User $user){
        return FolderOperation::ReadFolder($project->slug, "/" . $user->username);
    }

    public static function Copy(\Project $oldProject, \User $user, \Project $project){
        FolderOperation::CloneFolder($oldProject->slug, "/" . $oldProject->user->username, $project ->slug, "/" . $user->username);
    }
        
}
