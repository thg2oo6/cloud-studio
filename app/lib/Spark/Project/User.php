<?php

namespace Spark\Project;
use Spark\Base\Object;
use Spark\FileSystem\FolderOperation;
use Spark\FileSystem\FileOperation;

/**
 * Description of User
 *
 * @author Valentin
 */
class User extends Object {
    public static function CreateWorkspace(\User $user){
        FolderOperation::CreateFolder($user->username, '/');
        FileOperation::CreateFile('index.php', '/' . $user->username);
        FileOperation::WriteFile(self::DefaultFile(), 'index.php', '/' . $user->username);
        
        $userdir = FolderOperation::FullPath($user->username, '/');
        $link = FolderOperation::FullPath($user->username, '/', 'testing');
        $deployment = FolderOperation::FullPath($user->username, '/', 'deployment');
        
        symlink($userdir, $link);
        FolderOperation::CopyFolder($userdir,$deployment);
    }
    
    public static function DeleteWorkspace(\User $user){
        FolderOperation::DeleteFolder($user->username, '/');
        FolderOperation::DeleteFolder($user->username, '/', 'deployment');
        
        $link = FolderOperation::FullPath($user->username, '/', 'testing');
        unlink($link);
    }
    
    protected static function DefaultFile(){
        $contents = "";
        
        $contents .= "<?php\n";
        $contents .= "require_once('".\Config::get("spark.special.tools")."/workspace.php');\n";
        
        return $contents;
    }
}
