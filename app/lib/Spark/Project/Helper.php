<?php

namespace Spark\Project;

use Spark\Base\Object;
use Spark\FileSystem\FolderOperation;
use Spark\FileSystem\FileOperation;

/**
 * Description of Project
 *
 * @author Valentin
 */
class Helper extends Object
{
    protected $project;
    protected $user;
    protected $base_path;

    public function __construct(\Project $project, \User $user)
    {
        $this->project = $project;
        $this->user = $user;

        $this->init();
    }

    protected function init()
    {
        $this->base_path = '/' . $this->user->username . '/' . $this->project->slug . '/';
    }

    public function Delete($resource, $path)
    {
        $resource = str_replace(" ", "_", $resource);
        $the_file = FileOperation::ComposeFile($resource, $this->base_path . $path);

        if (filetype($the_file) == "dir")
            $this->DeleteFolder($resource, $path);
        else
            if (stristr($resource, ".app.php") !== false) {
                $dir_name = substr($resource, 0, stripos($resource, ".php"));

                $this->DeleteFile($resource, $path);
                $this->DeleteFolder($dir_name, $path);
            } else
                $this->DeleteFile($resource, $path);
    }

    public function Rename($resource, $path, $newName)
    {
        $resource = str_replace(" ", "_", $resource);
        $the_file = FileOperation::ComposeFile($resource, $this->base_path . $path);

        if (filetype($the_file) == "dir")
            $this->RenameFolder($resource, $path, $newName);
        else
            if (stristr($resource, ".app.php") !== false) {
                $dir_name = substr($resource, 0, stripos($resource, ".php"));
                if (stristr($newName, ".app.php") === false) {
                    $exp = explode(".", $newName);
                    $newName = $exp[0] . ".app.php";
                }
                $dir_name2 = substr($newName, 0, stripos($newName, ".php"));

                //var_dump($resource, $dir_name, $dir_name2);
                $this->RenameFile($resource, $path, $newName);

                $data = FileOperation::ReadFilePath("base.window", \Config::get("spark.special.tools"));

                $data = str_replace("{{base_file}}", $dir_name2, $data);
                $this->WriteFile($data, $newName, $path);

                $this->RenameFolder("." . $dir_name, $path, "." . $dir_name2);
            } else
                $this->RenameFile($resource, $path, $newName);
    }

    public function Get($resource, $path)
    {
        $resource = str_replace(" ", "_", $resource);
        //$the_file = FileOperation::ComposeFile($resource, $this->base_path . $path);

        if (stristr($resource, ".app.php") !== false) {
            $dir_name = substr($resource, 0, stripos($resource, ".php"));
            $arr = $this->ReadFile($resource, $path);
            $arr["content"] = [
                "window" => $this->ReadFile("window.html", $path . "/." . $dir_name),
                "style" => $this->ReadFile("style.css", $path . "/." . $dir_name),
                "script" => $this->ReadFile("script.js", $path . "/." . $dir_name),
            ];
            return $arr;
        } else
            return $this->ReadFile($resource, $path);
    }

    public function Save($resource, $path, $content, $type)
    {
        $resource = str_replace(" ", "_", $resource);
        //$the_file = FileOperation::ComposeFile($resource, $this->base_path . $path);

        if (stristr($resource, ".app.php") !== false) {
            $dir_name = substr($resource, 0, stripos($resource, ".php"));
            $ctnt = json_decode($content);
            $this->WriteFile($ctnt->window, "window.html", $path . "/." . $dir_name);
            $this->WriteFile($ctnt->style, "style.css", $path . "/." . $dir_name);
            $this->WriteFile($ctnt->script, "script.js", $path . "/." . $dir_name);
        } else
            return $this->WriteFile($content, $resource, $path);
    }

    public function CreateWindow($resource, $path)
    {
        $resource = str_replace(" ", "_", $resource);
        //$the_file = FileOperation::ComposeFile($resource, $this->base_path . $path);

        $dir_name = substr($resource, 0, stripos($resource, ".php"));
        $this->CreateFolder("." . $dir_name, $path);
        $this->CreateFile("window.html", $path . "/." . $dir_name);
        $this->CreateFile("style.css", $path . "/." . $dir_name);
        $this->CreateFile("script.js", $path . "/." . $dir_name);
        $this->CreateFile($resource, $path);

        $data = FileOperation::ReadFilePath("base.window", \Config::get("spark.special.tools"));

        $data = str_replace("{{base_file}}", $dir_name, $data);
        $this->WriteFile($data, $resource, $path);

        $data = FileOperation::ReadFilePath("window.html", \Config::get("spark.special.tools"));
        $this->WriteFile($data, "window.html", $path . "/." . $dir_name);
    }

    public function CreateFolder($folder, $path)
    {
        $folder = str_replace(" ", "_", $folder);
        FolderOperation::CreateFolder($folder, $this->base_path . $path);
    }

    public function DeleteFolder($folder, $path)
    {
        $folder = str_replace(" ", "_", $folder);
        FolderOperation::DeleteFolder($folder, $this->base_path . $path);
    }

    public function RenameFolder($oldName, $path, $newName)
    {
        $oldName = str_replace(" ", "_", $oldName);
        $newName = str_replace(" ", "_", $newName);
        FolderOperation::RenameFolder($oldName, $this->base_path . $path, $newName, $this->base_path . $path);
    }

    public function CreateFile($filename, $path)
    {
        $filename = str_replace(" ", "_", $filename);

        FileOperation::CreateFile($filename, $this->base_path . $path);
    }

    public function WriteFile($content, $filename, $path)
    {
        $filename = str_replace(" ", "_", $filename);
        if (!FileOperation::ExistsFile($filename, $this->base_path . $path))
            $this->CreateFile($filename, $path);

        FileOperation::WriteFile($content, $filename, $this->base_path . $path);
    }

    public function ReadFile($filename, $path)
    {
        $filename = str_replace(" ", "_", $filename);

        return FileOperation::ReadFile($filename, $this->base_path . $path);
    }

    public function DeleteFile($filename, $path)
    {
        $filename = str_replace(" ", "_", $filename);

        FileOperation::DeleteFile($filename, $this->base_path . $path);
    }

    public function RenameFile($oldname, $path, $newname)
    {
        $oldname = str_replace(" ", "_", $oldname);
        $newname = str_replace(" ", "_", $newname);

        FileOperation::RenameFile($oldname, $this->base_path . $path, $newname, $this->base_path . $path);
    }
}
