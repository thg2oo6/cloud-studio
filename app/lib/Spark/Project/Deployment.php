<?php
namespace Spark\Project;
use Spark\Base\Object;
use Spark\FileSystem\FileOperation;
use Spark\FileSystem\FolderOperation;

/**
 * Description of Deployment
 *
 * @author Valentin
 */
class Deployment extends Object{
    public static function deploy(\Project $project, \User $user){
        $project_path = FolderOperation::FullPath($project->slug, '/' . $user->username);
        $deployment_path = FolderOperation::FullPath($project->slug, '/' . $user->username, 'deployment');
        
        FolderOperation::CopyFolder($project_path, $deployment_path);

        $act = new \Activity();
        $act->user_id = $user->id;
        $act->project_id = $project->id;
        $act->activity = "Project " . $project->name . " has been deployed.";
        $act->save();
    }
    
    public static function deploy_path($project, \User $user){
//        $project_path = FolderOperation::FullPath($project, '/' . $user->username);
//        $deployment_path = FolderOperation::FullPath($project, '/' . $user->username, 'deployment');
//
//        FolderOperation::CopyFolder($project_path, $deployment_path);
    }
    
    public static function undeploy(\Project $project, \User $user){
        if(FolderOperation::ExistsFolder($project->slug, '/' . $user->username, 'deployment')){
            $act = new \Activity();
            $act->user_id = $user->id;
            $act->project_id = $project->id;
            $act->activity = "Project " . $project->name . " has been undeployed.";
            $act->save();

            FolderOperation::DeleteFolder($project->slug, '/' . $user->username, 'deployment');
            return true;
        }
        return false;
    }

    public static function prepare(\Project $project, \User $user){
        $deployment_path = FolderOperation::FullPath($project->slug, '/' . $user->username, 'deployment');

        FolderOperation::CompressFolder($deployment_path, $project->slug . md5($project->slug . '|' . $user->username . "|" . $project->id));
    }

    public static function download(\Project $project, \User $user){
        return FileOperation::DownloadFile($project->slug . md5($project->slug . '|' . $user->username . "|" . $project->id));
    }
}
