<?php

namespace Spark\System;

use Spark\Base\Object;

/**
 * Description of Deployment
 *
 * @author Valentin
 */
class DumpDB extends Object
{
    private $tables = [];
    private $schema;
    private $maxPriority = 0;
    private $file;
    private $filename;

    private function __construct()
    {
        $this->schema = \Config::get("database.connections." . \Config::get("database.default") . ".database");
        \Log::info("Dumping tables from schema: " . $this->schema);
        $this->filename = \Config::get("spark.special.tools") . "/../tmp/" . $this->genFilename() . ".sql";
    }

    public static function run()
    {
        $obj = new DumpDB();

        $obj->getTableList();
        $obj->getRelations();
        $obj->setPriorities();
        $obj->sortPriorities();

        $obj->openFile();

        $obj->dropTables();
        $obj->reverse();
        $obj->createTables();

        $obj->closeFile();

        return $obj->filename;
    }

    private function getTableList()
    {
        $this->tables = [];

        $results = \DB::select("SELECT `table_name` FROM information_schema.tables WHERE table_schema = '" . $this->schema . "';");

        foreach ($results as $v) {
            $this->tables[$v->table_name] = new Table($v->table_name);
            \Log::info("Creating table entity: " . $v->table_name);
        }
    }

    private function getRelations()
    {
        $results = \DB::select("SELECT DISTINCT `table_name`, `referenced_table_name` FROM information_schema.key_column_usage WHERE referenced_table_schema = '" . $this->schema . "' and referenced_table_name IS NOT NULL and referenced_table_name <> table_name;");

        foreach ($results as $v) {
            $tableName = $v->table_name;
            $reference = $v->referenced_table_name;

            $this->tables[$tableName]->addParent($reference);
            \Log::info("Getting relations for table: " . $tableName);
        }

    }

    private function setPriorities()
    {
        foreach ($this->tables as $k => $v) {
            \Log::info("Setting priority for table: " . $v->getName());
            $priority = $this->getPriority($v->getName());

            if ($priority > $this->maxPriority) $this->maxPriority = $priority;

            $this->tables[$k]->setPriority($priority);
        }
    }

    private function getPriority($sTableName)
    {
        $priority = 0;

        $table = $this->tables[$sTableName];

        foreach ($table->getParents() as $v) {
            $aux = $this->getPriority($v) + 1;
            if ($priority < $aux)
                $priority = $aux;
        }

        return $priority;

    }

    private function sorter($a, $b)
    {
        if ($a->getPriority() > $b->getPriority())
            return -1;

        return $a->getPriority() == $b->getPriority() ? 0 : 1;
    }

    private function sortPriorities()
    {
        uasort($this->tables, array($this, 'sorter'));
    }

    private function reverse()
    {
        $this->tables = array_reverse($this->tables);
    }

    private function dropTables()
    {
        \Log::info("Dropping tables");

        $s = "";
        foreach ($this->tables as $k => $v) {
            $s .= ", " . $k;
        }

        fprintf($this->file, "-- BEGIN DROP TABLES\n");
        fprintf($this->file, "DROP TABLE IF EXISTS %s;\n", substr($s, 2));
        fprintf($this->file, "-- END DROP TABLES\n\n");
    }

    private function createTables()
    {
        \Log::info("Creating tables");

        foreach ($this->tables as $k => $v) {
            \Log::info("Creating table: " . $k);
            $results = \DB::select("SHOW CREATE TABLE " . $k);

            fprintf($this->file, "-- BEGIN CREATE TABLE\n");
            fprintf($this->file, "%s\n\n", $results[0]->{'Create Table'});

            \Log::info("Dummping data from table: " . $k);
            $results = \DB::select("SELECT column_name FROM information_schema.columns WHERE table_schema='" . $this->schema . "' AND table_name='" . $k . "';");

            $s = "";
            foreach ($results as $r) {
                $s .= ", " . $r->column_name;
            }

            fprintf($this->file, "INSERT INTO `%s` (%s) VALUES\n", $k, substr($s, 2));

            $results = \DB::select("SELECT * FROM " . $k . ";");
            $n = count($results);
            $i = 0;

            foreach ($results as $r) {
                $i++;

                $s = "";
                foreach ($r as $col) {
                    if (is_null($col))
                        $s .= ", null";
                    elseif (is_int($col))
                        $s .= ", " . $col;
                    else
                        $s .= ", \"" . $col . "\"";
                }

                fprintf($this->file, "(%s)%s\n", substr($s, 2), $i == $n ? ";" : ",");
            }

            fprintf($this->file, "-- END CREATE TABLE\n\n");
        }
    }

    private function genFilename()
    {
        return date("YmdHis") . "-backup";
    }

    private function openFile()
    {
        $this->file = fopen($this->filename, "w");
    }

    private function closeFile()
    {
        fclose($this->file);
    }
}
