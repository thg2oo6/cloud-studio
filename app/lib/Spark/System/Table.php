<?php

namespace Spark\System;

use Spark\Base\Object;

/**
 * Description of Deployment
 *
 * @author Valentin
 */
class Table extends Object {
	private $table_name;
	private $priority;
	private $parents;
	
	public function __construct($tableName){
		$this->table_name = $tableName;
		
		$this->parents = [];
		$this->priority = 0;
	}
	
	public function getName(){ return $this->table_name; }
	public function getPriority(){ return $this->priority; }
	public function setPriority($priority){ $this->priority = $priority; }
	public function addParent($parent){ $this->parents[] = $parent; }
	public function getParents(){ return $this->parents; }

}
