<?php

namespace Spark\System;

use Spark\Base\Object;

/**
 * Description of Deployment
 *
 * @author Valentin
 */
class Authorization extends Object {

    public static function allowed($right) {
        try {
            $authorization = \LevelAuthorization::where("slug", "=", $right)->firstOrFail();

            return $authorization->level <= \Auth::user()->level;
        } catch (\Exception $ex) {
            return true;
        }
    }

}
