<?php
namespace Spark\FileSystem;
use Spark\Base\Object;

//TODO: Document the class
class FileOperation extends Object{
    public static function ReadFile($filename, $path){
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $path . "/" . $filename);
        
        if(!file_exists($file))
            throw new FileException("Invalid file requested");
        
        $info = pathinfo($file);
        $array = [];
        $basename = basename($file);
        $array["content"] = file_get_contents($file);
        $array["lang"] = isset($info['extension'])?self::lang($info['extension']):'txt';
        $array["basepath"] = str_replace("//", "/", substr($file, 0, strlen($file) - strlen($basename)));
        $array["basename"] = $basename;
        $array["path"] = substr($filename, 0, strlen($filename) - strlen($basename));
                
        return $array;
    }

    public static function ReadFilePath($filename, $path){
        $file = str_replace("\\", "/", $path . "/" . $filename);

        if(!file_exists($file))
            throw new FileException("Invalid file requested");

        return file_get_contents($file);
    }
    
    public static function WriteFile($content, $filename, $path){
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $path . "/" . $filename);
        
        if(!file_exists($file))
            throw new FileException("Invalid file requested");
        
        return file_put_contents($file, $content);
    }
    
    public static function CreateFile($filename, $path){
        $file = str_replace("\\", "/",\Config::get("spark.path.projects") . $path . "/" . $filename);
            
        if(file_exists($file))
            throw new FileException("File exists");
                
        touch($file);
    }
    
    public static function RenameFile($oldname, $oldpath, $newname, $newpath = ''){
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $oldpath . "/" . $oldname);
            
        if(!file_exists($file))
            throw new FileException("Invalid file requested");
        
        if(empty($newpath)) $newpath = $oldpath;
                
        $newfile = str_replace("\\", "/", \Config::get("spark.path.projects") . $newpath . "/" . $newname);
            
        if(file_exists($newfile))
            throw new FileException("File exists");
                
        rename($file, $newfile);
    }
    
    public static function ExistsFile($filename, $path){
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $path . "/" . $filename);
            
        return file_exists($file);
    }

    public static function ComposeFile($filename, $path){
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $path . "/" . $filename);

        return $file;
    }
    
    public static function DeleteFile($filename, $path){
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $path . "/" . $filename);
            
        if(!file_exists($file))
            throw new FileException("Invalid file requested");
        
        unlink($file);
    }

    public static function DownloadFile($filename){
        $file = str_replace("\\", "/", \Config::get("spark.special.tmp") . "/" . $filename . ".tar.bz2");

        if(!file_exists($file))
            throw new FileException("Invalid file requested");

        return $file;
    }
    
    public static function lang($ext){
        switch($ext){
            case "php":     return "php"; break;
            case "js":      return "javascript"; break;
            case "json":    return "json"; break;
            case "css":     return "css"; break;
            case "less":    return "less"; break;
            case "html":    return "html"; break;
            case "py":      return "python"; break;
            case "java":    return "java"; break;
            default:
                return "text";
        }
    }
}