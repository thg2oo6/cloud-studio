<?php
namespace Spark\FileSystem;

use Spark\Base\Object;

class FolderOperation extends Object
{
    public $file_id = 0;

    private function __construct()
    {

    }

    public static function ReadFolder($folder, $path)
    {
        $ref = new self();
        $ref->file_id = 0;

        return $ref->_ReadFolder($folder, $path);
    }

    public function _ReadFolder($folder, $path)
    {
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $path . "/" . $folder);

        if (!file_exists($file))
            throw new FileException("Invalid folder requested");

        $dir = opendir($file);
        $baza = !empty($file) ? $file . "\\" : "\\";

        $array = [];
        while ($f = readdir($dir)) {
            if ($f == "." || $f == "..")
                continue;

            if (filetype($file . "/" . $f) == "dir"){
//                if(stristr($f,".app")!==false)
//                    $array[] = $this->create_object($f, 'App', '');
//                elseif(stristr($f,".mobi")!==false)
//                    $array[] = $this->create_object($f, 'Mobi', '');
//                else
                if(starts_with($f,'.'))
                    continue;
                $array[] = $this->create_object($f, 'Directory', $this->_ReadFolder($f, $path . "/" . $folder));
            }
            else
                if (stristr($f, ".app.php") !== false)
                    $array[] = $this->create_object($f, 'App', '');
                else
                    $array[] = $this->create_object($f, 'File', '');
        }

        closedir($dir);

        usort($array, array("\Spark\FileSystem\FolderOperation", "sort_dir"));
        return $array;
    }

    public static function sort_dir($a, $b)
    {
        if ($a['type'] != $b['type']) {
            if ($a['type'] == "directory")
                return -1;
            else
                return 1;
            return 0;
        } else
            if ($a['file'] > $b['file'])
                return 1;
            else if ($a['file'] < $b['file'])
                return -1;
        return 0;
    }

    private function create_object($name, $type, $content)
    {
        $array = [];

        $array['id'] = ++$this->file_id;
        $array['file'] = $name;
        $array['type'] = strtolower($type);
        if ($type == "Directory") {
            $array['content'] = $content;
        }

        return $array;
    }

    public static function CreateFolder($foldername, $path, $zone = 'projects')
    {
        $file = str_replace("\\", "/", \Config::get("spark.path." . $zone) . $path . "/" . $foldername);

        if (file_exists($file))
            throw new FileException("Folder exists");

        mkdir($file);
    }

    public static function RenameFolder($oldname, $oldpath, $newname, $newpath = '')
    {
        $file = str_replace("\\", "/", \Config::get("spark.path.projects") . $oldpath . "/" . $oldname);

        if (!file_exists($file))
            throw new FileException("Invalid folder requested");

        if (empty($newpath)) $newpath = $oldpath;

        $newfile = str_replace("\\", "/", \Config::get("spark.path.projects") . $newpath . "/" . $newname);

        if (file_exists($newfile))
            throw new FileException("Folder exists");

        rename($file, $newfile);
    }

    public static function ExistsFolder($foldername, $path, $zone = 'projects')
    {
        $file = str_replace("\\", "/", \Config::get("spark.path." . $zone) . $path . "/" . $foldername);

        return file_exists($file);
    }

    public static function DeleteFolder($foldername, $path, $zone = 'projects')
    {
        $file = str_replace("\\", "/", \Config::get("spark.path." . $zone) . $path . "/" . $foldername);

        if (!file_exists($file))
            throw new FileException("Invalid folder requested");

        if (filetype($file) != "dir")
            throw new FileException("{$foldername} is not a folder");

        self::recursive_delete($file);
        rmdir($file);
    }

    protected static function recursive_delete($folder)
    {
        $dir = opendir($folder);

        while ($f = readdir($dir)) {
            if ($f == "." || $f == "..")
                continue;

            if (filetype($folder . "/" . $f) == "dir") {
                self::recursive_delete($folder . "/" . $f);
                rmdir($folder . "/" . $f);
            } else
                unlink($folder . "/" . $f);
        }

        closedir($dir);
    }

    public static function CloneFolder($oldFolderName, $oldPath, $newFolderName, $newPath){
        $fileOld = str_replace("\\", "/", \Config::get("spark.path.projects") . $oldPath . "/" . $oldFolderName);
        $fileNew = str_replace("\\", "/", \Config::get("spark.path.projects") . $newPath . "/" . $newFolderName);

        self::CopyFolder($fileOld, $fileNew);
    }

    public static function CopyFolder($source, $dest)
    {
        if (!file_exists($source))
            throw new FileException("Invalid folder requested");

        if (filetype($source) != "dir")
            throw new FileException("{$source} is not a folder");

        if (file_exists($dest)) {
            //throw new FileException("Destination already exists");
            self::recursive_delete($dest);
            rmdir($dest);
        }

        mkdir($dest);
        self::recursive_copy($source, $dest);
    }

    protected static function recursive_copy($source, $dest)
    {
        $dir = opendir($source);

        while ($f = readdir($dir)) {
            if ($f == "." || $f == "..")
                continue;

            if (filetype($source . "/" . $f) == "dir") {
                mkdir($dest . "/" . $f);
                self::recursive_copy($source . "/" . $f, $dest . "/" . $f);
            } else
                copy($source . "/" . $f, $dest . "/" . $f);
        }

        closedir($dir);
    }

    public static function CompressFolder($source, $filename)
    {
        if (!file_exists($source))
            throw new FileException("Invalid folder requested");

        if (filetype($source) != "dir")
            throw new FileException("{$source} is not a folder");

        system("tar cfj " . \Config::get("spark.special.tmp") . "/" . $filename . ".tar.bz2 " . $source);
    }

    public static function FullPath($foldername, $path, $zone = "projects")
    {
        $file = str_replace("\\", "/", \Config::get("spark.path." . $zone) . $path . "/" . $foldername);
        return $file;
    }
}