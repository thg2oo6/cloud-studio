<?php

namespace Spark\Base;

/**
 * Description of About
 *
 * @author Valentin
 */
class About extends Object{
    const MAJOR = 0;
    const MINOR = 4;
    const SPRINT = 3;

    const DISABLE_REGISTER = false;
    
    public static function version($separator = "."){
        return self::MAJOR . $separator . self::MINOR . $separator . self::SPRINT;
    }
}
