<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as'=>'home', function(){
    return View::make('index');
}));
Route::get('login', array('as'=>'login',function(){
    return View::make('login');
}))->before('guest');

Route::get('register', array('as'=>'register',function(){
    return View::make('register');
}))->before('guest');

Route::get('recover', array('as'=>'recover',function(){
    return View::make('recover');
}))->before('guest');

//Route::get('/blog', array('as'=>'blog', function(){
//    return View::make('blog');
//}));

Route::post('login', function () {
    $user = array(
        'username'   => Input::get('username'),
        'password'   => Input::get('password'),
        'oauth'      => 0,
        'activated'  => 1
    );

    if (Auth::attempt($user)) {
        if(Auth::user()->activation != null){
            Auth::user()->activation = null;
            Auth::user()->save();
        }
        return Redirect::intended('dashboard')
            ->with('cds_notice', 'You are successfully logged in.');
    }

    // authentication failure! lets go back to the login page
    return Redirect::route('login')
        ->with('cds_error', 'Your username/password combination was incorrect.')
        ->withInput();
})->before('csrf');
Route::post('register', 'HomeController@register')->before('csrf');
Route::post('recover', 'HomeController@recover')->before('csrf');

Route::get('activate/{hash}', function($id){
    try{
        $user = User::where("activation", $id)->firstOrFail();
        $user->activated = true;
        $user->activation = null;
        $user->save();

        return View::make("activated", array("user" => $user));
    }catch(\Exception $ex){
        return View::make("404", array("message" => "Invalid activation code."));
    }
})->before("guest");

Route::get('recover/{hash}', function($id){
    try{
        $user = User::where("activation", $id)->firstOrFail();

        return View::make("recovery", array("user" => $user));
    }catch(\Exception $ex){
        return View::make("404", array("message" => "Invalid password recovery code."));
    }
})->before("guest");

Route::post('recover/{hash}', 'HomeController@recovery')->before("csrf");

Route::get('dashboard', array('as' => 'dashboard', function(){
    return View::make("dashboard", array("types" => ProjectType::all()));
}))->before('auth');

Route::get('logout', array('as' => 'logout', function () {
    Auth::logout();
/*     Session::forget('oauth'); */

    return Redirect::route('login')->with('cds_notice', 'You are successfully logged out.');
}))->before('auth');


/* Routes defined for admin tools. */
Route::group(array('prefix' => 'tools', 'before' => 'admin'), function()
{
    Route::get('dump','ToolsController@dumpDatabase');
    Route::get('forceUpdate', 'ToolsController@forceUpdate');
    
    $lambda = function(){
    	$error = new STDClass();
    	$error->status = "error";
		$error->message = "Invalid request";
		return Response::json($error, 404);
	};
	Route::get('',$lambda);
	Route::post('',$lambda);
});


/* Routes defined for API calls. */
Route::group(array('prefix' => 'api/master', 'before' => 'auth'), function()
{
    Route::resource('project','ProjectController');
    Route::get('publicProject','ProjectController@publicProjects');
    Route::get('files/{AppName}/contents', 'ProjectCtntController@projectContent');
    Route::post('clone/{AppName}','ProjectController@cloneProject');
    Route::post('files/{AppName}/newFile', 'ProjectCtntController@newFile');
    Route::post('files/{AppName}/newFolder', 'ProjectCtntController@newFolder');
    Route::post('files/{AppName}/rename','ProjectCtntController@renameRsrc');
    Route::post('files/{AppName}/newWindow', 'ProjectCtntController@newWindow');
    Route::post('files/{AppName}/delete', 'ProjectCtntController@deleteRsrc');
    Route::post('files/{AppName}/get', 'ProjectCtntController@getRsrc');
    Route::post('files/{AppName}/save', 'ProjectCtntController@saveRsrc');
    Route::post('files/{AppName}/prepare', 'ProjectCtntController@prepare');
    Route::get('files/{AppName}/download', 'ProjectCtntController@download');
    Route::post('files/{AppName}/deploy', 'ProjectCtntController@deploy');
    Route::post('files/{AppName}/undeploy', 'ProjectCtntController@undeploy');

    Route::post('lang', 'ProjectCtntController@getLang');

    $lambda = function(){
    	$error = new STDClass();
    	$error->status = "error";
		$error->message = "Invalid request";
		return Response::json($error, 404);
	};
	Route::get('',$lambda);
	Route::post('',$lambda);
});

Route::post('api/authenticate', 'UserController@authenticate');
Route::group(array('prefix' => 'api/v1', 'before' => 'auth.token'), function()
{
    Route::resource('project','ProjectController');
    Route::post('files/{AppName}/deploy', 'ProjectCtntController@deploy');
    Route::post('files/{AppName}/undeploy', 'ProjectCtntController@undeploy');

    $lambda = function(){
        $error = new STDClass();
        $error->status = "error";
        $error->message = "Invalid request";
        return Response::json($error, 404);
    };
	Route::get('',$lambda);
	Route::post('',$lambda);
});

Route::get('app/{AppName}', array('as' => 'app', 'uses' => 'HomeController@Application'))->before('auth');
Route::post('settings', 'UserController@settings')->before('auth');
Route::post('resetAPI', 'UserController@resetAPI')->before('auth');