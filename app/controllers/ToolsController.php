<?php

class ToolsController extends BaseController
{

    //Admin related
    public function dumpDatabase()
    {
        return Response::download(\Spark\System\DumpDB::run());
    }

    public function forceUpdate()
    {
        $this->updateUsers();
    }

    private function updateUsers()
    {
        try {
            $users = User::all();

            foreach ($users as $user) {
                $user->updateApi();
                $user->password = "parola123";
                $user->save();
            }

            return Response::json(
                ["status" => "ok"],
                200
            );
        } catch (\Exception $ex) {
            return Response::json(
                ["status" => "ERROR", "message" => "Something happened..."],
                500
            );
        }
    }

}