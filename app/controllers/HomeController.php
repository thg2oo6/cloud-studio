<?php

class HomeController extends BaseController {

	public function showWelcome()
	{
		return View::make('hello');
	}

    public function application($appname){
        try{
            $project = Project::whereRaw('slug = ? and user_id = ?',array($appname,Auth::user()->id))->get();
            if(count($project) == 0)
                throw new \Exception();

            return View::make('cds_editor', array( 'project' => $project[0] ));
        }catch(\Exception $ex){
            return View::make('404', array( 'message' => 'Project not found!' . $ex->getMessage() ));
        }
    }

    public function register(){
        try{
            $uinfo = array(
                'username'  => Input::get('username'),
                'email'     => Input::get('email'),
                'password'  => Input::get('password'),
                'cpassword' => Input::get('cpassword'),
                'firstname' => Input::get('firstname'),
                'lastname'  => Input::get('lastname'),
            );

            if(strlen($uinfo['password'])<6)
                throw new \Exception("The password should have 6 characters.");

            if($uinfo['password']!=$uinfo['cpassword'])
                throw new \Exception("The passwords should be equal.");

            foreach($uinfo as $k=>$v)
                if(strlen($v)==0)
                    throw new \Exception("Invalid length for {$k}.");

            $user = User::whereRaw('username = ? or email = ?',array($uinfo['username'],$uinfo['email']))->get();
            if(count($user->toArray())!=0)
                throw new \Exception("User or email already exists!");

            unset($uinfo['cpassword']);
//            $uinfo['password'] = Hash::make($uinfo['password']);
            $user = new User();
            $user->username = $uinfo['username'];
            $user->email = $uinfo['email'];
            $user->password = $uinfo['password'];
            $user->firstname = $uinfo['firstname'];
            $user->lastname = $uinfo['lastname'];
            $user->activation = sha1($user->username . $user->email . $user->firstname . $user->lastname);
            $user->save();

            Mail::send('activation_mail', ["user" => $user, "url" => $user->activation, "password" => $uinfo['password']], function($message) use ($user)
            {
                $message->to($user->email, $user->firstname . " " . $user->lastname)->subject('Account activation!');
            });

            return Response::json(array("status"=>"ok"),200);
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = $ex->getMessage();
            return Response::json(
                $exception,
                200
            );
        }
    }

    public function recover(){
        try{
            $uinfo = array(
                'username'  => Input::get('username'),
            );

            foreach($uinfo as $k=>$v)
                if(strlen($v)==0)
                    throw new \Exception("Invalid length for {$k}.");

            $user = User::whereRaw('username = ? or email = ?',array($uinfo['username'],$uinfo['username']))->get();
            if(count($user->toArray())==0)
                throw new \Exception("User or email doesn't exists!");
            $user = $user->get(0);

            $user->activation = sha1($user->username . $user->email . $user->firstname . $user->lastname . "recover");
            $user->save();

            Mail::send('passwordreset_mail', ["user" => $user, "url" => $user->activation], function($message) use ($user)
            {
                $message->to($user->email, $user->firstname . " " . $user->lastname)->subject('Password reset!');
            });

            return Redirect::route('recover')->with('cds_notice', 'Your password was recovered succesfully.');
        }catch(\Exception $ex){
            // authentication failure! lets go back to the login page
            return Redirect::route('recover')
                ->with('cds_error', 'Your username/email was incorrect. '. $ex->getMessage())
                ->withInput();
        }
    }

    public function recovery($hash){
        try{
            $uinfo = array(
                'password'  => Input::get('password'),
                'cpassword'  => Input::get('cpassword'),
            );

            foreach($uinfo as $k=>$v)
                if(strlen($v)==0)
                    throw new \Exception("Invalid length for {$k}.");

            if(strlen($uinfo['password']) < 6)
                throw new \Exception("Invalid length for the password.");

            if($uinfo['password'] != $uinfo['cpassword'])
                throw new \Exception("The passwords don't match.");

            $user = User::whereRaw('activation = ?',array($hash))->get();
            if(count($user->toArray())==0)
                throw new \Exception("User doesn't exists!");
            $user = $user->get(0);

            $user->password = $uinfo['password'];
            $user->activation = null;
            $user->save();

            return Redirect::route('login')->with('cds_notice', 'Your password was recovered succesfully.');
        }catch(\Exception $ex){
            // authentication failure! lets go back to the login page
            return Redirect::to('recover/'.$hash)
                ->with('cds_error', 'Something happened '. $ex->getMessage())
                ->withInput();
        }
    }

}