<?php

class ProjectController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $projects = Project::where('user_id', Auth::user()->id)->get();
        $projectList = [];

        foreach ($projects as $project) {
            $projectList[] = [
                "name" => $project->name,
                "type" => $project->projectType->name,
                "owner" => $project->user == Auth::user()  ? "yes" : "no",
                "cloned" => $project->clones != null,
                "slug" => $project->slug,
                "logo" => $project->logo,
                "created_at" => $project->created_at->diffForHumans(),
                "last_activity" => Activity::where("project_id", "=", $project->id)->orderBy("created_at", "desc")->first()->created_at->diffForHumans(),
            ];
        }

        return Response::json(
            $projectList,
            200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function publicProjects()
    {
        $projects = Project::where('user_id', "<>", Auth::user()->id)->where('visibility', '=', 1)->get();
        $projectList = [];

        foreach ($projects as $project) {
            $projectList[] = [
                "name" => $project->name,
                "type" => $project->projectType->name,
                "owner" => $project->user->username,
                "slug" => $project->slug,
                "logo" => $project->logo,
                "created_at" => $project->created_at->diffForHumans(),
                "last_activity" => Activity::where("project_id", "=", $project->id)->orderBy("created_at", "desc")->first()->created_at->diffForHumans(),
                "clonable" => $this->isUserProject($project)
            ];
        }

        return Response::json(
            $projectList,
            200
        );
    }

    private function isUserProject($project)
    {
        $projects = Project::where('user_id', "=", Auth::user()->id)->where('clones', '=', $project->id)->get();

        if (count($projects) != 0)
            return false;

        $projects = Project::where('user_id', "=", Auth::user()->id)->where('id', '=', $project->clones)->get();

        if (count($projects) != 0)
            return false;

        return true;
    }

    private function getGoodSlug($title)
    {
        $base_slug = Str::slug($title);
        $i = 2;

        $slug = $base_slug;
        try {
            $project = Project::where('slug', $slug)->firstOrFail();
            while ($project != null) {
                $slug = $base_slug . "-" . $i;
                $i++;
                $project = Project::where('slug', $slug)->firstOrFail();
            }
        } catch (\Exception $ex) {
        }

        return $slug;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $projecttype = \ProjectType::where("slug", Request::get('type'))->firstOrFail();
            $project = new Project();

            $project->user_id = Auth::user()->id;
            $project->name = Request::get('name');
            $project->slug = $this->getGoodSlug($project->name);
            $project->description = Request::get('description');
            $project->logo = Request::get('logo');

            $project->projecttype_id = $projecttype->id;
            $project->clones = Request::get("clone") == 0 ? null : Request::get('clone');
            $project->visibility = intval(Request::get("visibility"));
            $project->save();

            \Spark\Project\Project::Create($project, Auth::user());
            /*
                        if($project->projecttype_id == 3 || $project->projecttype_id == 4)
                            exec("../tools/demo_app.sh " . ($project->projecttype_id==3?"html":"php") . " " . $sys_dir);
            */

            try {
                $activity = new Activity();
                $activity->project_id = $project->id;
                $activity->user_id = Auth::user()->id;
                $activity->activity = "Created the project: '{$project->name}'";

                $activity->save();
            } catch (\Exception $ex) {

            }

            return Response::json(
                '{"message": "Project created!", "status": "OK"}',
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error creating the project! " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function cloneProject($slug)
    {
        try {
            $project0 = Project::where('user_id', '<>', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            if ($this->isUserProject($project0)) {
                throw new \Exception("Cannot clone an already cloned project!");
            }
            $project = new Project();

            $project->user_id = Auth::user()->id;
            $project->name = $project0->name;
            $project->slug = $this->getGoodSlug($project->name);
            $project->description = $project0->description;
//            $project->description = "";
            $project->logo = $project0->logo;

            $project->projecttype_id = $project0->projecttype_id;
            $project->clones = $project0->id;
            $project->visibility = $project0->visibility;
            $project->save();

            \Spark\Project\Project::Copy($project0, Auth::user(), $project);
            /*
                        if($project->projecttype_id == 3 || $project->projecttype_id == 4)
                            exec("../tools/demo_app.sh " . ($project->projecttype_id==3?"html":"php") . " " . $sys_dir);
            */

            try {
                $activity = new Activity();
                $activity->project_id = $project0->id;
                $activity->user_id = Auth::user()->id;
                $activity->activity = "User " . Auth::user()->username . " cloned the project.";

                $activity->save();
            } catch (\Exception $ex) {

            }

            try {
                $activity = new Activity();
                $activity->project_id = $project->id;
                $activity->user_id = Auth::user()->id;
                $activity->activity = "Successfully cloned cloned the project.";

                $activity->save();
            } catch (\Exception $ex) {

            }

            return Response::json(
                '{"message": "Project cloned!", "status": "OK"}',
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error creating the project! " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($slug)
    {
        try {
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();

            $projectO = [
                "name" => $project->name,
                "type" => $project->projectType->name,
                "type_slug" => $project->projectType->slug,
                "owner" => $project->user == Auth::user() ? "yes" : "no",
                "slug" => $project->slug,
                "logo" => $project->logo,
                "description" => $project->description,
                "created_at" => $project->created_at->diffForHumans(),
                "updated_at" => $project->updated_at->diffForHumans(),
                "users" => $this->userList($project),
                "activities" => $this->activityList($project),
                "visibility" => $project->visibility ? "Public" : "Private"
            ];

            return Response::json(
                $projectO,
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "No project found. " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    private function userList($project)
    {
        $userList = [];

        $userList[] = [
            "username" => $project->user->username,
            "avatar" => md5($project->user->email),
            "name" => trim($project->user->firstname . " " . $project->user->lastname)
        ];

        if (empty($userList['name']))
            $userList[0]['name'] = $userList[0]['username'];

        return $userList;
    }

    private function activityList($project)
    {
        $activityList = [];
        $activities = Activity::where("project_id", "=", $project->id)->orderBy("created_at", "desc")->get();

        foreach ($activities as $activity) {
            $activityLt = [
                "user" => $activity->user->username,
                "avatar" => md5(strtolower($activity->user->email)),
                "name" => trim($activity->user->firstname . " " . $activity->user->lastname),
                "activity" => $activity->activity,
                "when" => $activity->created_at->diffForHumans()
            ];

            if (empty($activityLt['name']))
                $activityLt['name'] = $activityLt['user'];

            $activityList[] = $activityLt;
        }

        return $activityList;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($slug)
    {
        try {
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $projecttype = \ProjectType::where("slug", Request::get('type'))->firstOrFail();

            $project2 = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $project->name = Request::get('name');
            $project->slug = $this->getGoodSlug($project->name);
            $project->description = Request::get('description');
            $project->logo = Request::get('logo');

            $project->projecttype_id = $projecttype->id;
            $project->visibility = intval(Request::get("visibility")?Request::get("visibility"):"0");
            $project->save();

            \Spark\Project\Project::Rename($project->slug, $project2, Auth::user());

            /*
                        if($project->projecttype_id == 3 || $project->projecttype_id == 4)
                            exec("../tools/demo_app.sh " . ($project->projecttype_id==3?"html":"php") . " " . $sys_dir);
            */

            try {
                $activity = new Activity();
                $activity->project_id = $project->id;
                $activity->user_id = Auth::user()->id;
                $activity->activity = "Updated the project: '{$project->name}'";

                $activity->save();
            } catch (\Exception $ex) {

            }

            return Response::json(
                '{"message": "Project updated!", "status": "OK"}',
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error updating the project! " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return Response
     */
    public function destroy($slug)
    {
        $msg = new STDClass();
        try {

            $project = Project::where('user_id', '=', Auth::user()->id)->where("slug", "=", $slug)->firstOrFail();
            Activity::where('project_id', '=', $project->id)->delete();

            \Spark\Project\Project::Delete($project, Auth::user());
            $project->delete();

            $msg->status = "ok";
            $msg->message = "Project deleted!";

            return Response::json(
                $msg,
                200
            );
        } catch (\Exception $ex) {
            $msg->status = "error";
            $msg->message = "Project not found!";
            return Response::json(
                $msg,
                500
            );
        }
    }

    public function missingMethod($parameters = array())
    {
        return "Gotta catch 'em all!";
    }

}