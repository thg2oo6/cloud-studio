<?php

class ProjectCtntController extends \BaseController {

	public function projectContent($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();


            return Response::json(
                array(
                    "tree" => \Spark\Project\Project::Contents($project,Auth::user())
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error loading the project! ";
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function newFile($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());
            $helper->CreateFile(Request::get('file'), Request::get('path'));


            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error creating the file! " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function newFolder($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());
            $helper->CreateFolder(Request::get('folder'), Request::get('path'));


            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error creating the file! ";
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function newWindow($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());
            $file = Request::get('file');
            $helper->CreateWindow($file, Request::get('path'));


            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error creating the window! " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function getLang(){
        $file = Request::get('file');
        $arr = explode('.', $file);
        $ext = end($arr);

        return Response::json(
            [
              "lang" => \Spark\FileSystem\FileOperation::lang($ext)
            ],
            200
        );
    }

    public function deleteRsrc($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());
            $helper->Delete(Request::get('file'), Request::get('path'));

            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error deleting the file!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function prepare($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            \Spark\Project\Deployment::prepare($project, Auth::user());

            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error creating the archive!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function deploy($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            \Spark\Project\Deployment::deploy($project, Auth::user());

            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error deploying the project!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function undeploy($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            \Spark\Project\Deployment::undeploy($project, Auth::user());

            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error undeploying the project!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function download($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();

            return Response::download(\Spark\Project\Deployment::download($project,Auth::user()));
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error downloading the file!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function renameRsrc($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());
            $helper->Rename(Request::get('old_name'), Request::get('path'), Request::get('new_name'));

            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error renaming the file!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function getRsrc($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());

            return Response::json(
                $helper->Get(Request::get('file'), Request::get('path')),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error renaming the file!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function saveRsrc($slug){
        try{
            $project = Project::where('user_id', '=', Auth::user()->id)->where('slug', '=', $slug)->firstOrFail();
            $helper = new \Spark\Project\Helper($project, Auth::user());

            $helper->Save(Request::get('file'), Request::get('path'), Request::get('content'), Request::get('type'));

            return Response::json(
                array(
                    "status" => "OK"
                ),
                200
            );
        }catch(\Exception $ex){
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Error renaming the file!" . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

}