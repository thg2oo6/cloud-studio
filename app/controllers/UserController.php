<?php

class UserController extends BaseController
{

    public function settings()
    {
        try {
            $user = User::where('id', '=', Auth::user()->id)->firstOrFail();
            $user_info = [
                "firstname" => Request::get('firstname'),
                "lastname" => Request::get('lastname'),
                "oldpassword" => Request::get('oldpassword'),
                "password" => Request::get('password'),
                "cpassword" => Request::get('cpassword')
            ];

            if (strlen($user_info['firstname']) == 0)
                throw new Exception("What will your first name be?");

            if (strlen($user_info['lastname']) == 0)
                throw new Exception("What will your last name be?");

            $user->firstname = filter_var($user_info['firstname'], FILTER_SANITIZE_STRING);
            $user->lastname = filter_var($user_info['lastname'], FILTER_SANITIZE_STRING);

            if (strlen($user_info['password']) != 0) {
                if (strlen($user_info['oldpassword']) == 0)
                    throw new Exception("Like... What was the old password?");

                if (!Hash::check($user_info['oldpassword'], $user->password))
                    throw new Exception("The old password must be the same as the one we already know!");

                if (Hash::check($user_info['password'], $user->password))
                    throw new Exception("The old password is the same as the new password!");

                if (strlen($user_info['cpassword']) == 0)
                    throw new Exception("Still... a password confirmation must be filled!");

                if ($user_info['cpassword'] != $user_info['password'])
                    throw new Exception("Hmz... Which password is correct? The first one or the confirmation one?");

                $user->password = $user_info['password'];
            }

            $user->save();

            return Response::json(
                ["status" => "ok"],
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Something went wrong. " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function authenticate()
    {
        $uinfo = array(
            'username' => Input::get('username'),
            'randomstring' => Input::get('randomstring')
        );
        try {
            $user = User::where('username', '=', $uinfo["username"])->firstOrFail();
            $file_cts = file_get_contents("/tmp/" . $user->username . ".authkey");

//            \Spark\FileSystem\FileOperation::WriteFile("",".authkey", $user->username);
//            \Spark\FileSystem\FileOperation::WriteFile($user->apikey,".apikey", $user->username);

            if (trim($file_cts) != $uinfo['randomstring'])
                throw new \Exception("Invalid authentication key!");

            return Response::json(
                ["status" => "ok", "api" => $user->apikey],
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Something went wrong. " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

    public function resetAPI()
    {
        try {
            $user = User::where('id', '=', Auth::user()->id)->firstOrFail();
            $user->updateApi();
            $user->save();

            return Response::json(
                ["status" => "ok", "apikey" => $user->apikey],
                200
            );
        } catch (\Exception $ex) {
            $exception = new STDClass();
            $exception->status = "error";
            $exception->message = "Something went wrong. " . $ex->getMessage();
            return Response::json(
                $exception,
                500
            );
        }
    }

//    public function generateKey(){
//        try {
//            $user = User::where('username', '=', Auth::user()->username)->firstOrFail();
//            ignore_user_abort (true);
//            $file = "/tmp/" . $user->username . ".private";
//
//            header('Content-Description: File Transfer');
//            header('Content-Type: application/octet-stream');
//            header('Content-Disposition: attachment; filename='.basename($file));
//            header('Expires: 0');
//            header('Cache-Control: must-revalidate');
//            header('Pragma: public');
//            header('Content-Length: ' . filesize($file));
//            readfile($file);
//
//            unlink($file);
//        } catch (\Exception $ex) {
//            $exception = new STDClass();
//            $exception->status = "error";
//            $exception->message = "Something went wrong. " . $ex->getMessage();
//            return Response::json(
//                $exception,
//                500
//            );
//        }
//    }

}