(function(){
    
    function category_url(id){
        if(id==undefined)
            return CDS.path + "api/" + CDS.api + "/help/category";
        return CDS.path + "api/" + CDS.api + "/help/category/" + id;
    }
    
    function issue_url(id){
        if(id!=undefined)
            return CDS.path + "api/" + CDS.api + "/help/issue/" + id;
        alert("Select a question.");
    }
    
    function loadHelpC(){
        var tpl = $("#help-category-element").text();
		$("#help-category ul").text("");
        
        $.get(category_url(),{},function(data){
            for(var i in data){				
                $("#help-category ul").append(Mustache.render(tpl,data[i]));   
            }
            
            $(".loader").fadeOut(); 
        },'json');
    }
    
    $(document).on("click","#help-category li",function(e){
        e.preventDefault();
        
        if($(this).hasClass("active"))
            return false;
		
		$(".loader").fadeIn();
        var tpl = $("#help-issues-element").text();
        
        $("#help-category li.active").removeClass("active");
        $(this).addClass("active");
        if($(".help-info").css("display")!="none")
            $(".help-info").hide();
        
        $.get(category_url($(this).data('id')), {}, function(data){
            $("#help-issues ul").text("");
            for(var i in data){				
                $("#help-issues ul").append(Mustache.render(tpl,data[i]));   
            }
            
            if($("#help-issues").css("display")=="none")
                $("#help-issues").show();
            $(".loader").fadeOut();
        },'json');
    });
    
    $(document).on("click","#help-issues li",function(e){
        e.preventDefault();
        
        if($(this).hasClass("active"))
            return false;
		
		$(".loader").fadeIn();
        
        $("#help-issues li.active").removeClass("active");
        $(this).addClass("active");
        
        $.get(issue_url($(this).data('id')), {}, function(data){
            $("#help-title").text("");
            $("#help-title").text(data.title);
            
            $("#help-description").text("");
            $("#help-description").html(data.description);

            if($(".help-info").css("display")=="none")
                $(".help-info").show();
            
            $(".loader").fadeOut();
        },'json');
        
    });
    
    function resizeWin(){
        $(".help-info").css({width: window.innerWidth - 2 - $("#help-category").width() - $("#help-issues").width() });   
    }
    
    resizeWin();
    $(window).resize(resizeWin);
    loadHelpC();
}(window));


