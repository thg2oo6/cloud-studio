(function(){
    
    function project_url(id){
        if(id==undefined)
            return CDS.path + "api/" + CDS.api + "/project";
        return CDS.path + "api/" + CDS.api + "/project/" + id;
    }
    
    function loadProjects(){
        var tpl = $("#project-list-element").text();
		$(".project-list ul").text("");
        
        $.get(project_url(),{},function(data){
            for(var i in data){
				var date = new Date(data[i].updated_at);
				data[i].updated_at = date.format("HH:MM dd.mm.yyyy");
				
                $(".project-list ul").append(Mustache.render(tpl,data[i]));   
            }
            
            $(".loader").fadeOut(); 
        },'json');
    }
    
    function modal_message(){
        var self = this,
            data = {},
            title = "",
            text = "",
            action = "";
        
        self.YES_NO = 1;
        self.OK_CANCEL = 2;
        self.SAVE_CANCEL = 3;
        self.CREATE_CANCEL = 3;
        
        
        self.clear = function(){
            data = {};
            title = "";
            text = "";
            action = "";
        }
        
        self.push_data = function(element, value){
            data[element] = value;   
        }
        
        self.title = function(txt){
            if(txt==undefined)
                return title;
            title = txt;
        }
        
        self.text = function(txt){
            if(txt==undefined)
                return text;
            text = txt;
        }
        
        self.action = function(txt){
            if(txt==undefined)
                return action;
            action = txt;
        }
        
        self.setButtons = function(type){
            switch(type){
                default:
                case self.YES_NO:
                    $(".modal #modal-no").text("No");
                    $(".modal #modal-yes").text("Yes");
                    break;
                    
                case self.OK_CANCEL:
                    $(".modal #modal-no").text("Cancel");
                    $(".modal #modal-yes").text("Okay");
                    break;
                    
                case self.SAVE_CANCEL:
                    $(".modal #modal-no").text("Cancel");
                    $(".modal #modal-yes").text("Save");
                    break;
                case self.CREATE_CANCEL:
                    $(".modal #modal-no").text("Cancel");
                    $(".modal #modal-yes").text("Create");
                    break;
            }
        }
        
        self.make = function(){
            $(".modal .modal-title").html(title);
            $(".modal .modal-body").html(text);
            $(".modal #modal-yes").data("action",action);
            
            for(var i in data){
                $(".modal #modal-yes").data(i,data[i]);
            }
            
            return self;
        }
        
        self.show = function(){
            $(".modal").modal('show');   
        }
        
        return this;
    }
    var modal = modal_message();
    
    $(document).on("click",".project-list li",function(e){
        e.preventDefault();
        
        if($(this).hasClass("active"))
            return false;
		
		$(".loader").fadeIn();
        
        $(".project-list li.active").removeClass("active");
        $(this).addClass("active");
        
        $.get(project_url($(this).data('id')), {}, function(data){
            $("#project-icon").removeAttr('class');
            $("#project-icon").addClass('icon ' + data.logo);
            
            $("#project-title").text("");
            $("#project-title").text(data.name);
            
			var date = new Date(data.created_at);
            $("#project-date").text("");
            $("#project-date").text(date.format("HH:MM dd.mm.yyyy"));
            
            $("#project-description").text("");
            $("#project-description").html(data.description?data.description:'Enter a description here...');
			$("#project-description").data('id',data.id);
            
            $("#launcher").data('slug', data.slug);
            $("#delete").data('id', data.id);
            
            $(".project-info").show();
            $(".loader").fadeOut();
        },'json');
    });
    
    $("#create_project").hover(function(){
        $(this).animate({color: "#0076a3"});
    },function(){
        $(this).animate({color: "#ababab"});
    });
    
//    $("#create_project").tooltip({
//        placement: 'right'
//    });
    
    $(document).on("click","#launcher",function(e){
        e.preventDefault();
        
        var win=window.open(CDS.path + "app/" + $(this).data('slug') , '_blank');
        win.focus();
    });
    
    $(document).on("click","#delete",function(e){
        e.preventDefault();
        
        modal.clear();
        modal.title("Delete project: " + $("#project-title").text());
        modal.text("Are you sure that you want to delete the project \"" + $("#project-title").text() + "\"?");
        modal.action("delete");
        modal.push_data("id",$(this).data('id'));
        modal.setButtons(modal.YES_NO);
        modal.make().show();
        
    });
    
    function deleteProject(id){
        $(".modal").modal('hide');
        $(".loader").fadeIn();
        $.ajax({
            url: project_url(id),
            type: "DELETE",
            dataType: "json",
            success: function(data){
                modal.clear();
                modal.title("Success");
                modal.text(data.message);
                modal.setButtons(modal.OK_CANCEL);
                modal.make().show();
                
                $(".project-list li.active").remove();
                $(".project-info").hide();
                
                $(".loader").fadeOut();
            },
            error: function(e){
                modal.clear();
                modal.title("Error deleting");
                modal.text(e.responseJSON.message);
                modal.setButtons(modal.OK_CANCEL);
                modal.make().show();
                
                $(".loader").fadeOut();
            }
        });   
    }
    
    $(document).on("click","#create_project",function(e){
        e.preventDefault();
        
        modal.clear();
        modal.title("Create new project");
        modal.text($("#project-new").html());
        modal.action("create");
        modal.setButtons(modal.CREATE_CANCEL);
        modal.make().show();
        
    });
    
    $(document).on("click",".icons li, .project-types li",function(e){
        var $this = $(this).parent().parent();
        
        $("." + $this.attr('class') + " .active").removeClass('active');
        $(this).addClass('active');
    });
    
    
    function createProject(){
        var project_icon = $(".modal .icons li.active");
        var project_type = $(".modal .project-types li.active");
        var project_title = $(".modal #projectname");
        var alerts = "";
		
		$(".modal .messages").hide().delay(150);
		$(".modal .messages").text("");
		
        if(project_title.val().length == 0)
			alerts += "Please insert a title for the project!<br/>";
        
        if(project_icon.length == 0)
            alerts += "Please select an icon!<br/>";
        
		if(project_type.length == 0)
            alerts += "Please select a type!";
        
        if(alerts != ""){
            $(".modal .messages").html(alerts);
			$(".modal .messages").show();
			return;
        }
		
		var my_data = {
			title: project_title.val(),
			icon: project_icon.data('icon'),
			type: project_type.data('id')
		};
		$(".modal").modal('hide');
		$(".loader").fadeIn();
		
		$.ajax({
            url: project_url(),
            type: "POST",
			data: my_data,
            dataType: "json",
            success: function(data){
				data = JSON.parse(data);
                modal.clear();
                modal.title("Success");
                modal.text(data.message);
                modal.setButtons(modal.OK_CANCEL);
                modal.make().show();
                
				loadProjects();
                
                $(".loader").fadeOut();
            },
            error: function(e){
                modal.clear();
                modal.title("Error creating");
                modal.text(e.responseJSON.message);
                modal.setButtons(modal.OK_CANCEL);
                modal.make().show();
                
                $(".loader").fadeOut();
            }
        }); 
    }
    
    $(document).on("click","#modal-yes",function(e){
        switch($(this).data('action')){
            case 'delete':
//                $(".modal").modal('hide');
                
                deleteProject($(this).data('id'));
                break;
            case 'create':
//                $(".modal").modal('hide');
                
                createProject();
                break;
			case 'bug-report':
				$(location).attr('href',$(this).data('link'));
				$(".modal").modal('hide');
				break;
            default:
                $(".modal").modal('hide');
                $(".loader").fadeOut();
                
        }
//        $('body').removeClass('modal-open');
//        $('.modal-backdrop').hide();
    });
	
	$("#project-description").editable({
		callback : function( data ) {
			// Callback that will be called once the editor looses focus
			var project_id = $("#project-description").data('id');
			
			$(".loader").fadeIn();
			if( data.content ) {
				$.ajax({
					url: project_url(project_id),
					type: "PUT",
					data: {
						'update_type': 'description',
						'description': data.content
					},
					dataType: "json",
					success: function(data){
						loadProjects();
						
						data = JSON.parse(data);
						modal.clear();
						modal.title("Success");
						modal.text(data.message);
						modal.setButtons(modal.OK_CANCEL);
						modal.make().show();
						
						
						$(".loader").fadeOut();
					},
					error: function(e){
						modal.clear();
						modal.title("Error deleting");
						modal.text(e.responseJSON.message);
						modal.setButtons(modal.OK_CANCEL);
						modal.make().show();
						
						$(".loader").fadeOut();
					}
				});
			}else{
				if(data.content.length==0)
					$("#project-description").text('Enter a description here...');	
			}
		}
	});
	
	$(document).on("click","#bug-report",function(e){
		e.preventDefault();
		
		modal.clear();
		modal.title("Bug Reporting");
		modal.text("You will be redirected to the BitBucket issue tracker. If you want to stay on this page, please press Cancel!");
		modal.action("bug-report");
		modal.push_data('link',$(this).attr('href'));
		modal.setButtons(modal.OK_CANCEL);
		modal.make().show();
	});
	
    loadProjects();
}(window));


