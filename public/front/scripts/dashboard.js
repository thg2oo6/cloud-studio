(function () {

    function cds_modal() {
        var id = "#cds_modal",
            self = {},
            callback = null;

        self.button = {
            OK_CANCEL: 1,
            OKAY_CANCEL: 2,
            SAVE_CANCEL: 3,
            YES_NO: 4,
            CLOSE: 5
        };

        self.title = function (title) {
            $(id + " .modal-title").text(title);

            return self;
        }

        self.text = function (text) {
            $(id + " .modal-body").html(text);

            return self;
        };

        self.buttons = function (buttons) {
            if (buttons == self.button.CLOSE)
                $(id + " #btnCancel").css({display: 'none'});
            else
                $(id + " #btnCancel").css({display: 'inline-block'});

            switch (buttons) {
                case 1:
                    $(id + " #btnOkay").text("Ok");
                    $(id + " #btnCancel").text("Cancel");
                    break;
                case 2:
                    $(id + " #btnOkay").text("Okay");
                    $(id + " #btnCancel").text("Cancel");
                    break;
                case 3:
                    $(id + " #btnOkay").text("Save");
                    $(id + " #btnCancel").text("Cancel");
                    break;
                case 4:
                    $(id + " #btnOkay").text("Yes");
                    $(id + " #btnCancel").text("No");
                    break;
                case 5:
                    $(id + " #btnOkay").text("Close");
                    break;
            }

            return self;
        }

        self.callback = function (the_callback) {
            callback = the_callback;

            return self;
        }

        self.clearCallback = function () {
            callback = null;

            return self;
        }

        self.clear = function () {
            self.title("").text("");

            return self;
        }

        $(document).on("click", id + " #btnOkay", function (e) {
            e.preventDefault();

            var returnValue = true;
            if (callback !== null && callback !== undefined)
                returnValue = callback();

            if (returnValue)
                modal.hide();
        });

        self.show = function () {
            $(id).modal("show");

            return self;
        }

        self.hide = function () {
            $(id).modal("hide");

            return self;
        }

        return self;
    }

    var modal = CloudStudio.modal = cds_modal();

    function projectUrl(id) {
        if (id == undefined)
            return CloudStudio.path + "api/" + CloudStudio.api + "/project";
        return CloudStudio.path + "api/" + CloudStudio.api + "/project/" + id;
    }

    function projectClone(id) {
        if (id == undefined)
            throw new SyntaxError("Please give an ID");
        return CloudStudio.path + "api/" + CloudStudio.api + "/clone/" + id;
    }

    function publicProjectUrl(id) {
        if (id == undefined)
            return CloudStudio.path + "api/" + CloudStudio.api + "/publicProject";
        return CloudStudio.path + "api/" + CloudStudio.api + "/publicProject/" + id;
    }

    function loadProjects() {
        var text = "";
        var tpl = $("#project_listing").text();

//		$(".projects").text("");
        $.get(projectUrl(), {}, function (data) {
            console.log(data);
            for (var i in data) {
                if (i % 2 == 0)
                    text += '<section class="row project-row">';

                text += Mustache.render(tpl, data[i]);

                if (i % 2 == 1 || i == data.length - 1)
                    text += '</section>';
            }

            if (text.length === 0)
                $("#myprojectsTab .projects").html("<h3>No projects yet :(</h3>");
            else
                $("#myprojectsTab .projects").html(text);

            //$(".loader").fadeOut();
        }, 'json');
    }

    function loadPublicProjects() {
        var text = "";
        var tpl = $("#public_listing").text();

//		$(".projects").text("");
        $.get(publicProjectUrl(), {}, function (data) {
            console.log(data);
            for (var i in data) {
                if (i % 2 == 0)
                    text += '<section class="row project-row">';

                text += Mustache.render(tpl, data[i]);

                if (i % 2 == 1 || i == data.length - 1)
                    text += '</section>';
            }

            if (text.length === 0)
                $("#projectsTab .projects").html("<h3>No projects yet :(</h3>");
            else
                $("#projectsTab .projects").html(text);

            //$(".loader").fadeOut();
        }, 'json');
    }

    function resetForm() {
        $("#ProjectNameTxt").val("");
        $("#ProjectTypeTxt").val("");
        $("#ProjectLogoTxt").val("");
    }

    $(document).on("click", ".open-project", function (e) {
        e.preventDefault();

        var win = window.open(CloudStudio.path + "app/" + $(this).data('slug'), '_blank');
        win.focus();
    });

    $(document).on("click", "#create", function (e) {
        resetForm();

        $(".leftmenu .active").removeClass("active");
        $(".leftmenu #myprojects").parent().addClass("active");
        $(".content.container .active").removeClass("active");
        $("#creationTab").addClass("active");

        $("#creationTab .users").click();
    });

    $(document).on("click", "#creationTab .logos li", function (e) {
        $("#creationTab .logos .active").removeClass("active");
        $(this).addClass("active");

        $("#ProjectLogoTxt").val($(this).data("logo"));
    });

    $(document).on("click", "#updateTab .logos li", function (e) {
        $("#updateTab .logos .active").removeClass("active");
        $(this).addClass("active");

        $("#ProjectLogoUpd").val($(this).data("logo"));
    });

    $(document).on("submit", "#CreationForm", function (e) {
        e.preventDefault();

        var form = $(this).serializeObject();

        if (form.name === null || form.name === undefined || form.name.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please fill the name of the project!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }
        if (form.type === null || form.type === undefined || form.type.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please select the type of the project!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }
        if (form.logo === null || form.logo === undefined || form.logo.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please select the logo of the project!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }

        $.ajax({
            url: projectUrl(),
            dataType: 'json',
            type: 'POST',
            data: form,
            success: function (data) {
                $(".back-btn").click();

                loadProjects();
            },
            error: CloudStudio.error
        });
    });

    $(document).on("click", ".clone-project", function (e) {
        e.preventDefault();

        var slug = $(this).data('slug'),
            request = {
                url: projectClone(slug),
                dataType: 'json',
                type: 'POST',
                success: function (data) {
                    $(".back-btn").click();
                    modal.clear()
                        .title("Project cloned")
                        .text("The project has been cloned!")
                        .buttons(modal.button.CLOSE)
                        .callback(function () {
                            return true;
                        })
                        .show();
                    loadProjects();
                },
                error: CloudStudio.error
            };
        $.ajax(request);
    })

    $(document).on("submit", "#SettingsForm", function (e) {
        e.preventDefault();

        var form = $(this).serializeObject();

        if (form.firstname === null || form.firstname === undefined || form.firstname.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please fill your first name!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }
        if (form.lastname === null || form.lastname === undefined || form.lastname.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please fill your last name!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }

        if (form.password !== null && form.password !== undefined && form.password.trim().length !== 0) {

            if (form.oldpassword === null || form.oldpassword === undefined || form.oldpassword.trim().length === 0) {
                modal.clear()
                    .title("Form fields aren't filled")
                    .text("Please fill your old password!")
                    .buttons(modal.button.CLOSE)
                    .callback(function () {
                        return true;
                    })
                    .show();
                return;
            }

            if (form.password.trim().length < 6) {
                modal.clear()
                    .title("Form fields aren't filled")
                    .text("Please enter a password greater than 6 chars!")
                    .buttons(modal.button.CLOSE)
                    .callback(function () {
                        return true;
                    })
                    .show();
                return;
            }

            if (form.cpassword === null || form.cpassword === undefined || form.cpassword.trim().length === 0) {
                modal.clear()
                    .title("Form fields aren't filled")
                    .text("Please fill your password confirmation!")
                    .buttons(modal.button.CLOSE)
                    .callback(function () {
                        return true;
                    })
                    .show();
                return;
            }

            if (form.cpassword != form.password) {
                modal.clear()
                    .title("Form fields aren't filled")
                    .text("The passwords should be exactly the same!")
                    .buttons(modal.button.CLOSE)
                    .callback(function () {
                        return true;
                    })
                    .show();
                return;
            }

        } else {
            delete form.password;
            delete form.cpassword;
            delete form.oldpassword;
        }

//        console.log(form);
        $.ajax({
            url: CloudStudio.path + "settings",
            dataType: 'json',
            type: 'POST',
            data: form,
            success: function (data) {
                modal.clear()
                    .title("Settings saved")
                    .text("The modification have been saved!")
                    .buttons(modal.button.CLOSE)
                    .callback(function () {
                        $(location).attr('href', $(location).attr('href'));
                    })
                    .show();
            },
            error: CloudStudio.error
        });
    });

    $(document).on("click", "#resetAPI", function (e) {
        e.preventDefault();

        var request = {
            url: CloudStudio.path + "resetAPI",
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                $("#APIKeyTxt").val(data.apikey);
            },
            error: CloudStudio.error
        };

        modal.clear()
            .title("Reset API Key?")
            .text("Are you sure you want to reset the API Key?")
            .buttons(modal.button.YES_NO)
            .callback(function () {
                $.ajax(request);
                return true;
            })
            .show();

    });

    $(document).on("click", ".back-btn", function (e) {
        e.preventDefault();

        resetForm();

        $(".leftmenu .active").removeClass("active");
        $(".leftmenu #myprojects").parent().addClass("active");
        $(".content.container .active").removeClass("active");
        $("#myprojectsTab").addClass("active");

    });

    $(document).on("click", ".project-details", function (e) {
        e.preventDefault();

        $.get(projectUrl($(this).data('slug')), {}, function (data) {
            console.log(data);

            $("#myprojectTab #ProjectLogo").addClass(data.logo);
            $("#myprojectTab #ProjectName").text(data.name);
            $("#myprojectTab #ProjectDelete").data("name", data.name).data("slug", data.slug);
            $("#myprojectTab #ProjectOwner").text(data.owner);
            $("#myprojectTab #ProjectType").text(data.type);
            $("#myprojectTab #ProjectCreation").text(data.created_at);
            $("#myprojectTab #ProjectUpdate").text(data.updated_at);
            $("#myprojectTab #ProjectVisibility").text(data.visibility);
            $("#myprojectTab #ProjectDescription").text(data.description);
            $("#myprojectTab .open-project").data("slug", data.slug);
            $("#myprojectTab .edit").data("slug", data.slug);

            $(".activityList").text("");

            var tpl = $("#activity_listing").text();
            for (var i in data.activities) {
                var activity = data.activities[i];
                $(".activityList").append(Mustache.render(tpl, activity));
            }

            $(".leftmenu .active").removeClass("active");
            $(".leftmenu #myprojects").parent().addClass("active");
            $(".content.container .active").removeClass("active");
            $("#myprojectTab").addClass("active");
            //$(".loader").fadeOut();

            $('.user_image').tooltip()
        }, 'json');
    });

    $(document).on("click", ".edit", function (e) {
        e.preventDefault();

        var slug = $(this).data('slug');
        $.get(projectUrl(slug), {}, function (data) {
            console.log(data);

            $(".leftmenu .active").removeClass("active");
            $(".leftmenu #myprojects").parent().addClass("active");
            $(".content.container .active").removeClass("active");
            $("#updateTab").addClass("active");
            $("#UpdateForm").data("slug", slug);

            $("#updateTab #ProjectLogoUpd").val(data.logo);
            $("#updateTab .cds-logo." + data.logo).addClass("active");
            $("#updateTab #ProjectNameUpd").val(data.name);
            $("#updateTab #ProjectTypeUpd").val(data.type_slug);
            $("#updateTab input[name='visibility'][checked]").attr("checked", false);
            $("#updateTab input[name='visibility'][value='" + (data.visibility == "Public" ? 1 : 0) + "']").attr("checked", true);
            $("#updateTab #ProjectDescriptionUpd").val(data.description);

            //$(".loader").fadeOut();

        }, 'json');
    });

    $(document).on("submit", "#UpdateForm", function (e) {
        e.preventDefault();

        var form = $(this).serializeObject();

        if (form.name === null || form.name === undefined || form.name.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please fill the name of the project!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }
        if (form.type === null || form.type === undefined || form.type.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please select the type of the project!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }
        if (form.logo === null || form.logo === undefined || form.logo.trim().length === 0) {
            modal.clear()
                .title("Form fields aren't filled")
                .text("Please select the logo of the project!")
                .buttons(modal.button.CLOSE)
                .callback(function () {
                    return true;
                })
                .show();
            return;
        }

        var slug = $(this).data("slug");
        $.ajax({
            url: projectUrl(slug),
            dataType: 'json',
            type: 'PUT',
            data: form,
            success: function (data) {
                $(".back-btn").click();

                loadProjects();
            },
            error: CloudStudio.error
        });
    });

    $(document).on("click", ".delete", function (e) {
        e.preventDefault();

        var slug = $(this).data('slug'),
            request = {
                url: projectUrl(slug),
                dataType: 'json',
                type: 'DELETE',
                success: function (data) {
                    $(".back-btn").click();
                    loadProjects();
                },
                error: CloudStudio.error
            };

        modal.clear()
            .title("Delete project '" + $(this).data("name") + "'?")
            .text("Are you sure you want to delete this project?")
            .buttons(modal.button.YES_NO)
            .callback(function () {
                $.ajax(request);
                return true;
            })
            .show();

    });

    $(document).on("click", ".leftmenu a", function () {
        var id = $(this).attr("id");

        $(".leftmenu .active").removeClass("active");
        $(this).parent().addClass("active");

        $(".content.container .active").removeClass("active");
        $(".content.container #" + id + "Tab").addClass("active");
    });

    $(document).on("click", "#toolsTab a", function () {
        var id = $(this).attr("id");

        if (id == "dump")
            $(location).attr("href", CloudStudio.path + "tools/" + id);
    });

    $(document).on("click", ".leftmenu #projects", function () {
        loadPublicProjects();
    });

    loadProjects();
}(window));