var header_height = $(".top-header").height() + 35;

$(document).on("click",".menu a", function(e){    
    var block = $(this).attr('href');
    if(block.substr(0,1)=="#"){
        e.preventDefault();
        
        $("body").animate({scrollTop: $(block).position().top - header_height });
        
        $(".menu .active").removeClass('active');
        $(this).parent().addClass('active');
    }
});

$(document).scroll(function() {
    var cutoff = $(window).scrollTop();
    $('.menu a').each(function() {
         var block = $(this).attr('href');
        if(block.substr(0,1)=="#"){
            if ($(block).offset().top > cutoff) {
                $(".menu .active").removeClass('active');
                $(this).parent().addClass('active');
                return false; // stops the iteration after the first one on screen
            }
        }
    });
});

$("#screen a").fancybox({
    padding: 2
});