$(document).on("submit","#RegisterAction",function(e){
    e.preventDefault();
    
    var form = $(this).serializeObject(),
        action = $(this).attr('action'),
        button = $("#RegisterAction button[type=submit]");
    button.attr('disabled',true);

    $(".msg").removeClass("error");
    $(".msg").addClass("warning");
    $("#errorMsg").text("Please wait...");
    $(".msg").show();

    $.post(
        action,
        form,
        function(data){
            if(data.status=="ok")
                $(location).attr('href', $("#login").attr('href'));
            else{
                $(".msg").removeClass("warning");
                $(".msg").addClass("error");
                $("#errorMsg").text(data.message);
                $(".msg").show();
                button.attr('disabled', false);
            }
        },
        'json'
    );
    
});