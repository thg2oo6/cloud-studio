define(function (require, exports, module) {
    'use strict';

    exports.Sidebar = new function () {
        var Templates = {
            view: require("text!views/sidebar/sidebar.html"),
            containerView: require("text!views/sidebar/box.html")
        };

        /*--- MAIN BLOCK ---*/
        var Strings = require("nls/strings");
        var self = {},
            elements = {},
            privates = {};

        elements.projectFiles = ClassLoader(require("ext/sidebar/ProjectFile"));
        elements.toolbox = ClassLoader(require("ext/sidebar/Toolbox"));
        elements.properties = ClassLoader(require("ext/sidebar/Properties"));

        HookupEvent.define("close_sidebar");
        HookupEvent.define("sidebar_before_loadfinish");

        privates.close_sidebar = function(){
            $("#sidebar-ext").toggle('slide','left');
            $("#sidebar-ext").removeClass('online');
            $("#sidebar-ext .active").removeClass('active');
            $("#sidebar li a").removeClass("active");

            HookupEvent.execute("close_sidebar");
        }

        privates.attachEvents = function(){
            privates.detachEvents();

            DOMEvent.addReady(function(){
                $("#mainbar a:not(.disabled,.active)").tooltip({placement: 'right'});
                $("#mainbar a.disabled").attr('title','');
            });

            DOMEvent.addResize(function(){
                $("#sidebar").css({height: window.innerHeight - $(".dashboard #topbar").height() - 1});
                $("#sidebar-ext").css({height: window.innerHeight - $(".dashboard #topbar").height() - 1});

                $(".widget .content").css({height: $(".widget").height() - $(".widget .header").outerHeight() - 1});
            });

            $(document).on("click", "#sidebar li a",function(e){
                if($(this).hasClass("disabled"))
                    return;

                var is_visible = $("#sidebar-ext").css('display') !== "none";
                var active_tab = $("#sidebar-ext .active").attr('id');

//                close_sidebar();
                if(active_tab !== undefined)
                    if(active_tab == $(this).data('widget'))
                        return;

                $(".contentLoader").show();
                if (is_visible){
                    $("#sidebar-ext").removeClass('online');
                    $("#sidebar-ext .active").removeClass('active');
                    $("#sidebar li a").removeClass("active");
                }

                $("#sidebar-ext #" + $(this).data('widget')).addClass('active');
                $("#sidebar-ext").addClass('online');
                if (!is_visible)
                    $("#sidebar-ext").toggle('slide','left');
                $(this).addClass("active");

                $(".widget .content").css({height: $(".widget").height() - $(".widget .header").outerHeight() - 1});

                HookupEvent.execute("sidebar_before_loadfinish");

                $(".contentLoader").hide();
            });

            $(document).on("click", ".close-widget",function(){
                privates.close_sidebar();
            });
        }

        privates.detachEvents = function(){
            $(document).off("click", "#sidebar li a");
            $(document).off("click", ".close-widget");
        }


        self.render = function () {
            $(".dashboard").append(Mustache.render(Templates.view, Strings));

            for (var element in elements) {

                var _options = elements[element].forge(Strings[elements[element].id]);
                $("#sidebar-ext").append(Mustache.render(Templates.containerView, _options));
                if (elements[element].refill !== undefined)
                    elements[element].refill();

            }

            privates.attachEvents();
        }

        return self;
    };
});