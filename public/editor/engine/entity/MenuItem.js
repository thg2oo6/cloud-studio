define(function (require, exports, module) {
    'use strict';

    exports.MenuItem = function (settings) {

        var self = {};

        var properties = {
            name: '',
            icon: 'icon-none',
            shortcut: '',
            type: '',
            text: '',
            disable: false,
            submenu: []
        };


        $.extend(properties, settings);

        self.get = function (element) {
            return properties[element];
        }

        self.forge = function (text) {
            return $.extend(properties, {text: text});
        }

        return self;
    };

});