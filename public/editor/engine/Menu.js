define(function (require, exports, module) {
    'use strict';

    exports.Menu = new function () {
        /**
         * Because require.js doesn't allow us to load modules dynamically
         * we have to add them manually.
         */
        var menuDefinitions = [
            JSON.parse(require("text!ext/menu/file/definition.json")),
            JSON.parse(require("text!ext/menu/edit/definition.json")),
            JSON.parse(require("text!ext/menu/project/definition.json")),

            /* -- USER DEFINED MENUS -- */

            /* -- END USER DEFINED MENUS -- */

            // Usually HELP item is the last menu item
            JSON.parse(require("text!ext/menu/help/definition.json"))
        ];

        require("ext/menu/file/actions");
        require("ext/menu/edit/actions");
        require("ext/menu/project/actions");
        require("ext/menu/help/actions");

        /* -- USER MENU ACTIONS -- */

        /* -- END USER MENU ACTIONS -- */

        var Templates = {
            btn: require('text!views/header/button.html'),
            btn2: require('text!views/header/button2.html'),
            dropdown: require('text!views/header/dropdown.html'),
            divider: require('text!views/header/divider.html'),
            topbar: require("text!views/header/topbar.html")
        };

        /*--- MAIN BLOCK ---*/
        var MenuHelper = ClassLoader(require("engine/helper/MenuHelper"));
        var Strings = require("nls/strings");
        var self = {},
            privates = {};

        privates.elements = {};

        privates.Element = function (module) {
            var _definition = menuDefinitions[module];
            var _return = null;

            if (!isValid(_definition.submenu) || _definition.submenu.length == 0)
                _return = MenuHelper.MenuButton(_definition.description);
            else
                _return = MenuHelper.MenuDropdown(_definition.description, _definition.submenu);

            return _return;
        }

        privates.renderButton = function (element) {
            var _options = element.forge(Strings[element.get('name')]);
            return Mustache.render(Templates.btn, _options);
        }

        privates.renderDropDown = function (element) {
            var _options = element.forge(Strings[element.get('name')]);
            return Mustache.render(Templates.dropdown, _options);
        }

        privates.renderButtonDropDown = function (element) {
            var _options = element.forge(Strings[element.get('name')]);
            return Mustache.render(Templates.btn2, _options);
        }

        privates.renderDivider = function (element) {
            return Mustache.render(Templates.divider);
        }

        privates.leftBar = function () {
            var LeftBarTitle = "";

            $("#topbar .right a").hover(function () {
                LeftBarTitle = $(this).attr("title");
                $(this).attr("title", "");
                $("#topbar #title").text(LeftBarTitle);
            }, function () {
                $(this).attr("title", LeftBarTitle);
                $("#topbar #title").text("");
                LeftBarTitle = "";
            });
        }


        for (var element in menuDefinitions) {
            privates.elements[menuDefinitions[element].description.name] = new privates.Element(element);
        }

        self.get = function () {
            return privates.elements;
        }

        self.render = function () {
            $(".dashboard").append(Mustache.render(Templates.topbar, Strings));
            var Topbar = privates.elements;

            for (var element in Topbar) {
//                window.Shortcuts.add(Topbar[element].get('name'), Topbar[element].get('shortcut'));

                if (Topbar[element].get('type') == 'button')
                    $("#topbar #menubar").append(privates.renderButton(Topbar[element]));
                if (Topbar[element].get('type') == 'dropdown') {
                    $("#topbar #menubar").append(privates.renderDropDown(Topbar[element]));

                    var _submenu = Topbar[element].get('submenu');
                    var _id = Topbar[element].get('name');

                    for (var elementID = 0; elementID < _submenu.length; elementID++) {
//                        window.Shortcuts.add(_submenu[elementID].get('name'),_submenu[elementID].get('shortcut'));

                        if (_submenu[elementID].get('type') == 'button')
                            $("#" + _id + " ul").append(privates.renderButtonDropDown(_submenu[elementID]));

                        if (_submenu[elementID].get('type') == 'divider')
                            $("#" + _id + " ul").append(privates.renderDivider(_submenu[elementID]));
                    }
                }

            }

            privates.leftBar();

            require('ext/menu/global');
        }

        return self;
    };
});