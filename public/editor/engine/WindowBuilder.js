define(function (require, exports, module) {
    'use strict';

    exports.WindowBuilder = new function () {
        var Templates = {
            view: require("text!views/workspace/window.html")
//            codeView: require("text!views/workspace/code.html"),
//            windowView: require("text!views/workspace/window.html")
        };

        /*--- MAIN BLOCK ---*/
        var Strings = require("nls/strings");
        var self = {},
            privates = {},
            elements = {
                Container: ClassLoader(require("ext/toolbox/Container")),
                Control: ClassLoader(require("ext/toolbox/Control")),
                Data: ClassLoader(require("ext/toolbox/Data")),
                Form: ClassLoader(require("ext/toolbox/Form"))
            };

        privates.defaultElement = {
            greedy: true,
            tolerance: "pointer",
            hoverClass: "drop-hover",
            drop: function (ev, ui) {
                if (ui.helper.data('type') != null) {
                    ev.preventDefault();
                    privates.createElement(ui.helper.data('type'), this);
                }
            }
        };

        privates.resize = function () {
            var sidebarExt = 0;
            if ($("#sidebar-ext").hasClass("online"))
                sidebarExt = $("#sidebar-ext").width() + 1;

            $(".workspace").css({
                height: window.innerHeight - $(".dashboard #topbar").height() - 1
            });

            $(".workspace .xcontainer").css({
                height: $(".workspace").height() - $(".workspace .tabs").height()
            });

            $(".workspace").animate({
                width: window.innerWidth - $("#sidebar").width() - 1 - sidebarExt,
                left: $("#sidebar").width() + 1 + sidebarExt
            });
        }

        privates.createElement = function (element, target) {
            var e = $(".workspace .tabs li.active");

            if (elements.Container[element] != null) {
                if (element == "header")
                    if ($(target).data('boxtype') == "header")
                        elements.Container.header(target, privates.defaultElement);
                    else
                        e = null;
                else if (element == "footer")
                    if ($(target).data('boxtype') == "footer")
                        elements.Container.footer(target, privates.defaultElement);
                    else
                        e = null;
                else
                    elements.Container[element](target, privates.defaultElement);
            } else if (elements.Control[element] != null) {
                elements.Control[element](target, privates.defaultElement);
            } else if (elements.Data[element] != null) {
                elements.Data[element](target, privates.defaultElement);
            } else if (elements.Form[element] != null) {
                elements.Form[element](target, privates.defaultElement);
            } else {
                console.log("Feature not implemented");
                e = null;
            }
            if (e) {
                if (!e.hasClass('changed')) {
                    e.data('changed', true);
                    e.addClass("changed");
                }
            }
        }

        self.init = function () {
            $("#TOOLBOX").disableSelection()
            $("#TOOLBOX li").not(".disabled").draggable({
                helper: 'clone',
                containment: 'frame'
            });
            var ctxMenu = '<div id="dropbox-ctx">\n';
            ctxMenu += '<ul class="dropdown-menu" role="menu">\n';
            ctxMenu += '<li><a id="delete" tabindex="-1">' + Strings.CTX2_DELETEOBJ + '</a></li>\n';
            ctxMenu += '<li class="divider"></li>\n';
            ctxMenu += '<li><a id="properties" tabindex="-1">' + Strings.CTX2_PROPERTIES + '</a></li>\n';
            ctxMenu += '</ul>\n';
            ctxMenu += '</div>\n';

            $(".workspace .xcontainer").append(ctxMenu);
        }


        privates.rebuild = function (tab_name, contentJSON) {
            try {
                //var contentJSON = JSON.parse(content);
                var style = contentJSON.style.content,
                    layout = contentJSON.window.content,
                    actions = contentJSON.script.content

                if (layout.length > 0)
                    $("#" + tab_name + " .layout").html(layout);

                $("#" + tab_name + " .designer").text(style);
                $("#" + tab_name + " .actions").text(actions);
            } catch (e) {

            }
        }

        privates.deleteElement = function (element) {
            var e = $(element);
            var e2 = $(".workspace .tabs li.active");
            if (!e2.hasClass('changed')) {
                e2.data('changed', true);
                e2.addClass("changed");
            }

            if (e.data('boxtype') != "header" && e.data('boxtype') != "footer") {
                e.remove();

                return;
            }

            e.html("");
            e.removeClass("navbar navbar-default");
            e.attr("role", "");
        }

        privates.restoreIdx = -150;
        self.openTab = function (name, path, lang, content, noAdd) {
            $(".workspaceLoader").show();
            $(".workspace .tabs li.active").removeClass('active');
            $(".workspace .xcontainer .active").removeClass('active');

            var tab = {
                unique_tab: "tab-" + (window.timestamp() - privates.restoreIdx),
                name: name,
                file: path + name,
                path: path
            };

            if (!noAdd)
                privates.parent.add(tab.file, "app");
            else
                privates.restoreIdx = privates.restoreIdx - 1;

            $(".workspace .tabs ul").append(Mustache.render($("#tab-tpl").html(), tab));
            $(".workspace .xcontainer").append(Mustache.render($("#tab-wbapp").html(), tab));

            //Make element droppable
            var tabid = "#" + tab.unique_tab;
            $("<style/>", {
                class: "designer",
                id: tab.unique_tab + "-designer"
            }).appendTo(tabid);
            $("<section/>", {
                class: "actions",
                id: tab.unique_tab + "-action"
            }).appendTo(tabid);

            $(document).on("click", tabid + " a", function (e) {
                e.preventDefault();
            });
            $(document).on("submit", tabid + " form", function (e) {
                e.preventDefault();
            });

            privates.rebuild(tab.unique_tab, content);
            $(tabid).addClass(name.split(".")[0]);

            var $element = null;
            var wb = ".workspace .xcontainer .wbuilder.active";
            $(wb + " .layout").contextmenu({
                target: '#dropbox-ctx',
                before: function (e, element) {
                    e.preventDefault();
                    try {
                        $element.removeClass("drop-hover");
                    } catch (e) {

                    }

                    $(e.target).addClass("drop-hover");
                    if ($(e.target).data('boxtype') != null) {
                        if ($(e.target).data('boxtype') == 'header' && $(e.target).hasClass('navbar')) {
                            $("#dropbox-ctx #rename").removeClass('disabled');
                            $("#dropbox-ctx #delete").removeClass('disabled');

                            $element = $(e.target);
                            return true;
                        }
                        if ($(e.target).data('boxtype') == 'footer' && $(e.target).hasClass('navbar')) {
                            $("#dropbox-ctx #rename").removeClass('disabled');
                            $("#dropbox-ctx #delete").removeClass('disabled');

                            $element = $(e.target);
                            return true;
                        }

                        $("#dropbox-ctx #rename").addClass('disabled');
                        $("#dropbox-ctx #delete").addClass('disabled');
                        $(e.target).removeClass("drop-hover");

                        $element = null;
                        return false;
                    } else {
                        $("#dropbox-ctx #rename").removeClass('disabled');
                        $("#dropbox-ctx #delete").removeClass('disabled');

                        $element = $(e.target);
                    }

                    return true;
                },
                onItem: function (e, element) {
                    e.preventDefault();

                    if ($(element).hasClass('disabled'))
                        return false;

                    var id = $(element).attr('id');
                    switch (id) {
                        case 'properties':
                            window.PropertyLoader($element);
                            break;
                        case 'delete':
                            privates.deleteElement($element);
                            break;
                    }

                },
                onClose: function () {
                    try {
                        $element.removeClass("drop-hover");
                    } catch (e) {

                    }
                }
            })
            $(tabid + " .header, " + tabid + " .ctnt, " + tabid + " .footer").droppable(privates.defaultElement);
            $(tabid + " .layout section").not("not-droppable").droppable(privates.defaultElement);
            $(tabid + " ul").not("not-droppable").droppable(privates.defaultElement);

            $(".workspaceLoader").hide();
        }

        self.empty = function () {
            return $(".workspace .tabs li").length == 0;
        };

        self.render = function (parent) {
            $(".dashboard").append(Templates.view);

            DOMEvent.addReady(privates.resize);
            DOMEvent.addResize(privates.resize);
            HookupEvent.add('sidebar_before_loadfinish', privates.resize);
            HookupEvent.add('close_sidebar', privates.resize);

            privates.parent = parent;
        }

        return self;
    };
});