define(function (require, exports, module) {
    'use strict';

    window.ClassLoader = function (module) {
        for (var i in module)
            return module[i];

        return null;
    }
})