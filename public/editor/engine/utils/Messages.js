define(function (require, exports, module) {
    'use strict';

    var Strings = require("nls/strings");

    var modalBox = new function(){
        var self = {},
            privates = {};

        privates.data = {};
        privates.title = "";
        privates.text = "";
        privates.action = null;

        self.YES_NO = 1;
        self.OK_CANCEL = 2;
        self.SAVE_CANCEL = 3;
        self.CREATE_CANCEL = 3;

        self.clear = function(){
            privates.data = {};
            privates.title = "";
            privates.text = "";
            privates.action = null;
        }

        self.push_data = function(element, value){
            privates.data[element] = value;
        }

        self.title = function(txt){
            if(txt==undefined)
                return privates.title;
            privates.title = txt;
        }

        self.text = function(txt){
            if(txt==undefined)
                return privates.text;
            privates.text = txt;
        }

        self.action = function(callback){
            if(callback == undefined || callback == null)
                return privates.action;
            privates.action = callback;
        }

        self.setButtons = function(type){
            switch(type){
                default:
                case self.YES_NO:
                    $(".modal_one #modal-no").text(Strings.NO);
                    $(".modal_one #modal-yes").text(Strings.YES);
                    break;

                case self.OK_CANCEL:
                    $(".modal_one #modal-no").text(Strings.CANCEL);
                    $(".modal_one #modal-yes").text(Strings.OK);
                    break;

                case self.SAVE_CANCEL:
                    $(".modal_one #modal-no").text(Strings.CANCEL);
                    $(".modal_one #modal-yes").text(Strings.SAVE);
                    break;
                case self.CREATE_CANCEL:
                    $(".modal_one #modal-no").text(Strings.CANCEL);
                    $(".modal_one #modal-yes").text(Strings.CREATE);
                    break;
            }
        }

        self.make = function(){
            $(".modal_one .modal-title").html(privates.title);
            $(".modal_one .modal-body").html(privates.text);
            $(".modal_one #modal-yes").data("action",privates.action);

            for(var i in privates.data){
                $(".modal_one #modal-yes").data(i,privates.data[i]);
            }

            return self;
        }

        self.show = function(){
            $(".modal_one").modal('show');
        }

        $(document).on("click",".modal_one #modal-yes",function(e){
            var action = $(this).data('action');

            if(action != undefined && action != null && action != "")
                action();

            $(".modal_one").modal('hide');
        });

        return self;
    };

    $(document).off("keydown", "#prompt-value");
    $(document).on("keydown", "#prompt-value", function(event) {
        if (event.keyCode == 13) {
            $(".modal_one #modal-yes").click();
        }
    });

    exports.Messages = new function () {

        /*--- MAIN BLOCK ---*/
        var self = {},
            privates = {};


        self.notification = function(message, title){
            self.confirm(message, null, title);
        }

        self.confirm = function(message, callback, title){
            if(message == undefined || message == null || message == "")
                return;

            modalBox.clear();
            if(title == undefined || title == null || title == "")
                title = Strings.NOTIFICATION_TITLE;

            modalBox.title(title);
            modalBox.text(message);
            modalBox.setButtons(modalBox.OK_CANCEL);
            modalBox.action(callback);
            modalBox.make().show();
        }

        self.form_call = function(message, title, action, callback, buttons){
            if(message == undefined || message == null || message == "")
                return;

            modalBox.clear();
            if(title == undefined || title == null || title == "")
                title = Strings.NOTIFICATION_TITLE;

            modalBox.title(title);
            modalBox.text(message);
            if(!isValid(buttons))
                buttons = modalBox.SAVE_CANCEL;
            else
                buttons = modalBox.OK_CANCEL;
            modalBox.setButtons(buttons);
            modalBox.action(callback);
            modalBox.make().show();

            $(".modal_one #modal-yes").attr("disabled",true);
            action($(".modal_one #modal-yes"), $(".modal_one .modal-body"));
        }

        self.wait = function(message, title, callback){
            if(message == undefined || message == null || message == "")
                return;

            modalBox.clear();
            if(title == undefined || title == null || title == "")
                title = Strings.NOTIFICATION_TITLE;

            modalBox.title(title);
            modalBox.text(message);
            modalBox.setButtons(modalBox.OK_CANCEL);
            modalBox.action();
            modalBox.make().show();
            callback($(".modal_one #modal-yes"), $(".modal_one .modal-body"));
//            $(".modal_one #modal-yes").attr("disabled",true);
        }

        self.prompt = function(message, value, callback, title){
            if(message == undefined || message == null || message == "")
                return;

            modalBox.clear();
            if(title == undefined || title == null || title == "")
                title = Strings.PROMPT_TITLE;

            modalBox.title(title);
            modalBox.text(message + '<br /><br /><input type="text" id="prompt-value" class="form-control" value="' + value + '">');
            modalBox.setButtons(modalBox.OK_CANCEL);
            modalBox.action(function(){
                if(callback != undefined && callback != null)
                    callback($("#prompt-value").val());
                else
                    console.log($("#prompt-value").val());
            });

            modalBox.make().show();
        }

        self.error = function(message, title){
            if(message == undefined || message == null || message == "")
                return;

            modalBox.clear();
            if(title == undefined || title == null || title == "")
                title = Strings.ERROR_TITLE;

            modalBox.title(title);
            modalBox.text(message);
            modalBox.setButtons(modalBox.OK_CANCEL);
            modalBox.make().show();

            throw message;
        }

        return self;
    };
});