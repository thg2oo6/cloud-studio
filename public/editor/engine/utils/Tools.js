define(function (require, exports, module) {
    'use strict';

    window.isValid = function (element) {
        if (element === null || element === undefined) return false;
        return true;
    }

    window.errorHandling = function (jqXHR, textStatus, errorThrown){
//        console.log(jqXHR);
        if(jqXHR.responseJSON == null)
            console.log(jqXHR.responseJSON);
        else{
            var error = JSON.parse(jqXHR.responseText);
            console.log(error);
        }
    }

    window.uniqueID = function (name){
        var newName = name.replace(" ", "-"),
            iD = newName,
            counter = 2;

        while($("#"+iD).length != 0){
            iD = newName + "-" + counter;
            counter++;
        }

        return iD;
    }

    window.timestamp = function(){
        return Math.round(new Date().getTime() / 1000);
    }

    if ( typeof String.prototype.startsWith != 'function' ) {
        String.prototype.startsWith = function( str ) {
            return str.length > 0 && this.substring( 0, str.length ) === str;
        }
    };

    if ( typeof String.prototype.endsWith != 'function' ) {
        String.prototype.endsWith = function( str ) {
            return str.length > 0 && this.substring( this.length - str.length, this.length ) === str;
        }
    };
});