define(function (require, exports, module) {
    'use strict';

    exports.Workspace = new function () {
        var Templates = {
            view: require("text!views/workspace/workspace.html"),
            codeView: require("text!views/workspace/code.html")
        };

        /*--- MAIN BLOCK ---*/
        var self = {},
            privates = {};

        privates.restoreIdx = 0;
        privates.activeElement = null;
        self.WindowBuilder = ClassLoader(require("engine/WindowBuilder"));

        privates.resize = function(){
            var sidebarExt = 0;
            if ($("#sidebar-ext").hasClass("online"))
                sidebarExt = $("#sidebar-ext").width() + 1;

            $(".workspace").css({
                height: window.innerHeight - $(".dashboard #topbar").height() - 1
            });

            $(".workspace .xcontainer").css({
                height: $(".workspace").height() - $(".workspace .tabs").height()
            });

            $(".workspace").animate({
                width: window.innerWidth - $("#sidebar").width() - 1 - sidebarExt,
                left: $("#sidebar").width() + 1 + sidebarExt
            });
        }

        privates.leftright = function(){
            $(document).on("click",".navi #prev",function(e){
                e.preventDefault();
                var et = $(this).parent().parent().children("ul");
                var left = parseInt(et.css('left'));

                if(left<0)
                    if(left + 85 > 0)
                        et.animate({left: 0});
                    else
                        et.animate({left: left + 85});
            });

            $(document).on("click",".navi #next",function(e){
                e.preventDefault();
                var et = $(this).parent().parent().children("ul");
                var left = parseInt(et.css('left'));

                var length = 0, last_length = 0;
                et.children('li').each(function(i,el){
                    length += $(el).width();
                });
                last_length = parseInt(et.children("li:last-child").width());

                if($(".tabs").width() < length + 85)
                    if(Math.abs(left) < Math.abs(length) - last_length - 85)
                        et.animate({left: left - 85});
            });
        }

        privates.setMode = function(editor){
            var session = editor.getSession();
            session.setFoldStyle("markbegin");
            editor.setSelectionStyle("text");
            editor.setHighlightActiveLine(true);
            editor.setDisplayIndentGuides(true);
            editor.setHighlightSelectedWord(true);
            editor.setAnimatedScroll(true);
            session.setUseSoftTabs(true);
            editor.setBehavioursEnabled(true);
            editor.setOption("scrollPastEnd", true);
            editor.setFontSize(13);

            editor.setOption({enableBasicAutocompletion: true, enableSnippets: true});
        }

        self.openTab = function(name, path, lang, content, noAdd){
            $(".workspace .tabs li.active").removeClass('active');
            $(".workspace .xcontainer .active").removeClass('active');

            var tab = {
                unique_tab: "tab-" + (window.timestamp() - privates.restoreIdx),
                name: name,
                file: path + name,
                path: path
            };

            if(!noAdd)
                self.add(tab.file, "file");
            else
                privates.restoreIdx = privates.restoreIdx - 1;

            $(".workspace .tabs ul").append(Mustache.render($("#tab-tpl").html(),tab));
            $(".workspace .xcontainer").append(Mustache.render($("#tab-ctnt").html(),tab));

            $("#" + tab.unique_tab + " .textarea").text(content);

            var editor = window.ace.edit(tab.unique_tab + "-textarea");
            editor.setTheme("ace/theme/" + CDSConfig.Theme);
            editor.getSession().setMode('ace/mode/' + lang);

            editor.getSession().on("change", function(el){
                var e = '.workspace .tabs li[data-container="' + tab.unique_tab + '"]';
                if(!$(e).hasClass('changed')){
                    $(e).data('changed',true);
                    $(e).addClass("changed");
                }
            });
            privates.setMode(editor);


            $("#" + tab.unique_tab).data('editor', editor);
            $(".workspaceLoader").hide();
        }

        self.empty = function(){
            return $(".workspace .tabs li").length == 0;
        };

        self.closeElement = function(element){
            if($(element).parent().hasClass('changed')){
                var iConfirm = confirm("Are you sure you want to close the file?");

                if(!iConfirm)
                    return;
            }

            var hasActive = $(element).parent().hasClass("active");
            var box = $(element).parent().data('container');

            $(element).parent().remove();
            $(".workspace .xcontainer section#" + box).remove();

            self.remove($(element).parent().data('file'));

            if(hasActive){

                privates.activeElement = null;
                self.lastActive();
            }
        };

        //OPENSTACK FUNCTIONALITY
        self.add = function(element, info){
            var all = localStorage.getItem("Project-" + CDSConfig.Project);
            if(all == null || all == undefined)
                all = "{ }";
            all = JSON.parse(all);

            all[element] = info;
            localStorage.setItem("Project-" + CDSConfig.Project, JSON.stringify(all) );
        };
        self.remove = function(element){
            var all = localStorage.getItem("Project-" + CDSConfig.Project);
            if(all == null || all == undefined)
                all = "{ }";
            all = JSON.parse(all);

            for(var i in all){
                if(i == element){
                    delete all[i];
                    break;
                }
            }

            localStorage.setItem("Project-" + CDSConfig.Project, JSON.stringify(all) );
        };

        self.restore = function(){
            var all = localStorage.getItem("Project-" + CDSConfig.Project);

            if(all == null || all == undefined)
                all = "{ }";
            all = JSON.parse(all);
            for(var i in all){
                if(all[i] == "app"){
                    $.ajax({
                        async: true,
                        dataType: 'json',
                        type: 'post',
                        url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/get"),
                        data: {
                            file: i
                        },
                        success: function(data){
                            self.WindowBuilder.openTab(data.basename, data.path, data.lang, data.content, true);
                        },
                        error: function(e){
                            delete all[i];
                            localStorage.setItem("Project-" + CDSConfig.Project, JSON.stringify(all));
                        }
                    });
                }else{
                    $.ajax({
                        async: true,
                        dataType: 'json',
                        type: 'post',
                        url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/get"),
                        data: {
                            file: i
                        },
                        success: function(data){
                            self.openTab(data.basename, data.path, data.lang, data.content, true);
                        },
                        error: function(e){
                            delete all[i];
                            localStorage.setItem("Project-" + CDSConfig.Project, JSON.stringify(all));
                        }
                    });
                }


            }
        }
        //OPENSTACK FUNCTIONALITY

        self.clickElement = function(element){
            $(".workspace .tabs li.active").removeClass('active');
            $(".workspace .xcontainer section.active").removeClass('active');

            var box = $(element).parent().data('container');
            $(element).parent().addClass('active');
            $('#PROJECT-FILES .content li a.active-file').removeClass("active-file");
            $('#PROJECT-FILES .content li a[data-name="' + $(element).parent().data('name').replace("//","/") + '"][data-path="' + $(element).parent().data('path').replace("//", "/") + '"]').addClass("active-file");
            $(".workspace .xcontainer section#" + box).addClass('active');

//            if(!$(".workspace .xcontainer section#" + box).hasClass("wbuilder")){
//                if($("#sidebar-ext").css('display') !== "none" && $("#sidebar-ext .active").attr("id") == "TOOLBOX"){
//                    $("#TOOLBOX .close-widget").click();
//                }
////                $("a[data-widget=TOOLBOX]").addClass("disabled");
//            }else{
//                $("a[data-widget=TOOLBOX]").removeClass("disabled");
//                if($("#sidebar-ext").css('display') !== "none" && $("#sidebar-ext .active").attr("id") != "TOOLBOX"){
//                    $("a[data-widget=TOOLBOX]").click();
//                }
//            }

            $(".workspaceLoader").hide();

            privates.activeElement = element;
        };
        self.lastActive = function(){
            if(self.empty())
                return;

            if(privates.activeElement == null)
                privates.activeElement = $(".tabs li").last().children(".title");

            self.clickElement(privates.activeElement);
        };


        self.render = function () {
            $(".dashboard").append(Templates.view);
            $(".dashboard").append(Templates.codeView);


            DOMEvent.addReady(privates.resize);
            DOMEvent.addResize(privates.resize);
            HookupEvent.add('sidebar_before_loadfinish', privates.resize);
            HookupEvent.add('close_sidebar', privates.resize);

            //click(".tabs li .title", window.OpenStack.clickElement);
            $(document).on("click", ".tabs li .title", function (e){
                e.preventDefault();

                self.clickElement(this);
            });

            $(document).on("click", ".tabs li .close-tab", function (e){
                e.preventDefault();

                self.closeElement(this);
            });

            self.WindowBuilder.init();
            self.WindowBuilder.render(self);

            self.restore();
//            $(".dashboard").append(Templates.windowView);
//
//            for (var element in elements) {
//
//                var _options = elements[element].forge(Strings[elements[element].id]);
//                $("#sidebar-ext").append(Mustache.render(Templates.containerView, _options));
//                if (elements[element].refill !== undefined)
//                    elements[element].refill();
//
//            }
//
//            privates.attachEvents();
        }

        return self;
    };
});