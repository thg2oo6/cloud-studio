define(function (require, exports, module) {
    'use strict';

    window.HookupEvent = new function () {
        var self = {},
            privates = {};

        self.define = function (event) {
            if (isValid(privates[event])) return;

            privates[event] = [];
        }

        self.add = function (event, callback) {
            if (!isValid(event) || !isValid(callback)) return;
            privates[event].push(callback);
        }

        self.remove = function (event, id) {
            if (!isValid(privates[event])) return;
            if (isValid(id)) delete privates[event][id];

            delete privates[event];
        }

        self.empty = function (event) {
            if (!isValid(privates[event])) return true;

            return privates[event].length === 0;
        }

        self.get = function (event, id) {
            if (!isValid(privates[event])) return null;
            if (isValid(id)) return privates[event][id];

            return privates[event];
        }

        self.execute = function (event, id) {
            if (!isValid(privates[event])) return;
            if (isValid(id)) return privates[event][id]();

            for (var i in privates[event])
                privates[event][i]();
        }

        return self;
    };
});