define(function (require, exports, module) {
    'use strict';

    exports.MenuEvent = new function () {
        var self = {};

        self.enable = function (menuElement) {
            var element = menuElement.replace(".", "_").toUpperCase();
            $("#" + element).removeClass("disable");
        }

        self.disable = function (menuElement) {
            var element = menuElement.replace(".", "_").toUpperCase();
            $("#" + element).addClass("disable");
        }

        self.attach = function (menuElement, eventCallback) {
            var element = menuElement.replace(".", "_").toUpperCase();

            $(document).on("click", "#" + element, function (e) {
                if (!$(this).hasClass("disabled"))
                    eventCallback(e);
            });
        }

        self.detach = function (menuElement) {
            var element = menuElement.replace(".", "_").toUpperCase();

            $(document).off("click", "#" + element);
        }

        self.multipleAttach = function (menuElements, eventCallback) {
            var elements = menuElements.split(",");

            for (var i in elements)
                self.attach(elements[i].trim(), eventCallback);
        }

        self.multipleDetach = function (menuElements) {
            var elements = menuElements.split(",");

            for (var i in elements)
                self.detach(elements[i].trim());
        }

        $(document).on("click", ".disabled", function (e) {
            e.preventDefault();

            console.log("This element is disabled");
            return false;
        });

        return self;
    };
});