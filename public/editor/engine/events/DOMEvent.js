define(function (require, exports, module) {
    'use strict';

    window.DOMEvent = new function () {
        var self = {},
            privates = {};

        privates.onReady = [];
        privates.onResize = [];

        self.add = function (event, callback) {
            if (!isValid(event) || !isValid(callback)) return;

            privates[event].push(callback);
        }

        self.addReady = function (callback) {
            self.add("onReady", callback);
        }

        self.addResize = function (callback) {
            self.add("onResize", callback);
        }

        self.onResize = function () {
            for (var i in privates.onResize)
                privates.onResize[i]();
        }

        self.onReady = function () {
            for (var i in privates.onReady)
                privates.onReady[i]();
        }

        return self;
    };
});