define(function (require, exports, module) {
    'use strict';

    exports.MenuHelper = new function () {
        var self = {};
        var MenuItem = ClassLoader(require("engine/entity/MenuItem"));

        self.MenuButton = function (options) {
            $.extend(options, {type: 'button'});
            return new MenuItem(options);
        }

        self.MenuDropdown = function (options, submenus) {
            var _submenus = [];
            for (var i = 0; i < submenus.length; i++) {
                _submenus.push(new MenuItem(submenus[i]));
            }
            $.extend(options, {type: 'dropdown', submenu: _submenus});
            return new MenuItem(options);
        }

        self.MenuDivider = function MenuDivider(options) {
            return new MenuItem({type: 'divider'});
        }

        return self;
    };
});