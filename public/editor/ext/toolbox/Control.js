define(function (require, exports, module) {
    'use strict';

    exports.Control = new function () {
        var Messages = ClassLoader(require("engine/utils/Messages"));
        var self = {};

        self.navigation = function(target, de){
            var $el = $("<ul />",{
                class: "nav navbar-nav",
                id: window.uniqueID("navbar")
            }).droppable(de).appendTo(target);
        }

        self.button = function(target, de){
            var name = prompt("Button name","New button");
            if(!name)
                return;

            if($(target).is("ul")){
                var $el = $("<li />")
                var $a = $("<a/>",{
                    text: name,
                    id: window.uniqueID(name),
                    href: "javascript:void(0);"
                }).appendTo($el);
                $el.appendTo(target);
            }else
                var $el = $("<button />",{
                    class: "btn btn-default",
                    text: name,
                    id: window.uniqueID(name),
                }).appendTo(target);
        }
        self.buttonGroup = function(target, de){
            var $el = $("<section />",{
                class: "btn-group",
                id: window.uniqueID("btn-group")
            }).droppable(de).appendTo(target);
        }
        self.buttonToolbar = function(target, de){
            var $el = $("<section />",{
                class: "btn-toolbar",
                id: window.uniqueID("btn-toolbar")
            }).droppable(de).appendTo(target);
        }

        return self;
    };
});