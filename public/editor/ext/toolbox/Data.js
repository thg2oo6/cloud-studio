define(function (require, exports, module) {
    'use strict';

    exports.Data = new function () {
        var Messages = ClassLoader(require("engine/utils/Messages"));
        var self = {};

        self.text = function(target, de){
            Messages.prompt("Enter the text:", "", function(data){
                if(!data) return;

                var $el = $("<section />",{
                    id: window.uniqueID("text"),
                    class: "textinput",
                    text: data
                }).appendTo(target);
            });
        }
        self.list = function(target, de){
            var $el = $("<ul />",{
                class: 'list-view',
                id: window.uniqueID("list-view")
            }).droppable(de).appendTo(target);
        }
        self.listItem = function(target, de){
            var $el = $("<li />",{
                class: 'list-item',
                id: window.uniqueID("list-item")
            }).droppable(de).appendTo(target);
        }

        return self;
    };
});