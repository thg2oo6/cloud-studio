define(function (require, exports, module) {
    'use strict';

    exports.Container = new function () {
        var Messages = ClassLoader(require("engine/utils/Messages"));
        var self = {};

        self.header = function (target, de) {
            var active_CTN = $(".workspace .xcontainer .active");

            if (!active_CTN.children("section[data-boxtype=header]").hasClass("navbar")) {
                $(target).addClass('navbar navbar-default');
                $(target).attr('role', "navigation");
            } else {

            }
//        $("#elem-header").parent().draggable('disable').addClass('disabled');
        }

        self.footer = function (target, de) {
            var active_CTN = $(".workspace .xcontainer .active");

            if (!active_CTN.children("section[data-boxtype=footer]").hasClass("navbar")) {
                $(target).addClass('navbar navbar-inverse');
                $(target).attr('role', "navigation");
            } else {

            }
//        $("#elem-header").parent().draggable('disable').addClass('disabled');
        }

        self.container = function (target, de) {
            var $el = $("<section />", {
                class: "container",
                id: window.uniqueID("container")
            }).droppable(de).appendTo(target);
        }
        self.row = function (target, de) {
            var $el = $("<section />", {
                class: "row",
                id: window.uniqueID("row")
            }).droppable(de).appendTo(target);
        }
        self.section = function (target, de) {
            Messages.prompt("Section size", "6", function (size) {
                if (size == null) return;
                if (size < 1) {
                    Messages.notification("Invalid size!");
                    return;
                }
                if (size > 12) size = 12;

                size = parseInt(size);
                var $el = $("<section />", {
                    class: "col-md-" + size,
                    id: window.uniqueID("column")
                }).droppable(de).appendTo(target);
            });
        }
        self.spacer = function (target, de) {
            var size = Messages.prompt("Spacer size", "1", function (size) {
                if (size == null) return;
                if (size < 1) {
                    Messages.notification("Invalid size!");
                    return;
                }
                if (size > 12) size = 12;

                size = parseInt(size);
                var $el = $("<section />", {
                    class: "col-md-" + size + " not-droppable",
                    id: window.uniqueID("spacer")
                }).appendTo(target);
            });
        }

        return self;
    };
});