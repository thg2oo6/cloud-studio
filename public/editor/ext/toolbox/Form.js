define(function (require, exports, module) {
    'use strict';

    exports.Form = new function () {
        var Messages = ClassLoader(require("engine/utils/Messages"));
        var self = {};

        self.label = function(target, de){
            Messages.prompt("Enter the label:", "", function(data){
                if(!data) return;

                var $el = $("<label />",{
                    id: window.uniqueID("label"),
                    text: data
                }).appendTo(target);
            });
        }

        self.form = function(target, de){
            var $el = $("<form />",{
                id: window.uniqueID("form"),
                action: '',
                method: 'POST',
                class: 'form'
            }).droppable(de).appendTo(target);
        }

        self.textbox = function(target, de){
            Messages.prompt("Enter the placeholder:", "", function(data){
                var elem = window.uniqueID("textbox");
                var $el = $("<input />",{
                    id: elem,
                    name: elem,
                    class: 'form-control',
                    type: 'text',
                    placeholder: data
                }).appendTo(target);
            });
        }
        self.password = function(target, de){
            Messages.prompt("Enter the placeholder:", "", function(data){
                var elem = window.uniqueID("password");
                var $el = $("<input />",{
                    id: elem,
                    name: elem,
                    class: 'form-control',
                    type: 'password',
                    placeholder: data
                }).appendTo(target);
            });
        }
        self.email = function(target, de){
            Messages.prompt("Enter the placeholder:", "", function(data){
                var elem = window.uniqueID("email");
                var $el = $("<input />",{
                    id: elem,
                    name: elem,
                    class: 'form-control',
                    type: 'email',
                    placeholder: data
                }).appendTo(target);
            });
        }
        self.textarea = function(target, de){
            Messages.prompt("How many rows?", "5", function(data){
                if(!data) data = "5";

                var elem = window.uniqueID("textarea");
                var $el = $("<textarea />",{
                    id: elem,
                    name: elem,
                    class: 'form-control',
                    rows: parseInt(data)
                }).appendTo(target);
            });
        }

        self.helpblock = function(target, de){
            Messages.prompt("Enter the help block:", "", function(data){
                if(!data) return;

                var $el = $("<span />",{
                    id: window.uniqueID("helpblock"),
                    class: "help-block",
                    text: data
                }).appendTo(target);
            });
        }
        self.checkbox = function(target, de){
            Messages.prompt("Enter the checkbox content:", "", function(data){
                if(!data) return;

                var el = window.uniqueID("checkbox");
                var $el = $("<section />",{
                    id: "el-" + el,
                    class: "checkbox"
                });

                var label = $("<label/>");
                $("<input/>",{
                    type: "checkbox",
                    name: el,
                    id: el
                }).appendTo(label);
                label.append(data);
                label.appendTo($el);
                $el.appendTo(target);
            });
        }

        return self;
    };
});