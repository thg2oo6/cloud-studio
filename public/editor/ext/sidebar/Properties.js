define(function (require, exports, module) {
    'use strict';

    exports.Properties = new function () {

        var Strings = require("nls/strings"),
            ToolboxStr = require("text!views/sidebar/properties.html");

        /*--- MAIN BLOCK ---*/
        var self = {},
            privates = {};

        privates.activeObject = null;

        privates.modalWindow = new function () {
            var self = {},
                title = "",
                action = "";

            self.clear = function () {
                title = "";
                action = "";
            }

            self.title = function (txt) {
                if (txt == undefined)
                    return title;
                title = txt;
            }

            self.action = function (txt) {
                if (action == undefined)
                    return action;
                action = txt;
            }

            $(".modal_two #modal-no").text(Strings.CANCEL);
            $(".modal_two #modal-yes").text(Strings.SAVE);
            self.make = function () {
                $(".modal_two .modal-title").html(title);
                $(".modal_two #modal-yes").data("action", action);

                return self;
            }

            self.display = function (text) {
                $(".modal_two #modal-yes").data("action", action);

                $(".modal_two").css("display", "block");
                if ($(".modal_two #modalText").data('editor') == null) {
                    var lang = "css";
                    if (action == "click")
                        lang = "javascript";

                    var editor = window.ace.edit("modalText");
                    editor.setTheme("ace/theme/eclipse");
                    editor.getSession().setMode('ace/mode/' + lang);

                    privates.setMode(editor);

                    $(".modal_two #modalText").css("display", "block");
                    $(".modal_two #modalText").data('editor', editor);
                }
                else{
                    var lang = "css";
                    if (action == "click")
                        lang = "javascript";
                    $(".modal_two #modalText").data('editor').getSession().setMode('ace/mode/' + lang);
                }
                console.log($(".modal_two #modalText").data('editor'));
                $(".modal_two #modalText").data('editor').setValue(text);
                $(".modal_two #modalText").data('editor').resize();

                $(".modal_two").modal('show');
            }

            return self;
        };

        privates.modal = privates.modalWindow;

        privates.beginComment = function (fct_name) {
            return "\/\/BEGIN FUNCTION " + fct_name + "\n";
        }
        privates.endComment = function (fct_name) {
            return "\n" + "\/\/END FUNCTION " + fct_name + "\n";
        }

        privates.apply = {};
        privates.apply.default = function (el) {
            el.attr('class', $("#ObjectClass").val());
            el.attr('id', $("#ObjectID").val());
            el.text($("#ObjectText").val());
        };
        privates.apply.form = function (el) {
            el.attr('class', $("#ObjectClass").val());
            el.attr('id', $("#ObjectID").val());
        };
        privates.apply.input = function (el) {
            el.attr('class', $("#ObjectClass").val());
            el.attr('id', $("#ObjectID").val());
            el.attr('name', $("#ObjectName").val());
            el.attr('placeholder', $("#ObjectPlaceholder").val());
//        el.attr("required", $("#ObjectRequired").val());
        };
        privates.apply.textarea = function (el) {
            el.attr('class', $("#ObjectClass").val());
            el.attr('id', $("#ObjectID").val());
            el.attr('name', $("#ObjectName").val());
            el.attr('placeholder', $("#ObjectPlaceholder").val());
            el.attr('rows', $("#ObjectRows").val());
//        el.attr("required", $("#ObjectRequired").attr("checked")?true:false);
        };
        privates.apply.checkbox = function (el) {
            el.attr('class', $("#ObjectClass").val());
            el.attr('id', $("#ObjectID").val());
            el.attr('name', $("#ObjectName").val());
//        el.attr("required", $("#ObjectRequired").attr("checked")?true:false);
        };
        privates.apply.button = function (el) {
            el.attr('class', 'btn ' + $("#ObjectStyle").val());
            el.attr('id', $("#ObjectID").val());
            el.text($("#ObjectText").val());
        };

        $(document).on("click", "#ApplySettings", function (e) {
            e.preventDefault();

            var e = $(".workspace .tabs li.active");
            if (!e.hasClass('changed')) {
                e.data('changed', true);
                e.addClass("changed");
            }

            if (privates.activeObject.is("button")) {
                privates.apply.button($(privates.activeObject));
                return;
            }
            if (privates.activeObject.is("form")) {
                privates.apply.form($(privates.activeObject));
                return;
            }
            if (privates.activeObject.is("input")) {
                privates.apply.input($(privates.activeObject));
                return;
            }
            if (privates.activeObject.is("textarea")) {
                privates.apply.textarea($(privates.activeObject));
                return;
            }
            if (privates.activeObject.is("checkbox")) {
                privates.apply.checkbox($(privates.activeObject));
                return;
            }
//        if(activeObject.is("textinput")){ apply.textinput($(activeObject)); return; }

            privates.apply.default($(privates.activeObject));

        });

        $(document).on("click", ".modal_two #modal-yes", function (e) {
            e.preventDefault();

            if ($(this).data('action') == "click") {
                var action = $(".workspace .xcontainer .wbuilder.active .actions").text();
                action = action.replace("\r", "");
                if (action.match(privates.beginComment($(privates.activeObject).attr('id')) + "((.|[\n])*)" + privates.endComment($(privates.activeObject).attr('id')))) {
                    var spl1 = action.split(privates.beginComment($(privates.activeObject).attr('id')), 2);
                    var spl2 = spl1[1].split(privates.endComment($(privates.activeObject).attr('id')), 2);

                    action = spl1[0]
                        + privates.beginComment($(privates.activeObject).attr('id')) + $(".modal_two #modalText").data('editor').getValue() + privates.endComment($(privates.activeObject).attr('id'))
                        + spl2[1];

                } else {
                    action += privates.beginComment($(privates.activeObject).attr('id')) + $(".modal_two #modalText").data('editor').getValue() + privates.endComment($(privates.activeObject).attr('id'));
                }

                var e = $(".workspace .tabs li.active");
                if (!e.hasClass('changed')) {
                    e.data('changed', true);
                    e.addClass("changed");
                }

                $(".workspace .xcontainer .wbuilder.active .actions").text(action);
            }
            if ($(this).data('action') == "style") {
                var action = $(".modal_two #modalText").data('editor').getValue();

                var e = $(".workspace .tabs li.active");
                if (!e.hasClass('changed')) {
                    e.data('changed', true);
                    e.addClass("changed");
                }

                $(".workspace .xcontainer .wbuilder.active style").text(action);
            }
        });

        $(document).on("click", "#onClickView", function (e) {
            e.preventDefault();

            var action = $(".workspace .xcontainer .wbuilder.active .actions").text();
            action = action.match(
                privates.beginComment($(privates.activeObject).attr('id')) + "((.|[\n])*)" + privates.endComment($(privates.activeObject).attr('id'))
            );

            action = action == null ? '' : action[1];

            privates.modal.clear();
            privates.modal.title("Editing Actions for: #" + $(privates.activeObject).attr('id'));
            privates.modal.action("click");
            privates.modal.make().display(action);

//        $(".modal_two #modalText").data('editor').setValue(action);
        });

        $(document).on("click", "#onClickRemove", function (e) {
            e.preventDefault();

            var action = $(".workspace .xcontainer .wbuilder.active .actions").text();
            action = action.replace(
                privates.beginComment($(privates.activeObject).attr('id')) + "((.|[\n])*)" + privates.endComment($(privates.activeObject).attr('id')),
                ""
            );

            $(".workspace .xcontainer .wbuilder.active .actions").text(action);
        });

        $(document).on("click", "#onStyleView", function (e) {
            e.preventDefault();

            var action = $(".workspace .xcontainer .wbuilder.active style").text();

            privates.modal.clear();
            privates.modal.title("Editing Style for application");
            privates.modal.action("style");
            privates.modal.make().display(action);

//        $(".modal_two #modalText").data('editor').setValue(action);
        });

        $(document).on("click", "#onStyleRemove", function (e) {
            e.preventDefault();

            var action = $(".workspace .xcontainer .wbuilder.active style").text();
            action = action.replace(
                privates.beginComment2($(privates.activeObject).attr('id')) + "((.|[\n])*)" + privates.endComment2($(privates.activeObject).attr('id')),
                ""
            );

            $(".workspace .xcontainer .wbuilder.active style").text(action);
        });

        privates.setMode = function (editor) {
            var session = editor.getSession();
            session.setFoldStyle("markbegin");
            editor.setSelectionStyle(false ? "line" : "text");
            editor.setHighlightActiveLine(true);
            editor.setDisplayIndentGuides(true);
            editor.setHighlightSelectedWord(true);
            editor.setAnimatedScroll(true);
            session.setUseSoftTabs(true);
            editor.setBehavioursEnabled(true);
            editor.setOption("scrollPastEnd", true);
            editor.setFontSize(13);

            editor.setOption({enableBasicAutocompletion: true, enableSnippets: true});
        }

        self.id = "PROPERTIES";

        self.fill = function (callback) {
            $("#PROPERTIES .content").html(Mustache.render(ToolboxStr, Strings));

            callback();
        }

        self.refill = function (callback) {
            if ($(".contentLoader").css('display') != "block")
                $(".contentLoader").show();

            this.fill(function () {
                $(".contentLoader").hide();
            });
        }

        self.forge = function (name) {
            return {
                id: self.id,
                name: name
            }
        }

        self.loadObject = function (element) {
            $("#ObjectID").val(element.attr('id'));
            $("#ObjectClass").val(element.attr('class').replace("drop-hover", ""));
            $("#ObjectName").val(element.attr('name'));
            $("#ObjectPlaceholder").val(element.attr('placeholder'));
            $("#ObjectRows").val(element.attr('rows'));
            $("#ObjectType").val(element.get(0).tagName);
            $("#ObjectText").val(element.text());

            if (element.is("input") || element.is("textarea") || element.is("checkbox")) {
                $("#ObjectRequired").val(element.attr('required'));
                $(".FormObject").show();
            }
            else
                $(".FormObject").hide();

            if (element.is("textarea"))
                $(".TextareaObject").show();
            else
                $(".TextareaObject").hide();

            if (element.is("form"))
                $(".FormCtnt").show();
            else
                $(".FormCtnt").hide();

            if (element.is("button")) {
                $("#ObjectText").val(element.text());
                var style = element[0].className.split(" ");
                for (var i in style) {
                    if (style[i].substring(0, 4) == "btn-")
                        $("#ObjectStyle").val(style[i].trim());
                }
                $(".ButtonObject").show();
                $(".OthersClass").hide();
            } else {
                $(".OthersClass").show();
                $(".ButtonObject").hide();
            }

            if (element.is("input") || element.is("textarea") || element.is("checkbox") || element.is("form")) {
                $(".TextObj").hide();
            } else {
                $(".TextObj").show();
            }

            if (element.is("section") && $(element).hasClass("textinput")) {
                $("#ObjectText").val(element.text());
            }

            privates.activeObject = element;
            $("a[data-widget=PROPERTIES]").click();
        };

        window.PropertyLoader = self.loadObject;

        return self;
    };
});