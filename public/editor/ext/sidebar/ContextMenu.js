define(function (require, exports, module) {
    'use strict';

    exports.ContextMenu = new function () {

        var Strings = require("nls/strings"),
            Messages = ClassLoader(require("engine/utils/Messages")),
            Workspace = ClassLoader(require("engine/Workspace"))
            ;
        /*--- MAIN BLOCK ---*/

        var self = {},
            elements = {},
            privates = {},
            theElement = null;

        privates.init = function () {
            elements.ctxMenu = $("<div/>", {
                id: "files-ctx"
            });

            elements.ctxMenuList = $("<ul />", {
                class: "dropdown-menu",
                role: "menu"
            });
            elements.ctxMenuList.appendTo(elements.ctxMenu);
        }

        privates.addElement = function (id, name) {
            var li = $("<li/>");
            $("<a/>", {
                id: id,
                text: name,
                tabindex: -1
            }).appendTo(li);

            li.appendTo(elements.ctxMenuList);
        }

        privates.addSeparator = function () {
            $("<li/>", {
                class: "divider"
            }).appendTo(elements.ctxMenuList);
        }

        privates.parent = null;

        privates.events = {};
        privates.events.newFile = function (name) {
            var path = "";

            if (theElement != null) {
                path = theElement.data('path');

                if (theElement.hasClass('directory'))
                    path += theElement.data('name');

                path += "\\";
            }

            if (name == undefined || name == null || name.length == 0) {
                return false;
            }

            name = name.replace("-","_");

            $(".contentLoader").show();
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/newFile"),
                data: {
                    path: path,
                    file: name
                },
                success: function (data) {
                    $(".workspaceLoader").show();
                    if (data.status == "OK") {
                        privates.parent.refill();
                        $.ajax({
                            url: CDSConfig.APIPath("lang"),
                            data: {file: name},
                            success: function (data) {
                                Workspace.openTab(name, path, data.lang, "");
                                $(".contentLoader").hide();
                                privates.parent.refill();
                            },
                            dataType: 'json',
                            type: 'POST',
                            error: function (e) {
                                $(".contentLoader").hide();
                                privates.parent.refill();
                                Messages.error(e.responseJSON.message);
                            }
                        });
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    $(".contentLoader").hide();
                    privates.parent.refill();
                    Messages.error(e.responseJSON.message);
                }
            });

            return true;
        };
        privates.events.newFolder = function (name) {
            var path = "";
            if (theElement != null) {
                path = theElement.data('path');

                if (theElement.hasClass('directory'))
                    path += theElement.data('name');

                path += "\\";
            }

            if (name == undefined || name == null || name.length == 0) {
                return false;
            }

            name = name.replace("-","_");

            $(".contentLoader").show();
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/newFolder"),
                data: {
                    path: path,
                    folder: name
                },
                success: function (data) {
                    if (data.status == "OK") {
                        $(".contentLoader").hide();
                        privates.parent.refill();
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    $(".contentLoader").hide();
                    privates.parent.refill();
                    Messages.error(e.responseJSON.message);
                }
            });

            return true;
        };
        privates.events.newWindow = function (name) {
            var path = "";

            if (theElement != null) {
                path = theElement.data('path');

                if (theElement.hasClass('directory'))
                    path += theElement.data('name');

                path += "\\";
            }

            if (name == undefined || name == null || name.length == 0) {
                return false;
            }

            name = name.replace("-","_");

            if (!name.endsWith(".app.php"))
                name = name + ".app.php";

            $(".contentLoader").show();
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/newWindow"),
                data: {
                    path: path,
                    file: name
                },
                success: function (data) {
                    $(".workspaceLoader").show();
                    if (data.status == "OK") {
                        privates.parent.refill();
                        $.ajax({
                            url: CDSConfig.APIPath("lang"),
                            data: {file: name},
                            success: function (data) {
                                Workspace.WindowBuilder.openTab(name, path, data.lang, "");
                                $(".contentLoader").hide();
                                privates.parent.refill();
                            },
                            dataType: 'json',
                            type: 'POST',
                            error: function (e) {
                                $(".contentLoader").hide();
                                privates.parent.refill();
                                Messages.error(e.responseJSON.message);
                            }
                        });
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    $(".contentLoader").hide();
                    privates.parent.refill();
                    Messages.error(e.responseJSON.message);
                }
            });

            return true;
        };
        privates.events.renameFile = function (name) {
            if (name == undefined || name == null)
                Messages.error(Strings.NOFILE);

            $(".contentLoader").show();
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/rename"),
                data: {
                    path: theElement.data('path'),
                    old_name: theElement.data('name'),
                    new_name: name
                },
                success: function (data) {
                    if (data.status == "OK") {
                        var $e = $('.workspace .tabs li[data-name="' + theElement.data('name').replace("//", "/") + '"][data-path="' + theElement.data('path').replace("//", "/") + '"]');
                        if ($e.length > 0) {
                            Workspace.remove($e.data("file"));

                            $e.children('.title').html(name);
                            $e.attr('data-name', name);
                            $e.attr('data-file', theElement.data('path') + name);
                            $e.attr('data-path', theElement.data('path'));

                            $e.data('name', name);
                            $e.data('file', theElement.data('path') + name);
                            $e.data('path', theElement.data('path'));

                            Workspace.add($e.data("file"), "file");
                        }
                        $(".contentLoader").hide();
                        privates.parent.refill();
                        theElement = null;
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    $(".contentLoader").hide();
                    privates.parent.refill();
                    Messages.error(e.responseJSON.message);
                }
            });

        };
        privates.events.deleteFile = function () {
            $(".contentLoader").show();

            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/delete"),
                data: {
                    path: theElement.data('path'),
                    file: theElement.data('name')
                },
                success: function (data) {
                    if (data.status == "OK") {
                        var $e = $('.workspace .tabs li[data-file="' + (theElement.data('path') + theElement.data('name')).replace("//", "/") + '"]');
                        if ($e.length > 0)
                            Workspace.closeElement($e.children('.title'));
                        $(".contentLoader").hide();
                        privates.parent.refill();
                        theElement = null;
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    var $e = $('.workspace .tabs li[data-file="' + (theElement.data('path') + theElement.data('name')).replace("//", "/") + '"]');
                    if ($e.length > 0)
                        Workspace.closeElement($e.children('.title'));
                    $(".contentLoader").hide();
                    privates.parent.refill();
                    Messages.error(e.responseJSON.message);
                }
            });
        };

        privates.newFile = function () {
            Messages.prompt(Strings.NEWFILE_LABEL, "untitled_" + window.timestamp() + ".txt", privates.events.newFile);
        }

        privates.newFolder = function () {
            Messages.prompt(Strings.NEWFOLDER_LABEL, "folder_" + window.timestamp(), privates.events.newFolder);
        }

        privates.newWindow = function () {
            Messages.prompt(Strings.NEWWINDOW_LABEL, "ui_" + window.timestamp() + ".app.php", privates.events.newWindow);
        }

        privates.renameFile = function () {
            Messages.prompt(Strings.RENAME_LABEL, theElement.data('name'), privates.events.renameFile);
        }

        privates.deleteFile = function () {
            Messages.confirm(sprintf(Strings.DELETE_CONFIRMATION, theElement.data('name')), privates.events.deleteFile);
        }

        self.newFile = privates.newFile;
        self.newFolder = privates.newFolder;
        self.newWindow = privates.newWindow;
        self.renameFile = privates.renameFile;
        self.deleteFile = privates.deleteFile;

        privates.action = function (box, parent) {
            privates.parent = parent;
            $(box).contextmenu({
                target: '#files-ctx',
                before: function (e, element) {
                    e.preventDefault();

                    $(box + " a.focused").removeClass('focused');

                    if (e.target.className == 'elementname') {
                        $(e.target).parent().addClass('focused');

                        $("#files-ctx #rename").removeClass('disabled');
                        $("#files-ctx #delete").removeClass('disabled');

//                    if($(e.target).parent().data('type')!=null)
//                        $("#files-ctx #folder, #files-ctx #file").addClass('disabled');
//                    else
//                        $("#files-ctx #folder, #files-ctx #file").removeClass('disabled');

                        theElement = $(e.target).parent();
                    } else {
                        $("#files-ctx #rename").addClass('disabled');
                        $("#files-ctx #delete").addClass('disabled');

                        theElement = null;
                    }

                    return true;
                },
                onItem: function (event, element) {
                    event.preventDefault();

                    if ($(element).hasClass('disabled'))
                        return false;

                    var id = $(element).attr('id');
                    console.log(id);
                    switch (id) {
                        case 'file':
                            privates.newFile();
                            break;
                        case 'folder':
                            privates.newFolder();
                            break;
                        case 'window':
                            privates.newWindow();
                            break;
                        case 'rename':
                            privates.renameFile();
                            break;
                        case 'delete':
                            privates.deleteFile();
                            break;
                        case 'refresh':
                            parent.refill();
                            break;
                        default:
                            console.log("Feature not implemented");
                    }

                    //return true;

                },
                onClose: function () {
                    $(box + " a.focused").removeClass('focused');
                }
            });
        }

        self.render = function (box, parent) {
            privates.init(box);
            privates.addElement("file", Strings.CTX_NEWFILE);
            privates.addElement("folder", Strings.CTX_NEWFOLDER);
            privates.addElement("window", Strings.CTX_NEWWINDOW);
            privates.addSeparator();
            privates.addElement("rename", Strings.CTX_RENAME);
            privates.addElement("delete", Strings.CTX_DELETE);
            privates.addSeparator();
            privates.addElement("refresh", Strings.CTX_REFRESH);

            $(box).parent().append(elements.ctxMenu);

            privates.action(box, parent);
        }

        return self;
    };
});