define(function (require, exports, module) {
    'use strict';

    exports.ProjectFile = new function () {
        /*--- MAIN BLOCK ---*/
        var self = {},
            privates = {},
            ctxMenu = ClassLoader(require("ext/sidebar/ContextMenu")),
            Workspace = ClassLoader(require("engine/Workspace"));

        privates.openDirs = {};
        privates.shownElements = [];
        privates.firstEntry = true;

        privates.iconType = function (type) {
            switch (type) {
                case "app":
                    return "glyphicon-list-alt";
                    break;
                case "directory":
                    return "glyphicon-folder-close";
                    break;
                default:
                    return "glyphicon-file";
            }
        }

        privates.parsePaths = function (id, data, base) {
            var path = '<ul id="folder-' + id + '">';
            for (var i in data) {
                var ouricon = "";
                path += '<li> <a ';

                if (data[i].type == "directory")
                    path += 'data-folder="' + data[i].id + '"';
                else
                    path += 'id="file-' + data[i].id + '"';

                if (data[i].file.search(".app") >= 0) {
                    path += ' data-type="app"'
                    ouricon = "app";
                }

                path += ' data-path="' + base + '" data-name="' + data[i].file + '" class="' + data[i].type + '"';
                path += '><span class="glyphicon ' + privates.iconType(data[i].type) + '"></span> <span class="elementname">' + data[i].file + '</span></a>';


                if (data[i].type == "directory") {
                    if (privates.firstEntry) privates.openDirs[base + data[i].file] = false;
                    if (privates.openDirs[base + data[i].file] == true) privates.shownElements.push({path: base, name: data[i].file});
                    path += "\n" + privates.parsePaths(data[i].id, data[i].content, base + data[i].file + "/");
                }

                path += "</li>\n";
            }

            return path + "</ul>\n";
        }

        privates.unloadActions = function (box) {
            $(document).off("click", box + " .directory");
            $(document).off("click", box + " .file");
            $(document).off("click", box + " .app");
        }

        privates.loadActions = function (box) {
            privates.unloadActions(box);

            $(document).on("click", box + " .app", function (e) {
                e.preventDefault();

                $(".workspaceLoader").show();
                var elem = $(this);

                var tab_elem = $('.workspace .tabs li[data-file="' + elem.data('name') + elem.data('path') + '"]');
                if (tab_elem.length > 0) {
                    Workspace.clickElement(tab_elem.children('.title'));
                    return;
                }

                $(box + " .file.active-file").removeClass('active-file');
                elem.addClass('active-file');

                var the_file = $(this).data('path') + $(this).data('name');
                $.ajax({
                    url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/get"),
                    data: {
                        file: the_file
                    },
                    success: function (data) {
                        //console.log(data);
                        Workspace.WindowBuilder.openTab(elem.data('name'), elem.data('path'), data.lang, data.content);
                    },
                    dataType: 'json',
                    type: 'POST',
                    error: window.errorHandling
                });

            });

            $(document).on("click", box + " .directory", function (e) {
                e.preventDefault();

                $(this).children(".glyphicon").toggleClass("glyphicon-folder-open");
                $(this).children(".glyphicon").toggleClass("glyphicon-folder-close");
                $(this).parent().children("#folder-" + $(this).data('folder')).slideToggle();

                //TODO: Refactor this part to be safer
                privates.openDirs[$(this).data('path') + $(this).data('name')] = !$(this).children(".glyphicon").hasClass("glyphicon-folder-close");
            });

            $(document).on("click", box + " .file", function (e) {
                e.preventDefault();

                $(".workspaceLoader").show();
                var elem = $(this);

//                if(elem.hasClass('active-file')){
//                    $(".workspaceLoader").hide();
//                    return;
//                }

                var tab_elem = $('.workspace .tabs li[data-file="' + elem.data('name') + elem.data('path') + '"]');
                if (tab_elem.length > 0) {
                    Workspace.clickElement(tab_elem.children('.title'));
                    return;
                }

                $(box + " .file.active-file").removeClass('active-file');
                elem.addClass('active-file');

//            console.log("I have to load: " + $(this).data('name'));
                var the_file = $(this).data('path') + $(this).data('name');
                $.ajax({
                    url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/get"),
                    data: {
                        file: the_file
                    },
                    success: function (data) {
                        Workspace.openTab(elem.data('name'), elem.data('path'), data.lang, data.content);
                    },
                    dataType: 'json',
                    type: 'POST',
                    error: window.errorHandling
                });
            });

            ctxMenu.render(box, self);
        }

        self.id = "PROJECT-FILES";

        self.fill = function (callback) {
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/contents"),
                data: {},
                success: function (data) {
                    var element = "#" + self.id + " .content";
                    $(element).text("");
                    $(element).append(privates.parsePaths(0, data.tree, ''));
                    privates.loadActions(element);
                    if (privates.firstEntry) privates.firstEntry = false;

                    for (var i in privates.shownElements) {
                        //console.log(element + " a[data-path='" + privates.shownElements[i].path + "'][data-name='" + privates.shownElements[i].name + "']");
                        $(element + " a[data-path='" + privates.shownElements[i].path + "'][data-name='" + privates.shownElements[i].name + "']").click();
                    }
                    privates.shownElements = [];


                    if (callback !== undefined)
                        callback();
                    //                console.log(data);
                },
                dataType: 'json',
                type: 'GET',
                error: window.errorHandling
            });
        }

        self.refill = function () {
            self.fill();
        }

        self.forge = function (name) {
            return {
                id: self.id,
                name: name
            }
        }

        return self;
    };
});