define(function (require, exports, module) {
    'use strict';

    exports.Toolbox = new function () {

        var Strings     = require("nls/strings"),
            ToolboxStr  = require("text!views/sidebar/toolbox.html");

        /*--- MAIN BLOCK ---*/
        var self = {},
            privates = {};

        self.id = "TOOLBOX";

        self.fill = function (callback) {
            $("#TOOLBOX .content").html(Mustache.render(ToolboxStr,Strings));

            callback();
        }

        self.refill = function(callback){
            if($(".contentLoader").css('display') != "block")
                $(".contentLoader").show();

            this.fill(function(){
                $(".contentLoader").hide();
            });
        }

        self.forge = function (name) {
            return {
                id: self.id,
                name: name
            }
        }

        return self;
    };
});