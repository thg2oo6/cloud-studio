define(function (require, exports, module) {
    'use strict';
    var MenuEvent = ClassLoader(require('engine/events/MenuEvent'));

    MenuEvent.multipleAttach("LOGOUT, FILE_LOGOUT", function (e) {
        e.preventDefault();
        $(location).attr("href", CDSConfig.Logout);
    });

    MenuEvent.multipleAttach("HOME", function (e) {
        e.preventDefault();
        $(location).attr("href", CDSConfig.Home);
    });

    MenuEvent.multipleAttach("PROFILE, HELP_ACCOUNT", function (e) {
        e.preventDefault();
        $(location).attr("href", CDSConfig.Profile);
    });

    window.addEventListener("beforeunload", function (e) {
        if ($(".workspace .tabs li.changed").length > 0)
            return Strings.UNSAVED_NOTIFICATION;
    }, true);

    function ifXCV(e) {
        if (e.ctrlKey)
            switch (String.fromCharCode(e.which)) {
                case 'X':
                case 'C':
                case 'V':
                    return true;
            }

        return false;
    }

    function acceptedKey(e) {
        if (e.which >= 112 && e.which <= 123)
            return true;

        if (e.ctrlKey || e.altKey)
            return true;

        return false;
    }

    $(document).on("keydown", function (e) {
        if (!ifXCV(e) && acceptedKey(e)) {
            e.preventDefault();

            var element = "";
            element += e.ctrlKey ? "Ctrl+" : "";
            element += e.altKey ? "Alt+" : "";
            element += e.shiftKey ? "Shift+" : "";


            var tasta = "";
            if (e.which >= 65 && e.which <= 90) {
                tasta = String.fromCharCode(e.which);
                $("#topbar li[data-shortcut='" + element + tasta + "']").trigger("click");
            } else if (e.which >= 112 && e.which <= 123) {
                tasta = "F" + Math.abs(e.which - 111);
                $("#topbar li[data-shortcut='" + element + tasta + "']").trigger("click");
            }
        }
    });
});