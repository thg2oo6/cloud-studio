define(function (require, exports, module) {
    'use strict';
    var MenuEvent = ClassLoader(require("engine/events/MenuEvent")),
        Messages = ClassLoader(require("engine/utils/Messages")),
        ctxMenu = ClassLoader(require("ext/sidebar/ContextMenu")),
        Workspace = ClassLoader(require("engine/Workspace")),
        Strings = require("nls/strings");

    MenuEvent.attach("FILE_OPEN", function (e) {
        e.preventDefault();

        $("a[data-widget=\"PROJECT-FILES\"]").click();
    });

    MenuEvent.attach("FILE_NEW",function(e){
        e.preventDefault();

        ctxMenu.newFile();
    });

    MenuEvent.attach("FILE_NEW_WINDOW",function(e){
        e.preventDefault();

        ctxMenu.newWindow();
    });

    function getContent(tab_id){
        var result = {
            data: '',
            type: ''
        }

        var $container = $(".workspace .content#" + tab_id);

        if($container.data('editor') != null){
            result.data = $container.data('editor').getValue();
            result.type = "page";
        }else if($container.data('type') == "app"){
            result.data = JSON.stringify({
                style: $container.children('style').text(),
                window: $container.children('.layout').html(),
                script: $container.children('.actions').text()
            });
            result.type = "app";
        }

        return result;
    }

    MenuEvent.attach("FILE_SAVE", function(e){
        e.preventDefault();

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).children(".fader").show();

        active_tab.removeClass("changed");
        active_tab.children(".scroll").css({display: "inline-block"});

        var ctnt = getContent(active_tab.data('container')),
            file = active_tab.data('name'),
            path = active_tab.data('path')
            ;

        $.ajax({
            url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/save"),
            data: {
                file: file,
                path: path,
                content: ctnt.data,
                type: ctnt.type
            },
            success: function(data){
                active_tab.children(".scroll").hide();
                $(".workspace .content#" + active_tab.data('container')).children(".fader").hide();
            },
            dataType: 'json',
            type: 'POST',
            error: function(e){
                active_tab.addClass("changed");
                Messages.error(e.responseJSON.message);
            }
        });
    })

    MenuEvent.attach("FILE_SAVE_ALL",function(){
        $(".workspace .tabs li.changed").each(function(i,e){

            var active_tab = $(this);
            $(".workspace .content#" + active_tab.data('container')).children(".fader").show();

            active_tab.removeClass("changed");
            active_tab.children(".scroll").css({display: "inline-block"});

            var ctnt = getContent(active_tab.data('container')),
                file = active_tab.data('name'),
                path = active_tab.data('path')
                ;

            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/save"),
                data: {
                    file: file,
                    path: path,
                    content: ctnt.data,
                    type: ctnt.type
                },
                success: function(data){
                    active_tab.children(".scroll").hide();
                    $(".workspace .content#" + active_tab.data('container')).children(".fader").hide();
                },
                dataType: 'json',
                type: 'POST',
                error: function(e){
                    active_tab.addClass("changed");
                    Messages.error(e.responseJSON.message);
                }
            });
        });
//        alert("Saved!");
    });

    MenuEvent.attach("FILE_CLOSE",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        Workspace.closeElement(active_tab.children(".title"));
    });

});