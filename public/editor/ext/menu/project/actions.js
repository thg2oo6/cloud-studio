define(function (require, exports, module) {
    'use strict';
    var MenuEvent = ClassLoader(require("engine/events/MenuEvent")),
        Messages = ClassLoader(require("engine/utils/Messages"));

    MenuEvent.attach("PROJECT_RUN", function () {
        window.open(CDSConfig.Test, "_blank");
    });

    MenuEvent.attach("PROJECT_DEPLOY", function () {

        function redirect_call() {
            var win = window.open(CDSConfig.Live, "_blank");
            win.focus();
        }

        function ajax_call(button, txt) {
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/deploy"),
                data: { },
                success: function (data) {
                    if (data.status == "OK") {
                        button.attr("disabled", false);
                        txt.text("Application has been deployed.\nPress ok to be redirected to it.");

                        //redirect_call();
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    Messages.error(e.responseJSON.message);
                }
            });
        }

        Messages.form_call(
            "The application is about to be deployed!\nWhen it is ready, the ok button will be enabled.",
            "Deployment",
            ajax_call,
            redirect_call,
            true
        );
    });

    MenuEvent.attach("PROJECT_UNDEPLOY", function () {

        function ajax_call(button, txt) {
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/undeploy"),
                data: { },
                success: function (data) {
                    if (data.status != "OK")
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    Messages.error(e.responseJSON.message);
                }
            });
        }

        Messages.confirm(
            "Are you sure you want to undeploy the application?",
            ajax_call,
            "Deployment"
        );
    });

    MenuEvent.attach("PROJECT_DOWNLOAD", function () {

        function redirect_call() {
            var win = window.open(CDSConfig.APIPath("files/" + CDSConfig.Project + "/download"), "_blank");
            win.focus();
        }

        function ajax_call(button, txt) {
            $.ajax({
                url: CDSConfig.APIPath("files/" + CDSConfig.Project + "/prepare"),
                data: { },
                success: function (data) {
                    if (data.status == "OK") {
                        button.attr("disabled", false);
                        txt.text("File is ready to be downloaded.");

//                        redirect_call();
                    } else
                        Messages.error(data.message);
                },
                dataType: 'json',
                type: 'POST',
                error: function (e) {
                    Messages.error(e.responseJSON.message);
                }
            });
        }

        Messages.form_call(
            "The file is about to be created!\nWhen it is ready, the save button will be enabled.",
            "Download",
            ajax_call,
            redirect_call
        );
    });

    MenuEvent.attach("PROJECT_INFO", function () {
        $.ajax({
            url: CDSConfig.APIPath("project/" + CDSConfig.Project),
            data: { },
            success: function (data) {
                var text = "<ul id=\"ProjectInfoDetails\">"
                         + "<li><strong>Name:</strong> {{name}}</li>"
                         + "<li><strong>Type:</strong> {{type}}</li>"
                         + "<li><strong>Slug:</strong> {{slug}}</li>"
                         + "<li><strong>Owner:</strong> {{owner}}</li>"
                         + "<li><strong>Visibility:</strong> {{visibility}}</li>"
                         + "<li><strong>Created:</strong> {{created_at}}</li>"
                         + "<li><strong>Last update:</strong> {{updated_at}}</li>"
                         + "</ul>"
                    ;


                Messages.notification(Mustache.render(text, data), "Project info");
            },
            dataType: 'json',
            type: 'GET',
            error: function (e) {
                Messages.error(e.responseJSON.message);
            }
        });

    });

    MenuEvent.attach("PROJECT_SETTINGS", function () {

        Messages.confirm(
            Strings.DASHBOARD_GOTO_SETTINGS,
            function(){
               $(location).attr("href", CDSConfig.Profile);
            }
        );

    });


});
