define(function (require, exports, module) {
    'use strict';
    var MenuEvent = ClassLoader(require("engine/events/MenuEvent")),
        Messages = ClassLoader(require("engine/utils/Messages")),
        Strings = require("nls/strings")
        ;

    MenuEvent.attach("HELP_ISSUE",function(){
        Messages.confirm(
            Strings.BITBUCKET_NOTIFICATION,
            function(){
                var win = window.open("https://bitbucket.org/thg2oo6/cloud-studio/issues","_blank");
                win.focus();
            }
        );
    });
    MenuEvent.attach("HELP_ABOUT",function(){
        var text = "<strong>Cloud Developer Studio " + CDSConfig.Version + "</strong><br />";
        text += "2013 - 2014 &copy; Valentin Duricu <valentin at duricu.ro>";
        Messages.notification(text, "About");
    });

});