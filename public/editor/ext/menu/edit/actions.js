define(function (require, exports, module) {
    'use strict';
    var MenuEvent = ClassLoader(require("engine/events/MenuEvent")),
        Messages = ClassLoader(require("engine/utils/Messages")),
        Workspace = ClassLoader(require("engine/Workspace")),
        Strings = require("nls/strings")
    ;

    MenuEvent.attach("EDIT_UNDO",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).data('editor').undo();
    });
    MenuEvent.attach("EDIT_REDO",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).data('editor').redo();
    });

    var notWorking = function(){
        Messages.notification(Strings.NOT_WORKIING_WB);
    }
    MenuEvent.attach("EDIT_COPY", notWorking);
    MenuEvent.attach("EDIT_CUT", notWorking);
    MenuEvent.attach("EDIT_PASTE", notWorking);

    MenuEvent.attach("EDIT_GOTO",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        Messages.prompt(Strings.JUMP_TO, 1, function(line){
            if (line && !isNaN(Number(line))){
                var active_tab = $(".workspace .tabs li.active");
                $(".workspace .content#" + active_tab.data('container')).data('editor').gotoLine(Number(line));
            }
        });

    });

    MenuEvent.attach("EDIT_FIND",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).data('editor').execCommand('find');
    });

    MenuEvent.attach("EDIT_FIND_NEXT",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).data('editor').execCommand('findNext');
    });

    MenuEvent.attach("EDIT_FIND_PREV",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).data('editor').execCommand('findPrev');
    });

    MenuEvent.attach("EDIT_REPLACE",function(){
        if(Workspace.empty())
            Messages.error(Strings.OPEN_A_FILE);

        var active_tab = $(".workspace .tabs li.active");
        $(".workspace .content#" + active_tab.data('container')).data('editor').execCommand('replace');
    });

});