/*global require, define, cds: true, $, window, navigator, Mustache */

requirejs.config({

    paths: {
        "text": "3rdparty/text",
        "i18n": "3rdparty/i18n"
    },

    locale: window.localStorage.getItem("locale") || (typeof (CDSConfig) !== "undefined" ? CDSConfig.language : navigator.language)
});


define(function (require, exports, module) {
    'use strict';

    $("<section/>", {class: 'dashboard'}).appendTo("body");

    window.ace = require('ace/ace');
    require('ace/ext/language_tools');

    // Modules that do not return anything.
    /* -- GENERAL TOOLS -- */
    require('engine/utils/ClassLoader');
    require('engine/utils/Tools');

    /* -- GLOBAL OBJECTS -- */
    require('engine/events/DOMEvent');
    require('engine/events/HookupEvent');

    // Modules that return something.
    var Strings = require("nls/strings")
        ;

    // Modules that are loaded using the class loader.
    var Menu = ClassLoader(require("engine/Menu")),
        Sidebar = ClassLoader(require("engine/Sidebar")),
        Workspace = ClassLoader(require("engine/Workspace")),
        Messages = ClassLoader(require("engine/utils/Messages"))
        ;

    // Initialising elements
    Menu.render();
    Sidebar.render();
    Workspace.render();
    $(".dashboard").append(require("text!views/utils/messages.html"));

    DOMEvent.addReady(function () {
        DOMEvent.onResize();
        $(".loader").fadeOut();
    });

    var lazyLayout = _.debounce(DOMEvent.onResize, 300);
    // Finally we add the DOMEvents for document ready and window resize.
    $(document).ready(DOMEvent.onReady);
    $(window).resize(lazyLayout);
})