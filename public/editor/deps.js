/**
 * Cloud Developer Studio
 *
 * Version: 0.3.1
 * Author: Valentin Duricu <valentin@duricu.ro>
 * URL: http://cloud-studio.ro
 * File: deps.js
 *
 * Defines and tests the existence of system dependencies.
 */

window.setTimeout(function () {
    "use strict";
    var deps = { "Mustache": window.Mustache, "jQuery": window.$, "RequireJS": window.require };
    var key, missingDeps = [];
    for (key in deps) {
        if (deps.hasOwnProperty(key) && !deps[key]) {
            missingDeps.push(key);
        }
    }
    if (missingDeps.length === 0) {
        return;
    }
    var _text = "Missing libraries!\n";
    _text += "It looks like some libraries could not be found:\n";
    missingDeps.forEach(function (key) {
        _text += key + "\n";
    });

    _text += "Please contact " + CDSConfig.ContactEmail + " or submit a support ticket at " + CDSConfig.TicketSystem;

    alert(_text);

}, 1000);
