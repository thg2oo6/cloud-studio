define(function (require, exports, module) {
    'use strict';

    var strings = require('i18n!nls/lang');

    window.Strings = strings;

    module.exports = strings;
});