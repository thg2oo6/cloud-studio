define({
    /* File Menu Items */
    "FILE": "File",
    "FILE_NEW": "New file",
    "FILE_NEW_WINDOW": "New window",
    "FILE_OPEN": "Open file",
    "FILE_CLOSE": "Close file",
    "FILE_SAVE": "Save file",
    "FILE_SAVE_ALL": "Save all",
    "FILE_SAVE_AS": "Save as...",
    "FILE_LOGOUT": "Logout",

    /* Edit Menu Items */
    "EDIT": "Edit",
    "EDIT_UNDO": "Undo",
    "EDIT_REDO": "Redo",
    "EDIT_CUT": "Cut",
    "EDIT_COPY": "Copy",
    "EDIT_PASTE": "Paste",
    "EDIT_FIND": "Find",
    "EDIT_FIND_NEXT": "Find next",
    "EDIT_FIND_PREV": "Find prev",
    "EDIT_REPLACE": "Replace",
    "EDIT_GOTO": "Jump to line",

    /* Project Menu Items */
    "PROJECT": "Project",
    "PROJECT_DEBUG": "Debug",
    "PROJECT_DEPLOY": "Deploy",
    "PROJECT_UNDEPLOY": "Undeploy",
    "PROJECT_DOWNLOAD": "Download",
    "PROJECT_RUN": "Run",
    "PROJECT_INFO": "Project Info",
    "PROJECT_SETTINGS": "Project Settings",

    /* Help Menu Items */
    "HELP": "Help",
    "HELP_HOWTO": "How to use",
    "HELP_ISSUE": "Report an issue",
    "HELP_ACCOUNT": "My Account",
    "HELP_ABOUT": "About",

    /* Extra Toolbar */
    "HOME": "Home",
    "PROFILE": "Profile",
    "LOGOUT": "Logout",

    /* Sidebar Elements */
    "PROJECT-FILES": "Files",
    "TOOLBOX": "Toolbox",
    "PROPERTIES": "Properties",

    /*###### Labels & Messages ######*/
    /*Global Utils*/
    "YES": "Yes",
    "NO": "No",
    "OK": "Okay",
    "CANCEL": "Cancel",
    "SAVE": "Save",
    "CREATE": "Create",

    /*Message Utils*/
    "ERROR_TITLE": "ERROR",
    "NOTIFICATION_TITLE": "Notification",
    "PROMPT_TITLE": "Prompt",

    /*Action Messages*/
    "NOT_WORKIING_WB": "Currently this button is not supported, but you can use the corresponding shortcut.",
    "OPEN_A_FILE": "Open a file first!",
    "BITBUCKET_NOTIFICATION": "You will be redirected to the BitBucket issue tracker.",
    "UNSAVED_NOTIFICATION": "There are some unsaved files.\n\nAre you sure you want to exit?",
    "SAVED": "File saved",
    "JUMP_TO": "Jump to line:",
    "DASHBOARD_GOTO_SETTINGS": "To change the settings of the current project we will transfer you to Dashboard. There please select the desired project and make the changes.",

    /*Project Files Messages*/
    "NEWFILE_LABEL": "Give the file a name:",
    "NEWFOLDER_LABEL": "Give the folder a name:",
    "NEWWINDOW_LABEL": "Give the window a name:",
    "RENAME_LABEL": "Give the new file a name:",
    "NOFILE": "No file given!!!",
    "DELETE_CONFIRMATION": "Are you sure you want to delete the file '%s'?",

    "CTX_NEWFILE": "New file",
    "CTX_NEWFOLDER": "New folder",
    "CTX_NEWWINDOW": "New window",
    "CTX_RENAME": "Rename",
    "CTX_DELETE": "Delete",
    "CTX_REFRESH": "Refresh file tree",

    "CTX2_DELETEOBJ": "Delete object",
    "CTX2_PROPERTIES": "Object properties",
});