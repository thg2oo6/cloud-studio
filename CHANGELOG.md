#==== VERSION 0.4.x ====
## 0.4.3

## 0.4.2
- Transform settings in user settings
- Redesign the user settings page
- Added the APIKey to the user settings page
- API Key regeneration on request
- Project Update operations from Dashboard
- Project Information from IDE


## 0.4.1
- Dev Shell first version
- API to access project details
- API to authenticate user
- API Key field added to user
- Added link to Blog
- API Documentation
- Update Laravel version to 4.2

#==== VERSION 0.3.1 ====

- Public/Private project creation
- Listing of Public projects
- Listing of all the activities performed on a given project
- UI Builder fixes + new file format
- Code Editor enhancements
- API to create new Top menu elements
- Refactored IDE code to be more Object Oriented
